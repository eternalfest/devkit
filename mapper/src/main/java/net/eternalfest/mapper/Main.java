package net.eternalfest.mapper;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.RescaleOp;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.imageio.ImageIO;

import net.eternalfest.hfestlib.assets.Assets;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.tools.ResourceManager;
import net.eternalfest.hfestlib.tools.ShowableException;
import net.eternalfest.hfestlib.utils.FileUtils;
import org.jdom2.Document;

//TODO: use Paths
public class Main {
    private String options;
    private String[] args;
    private File root;
    private Assets resources;


    private boolean hasInfoFile = false;

    private int xMin = Integer.MAX_VALUE, xMax = Integer.MIN_VALUE, yMin = Integer.MAX_VALUE, yMax = Integer.MIN_VALUE;
    private int xSize, xOffset, ySize, yOffset;
    private int maxPics = 0;

    private int dummyLvls = 0, nbPicCaches = 0, nbVortex = 0, nbRespawn = 0;

    private ArrayList<LevelPos> listLvls = new ArrayList<LevelPos>();
    private List<String> infos = null;

    private byte[][] connexions = null; //0 = fermé, 1 = ouvert en bas, 2 = ouvert à gauche, 3 = ouvert aux deux, (dbg)

    public Main(String[] a) {
        options = "";
        args = a;

        if (args.length < 1 || args.length > 3) {
            System.out.println("Utilisation :\njava -jar mapCreator.jar [options] [dossier d'images] [fichier de config]");

            System.out.println("\nOptions :");
            System.out.println("-L : Éclaircit les images.");
            System.exit(1);
        }

        if (args[0].startsWith("-")) {
            options = args[0].replaceAll("-", "");

            String[] newArgs = new String[Math.max(0, args.length - 1)];
            for (int i = 1; i < args.length; i++)
                newArgs[i - 1] = args[i];

            args = newArgs;
        }

        root = new File(System.getProperty("user.dir"));
        try {
            resources = ResourceManager.getFromAmbientConfig(root.toPath());
        } catch (ShowableException e) {
            e.show();
            System.exit(1);
        }
    }

    public static void main(String[] args) {
        ShowableException.setGUI(false);

        Main main = new Main(args);
        main.load();
        main.createMap();
    }

    public static List<String> getFileContent(File file) {
        ArrayList<String> content = new ArrayList<String>();
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            String line = reader.readLine();
            while (line != null) {
                content.add(line);
                line = reader.readLine();
            }

        } catch (Exception e) {
            return null;
        }

        return content;
    }

    public void load() {
        loadLevels();

        connexions = new byte[xSize + 2][ySize + 1];

        loadInfos();
        processInfos();

        System.out.println((listLvls.size() - dummyLvls) + " niveaux cartographiés.");
        System.out.println(nbVortex + " vortex, " + nbRespawn + " respawns, " + nbPicCaches + " pics cachés.");


    }

    public void createMap() {
        MapDrawer map = new MapDrawer(new File(root, "carteLaby.png"), new File(root, "cartePics.png"));
        map.createMap();
        map.createPicMap();
    }

    private void loadLevels() {
        File levelDir = new File(args[0]);
        if (!levelDir.isAbsolute())
            levelDir = new File(root, args[0]);
        File[] listFiles = levelDir.listFiles();

        if (listFiles == null) {
            System.out.println("Le dossier " + levelDir.getAbsolutePath() + " n'existe pas ou est vide.");
            return;
        }

        for (int i = 0; i < listFiles.length; i++) {
            if (listFiles[i].isDirectory()) continue;

            String name = listFiles[i].getName();
            if (!name.endsWith(").png") && !name.endsWith(").lvl")) continue;
            if (!name.startsWith("Niveau (")) continue;

            String[] coords = name.substring(8, name.length() - 5).split(";");
            if (coords.length != 2) continue;

            int x = 0, y = 0;
            try {
                x = Integer.parseInt(coords[0]);
                y = Integer.parseInt(coords[1]);
            } catch (NumberFormatException e) {
                break;
            }

            listLvls.add(new LevelPos(x, y, listFiles[i], name.endsWith(").png")));
            if (x < xMin) xMin = x;
            if (x > xMax) xMax = x;
            if (y < yMin) yMin = y;
            if (y > yMax) yMax = y;
        }

        if (listLvls.isEmpty()) {
            System.out.println("Le dossier " + levelDir.getAbsolutePath() + " ne contient aucun niveau.");
            return;
        }


        xSize = xMax - xMin;
        xOffset = -xMin;
        ySize = yMax - yMin;
        yOffset = -yMin;
    }

    private void loadInfos() {
        if (args.length >= 2) {
            infos = getFileContent(new File(root, args[1]));
            if (infos == null) {
                System.out.println("Impossible de charger le fichier " + (new File(root, args[1])).getAbsolutePath() + ".");
                System.out.println("Certaines informations ne seront pas présentes.");
            } else hasInfoFile = true;
        }

        if (infos == null) {
            for (int i = 0; i < connexions.length; i++) {
                for (int j = 0; j < connexions[i].length; j++) {
                    if (i > 0) connexions[i][j] += 2;
                    if (j < connexions[i].length - 1)
                        connexions[i][j] += 1;
                }
            }
            infos = Collections.emptyList();
        }
    }

    public void processInfos() {
        for (String line : infos) {
            if (line == null) continue;
            if (line.startsWith("//")) continue; //commentaires

            String[] tokens = line.split("[;:,]");
            if (tokens.length == 0) continue;
            for (int i = 0; i < tokens.length; i++)
                tokens[i] = tokens[i].trim();

            switch (tokens[0]) {
                case "Niv":
                    processTokenNiv(tokens);
                    break;

                case "Vortex":
                    processTokenVortex(tokens);
                    break;

                case "Respawn":
                    processTokenRespawn(tokens);
                    break;
            }

        }
    }

    public void processTokenNiv(String[] tokens) {
        if (tokens.length != 5 && tokens.length != 4) return;
        int x, y, nbPics = 0;

        try {
            x = Integer.parseInt(tokens[1]);
            y = Integer.parseInt(tokens[2]);
            if (tokens.length >= 5)
                nbPics = Integer.parseInt(tokens[4]);
        } catch (NumberFormatException e) {
            return;
        }

        x += xOffset;
        y += yOffset;
        nbPicCaches += nbPics;

        if (x < 0 || x >= xSize + 1 || y < 0 || y >= ySize + 1) return;

        tokens[3] = tokens[3].toLowerCase();
        if (tokens[3].contains("g"))
            connexions[x][y] |= 2;
        if (tokens[3].contains("d"))
            connexions[x + 1][y] |= 2;
        if (tokens[3].contains("b"))
            connexions[x][y] |= 1;

        if (nbPics > 0) {
            if (nbPics > maxPics) maxPics = nbPics;
            for (LevelPos lvl : listLvls) {
                if (lvl.posX == (x - xOffset) && lvl.posY == (y - yOffset)) {
                    lvl.nbPics = nbPics;
                    break;
                }
            }
        }
    }

    public void processTokenVortex(String[] tokens) {
        if (tokens.length != 6) return;

        int x, y, xVortex, yVortex;
        try {
            x = Integer.parseInt(tokens[1]);
            y = Integer.parseInt(tokens[2]);
            xVortex = Integer.parseInt(tokens[4]);
            yVortex = Integer.parseInt(tokens[5]);
        } catch (NumberFormatException e) {
            return;
        }

        if (tokens[3].length() == 0) return;
        nbVortex++;

        for (LevelPos lvl : listLvls) {
            if (lvl.posX != x || lvl.posY != y) continue;

            if (lvl.vortexID == ' ') {
                lvl.vortexID = tokens[3].charAt(0);
                lvl.xVortex = xVortex;
                lvl.yVortex = yVortex;
            } else {
                LevelPos dummy = new LevelPos(x, y, null, true);
                dummy.vortexID = tokens[3].charAt(0);
                dummy.xVortex = xVortex;
                dummy.yVortex = yVortex;
                listLvls.add(dummy);
                dummyLvls++;
            }
            break;
        }
    }

    public void processTokenRespawn(String[] tokens) {
        if (tokens.length != 6) return;

        int x, y, xRespawn, yRespawn;
        try {
            x = Integer.parseInt(tokens[1]);
            y = Integer.parseInt(tokens[2]);
            xRespawn = Integer.parseInt(tokens[4]);
            yRespawn = Integer.parseInt(tokens[5]);
        } catch (NumberFormatException e) {
            return;
        }

        if (tokens[3].length() == 0) return;
        nbRespawn++;

        for (LevelPos lvl : listLvls) {
            if (lvl.posX != x || lvl.posY != y) continue;
            if (lvl.respawnID == ' ') {
                lvl.respawnID = tokens[3].charAt(0);
                lvl.xRespawn = xRespawn;
                lvl.yRespawn = yRespawn;
            } else {
                LevelPos dummy = new LevelPos(x, y, null, true);
                dummy.respawnID = tokens[3].charAt(0);
                dummy.xRespawn = xRespawn;
                dummy.yRespawn = yRespawn;
                listLvls.add(dummy);
                dummyLvls++;
            }
            break;
        }
    }

    static class LevelPos {
        int posX, posY;
        File path;
        boolean isImage = true;
        int nbPics = 0;

        char vortexID = ' ';
        int xVortex = -1, yVortex = -1;

        char respawnID = ' ';
        int xRespawn = -1, yRespawn = -1;

        LevelPos(int posX, int posY, File path, boolean isImage) {
            this.posX = posX;
            this.posY = posY;
            this.path = path;
            this.isImage = isImage;
        }
    }

    class MapDrawer {
        static final int imgXOffset = 100, imgYOffset = 100, lvlWidth = 420, lvlHeight = 500;
        static final int tileSize = 20;
        static final int lineThickness = 5;

        static final int picImgXOffset = 0, picImgYOffset = 0;
        static final int picLineThickness = 1;


        private File mapFile;
        private File picFile;

        private long startTime;
        private boolean eclaircir = options.contains("L"), drawVorticesAndRespawns = true;
        private int imgWidth, imgHeight, picImgWidth, picImgHeight;

        public MapDrawer(File mapFile, File picFile) {
            this.mapFile = mapFile;
            this.picFile = picFile;
        }

        public void createMap() {
            startTime = System.currentTimeMillis();

            imgWidth = (xSize + 1) * lvlWidth + 1 + 2 * imgXOffset;
            imgHeight = (ySize + 1) * lvlHeight + 2 * imgYOffset;

            BufferedImage carte = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_BGR);
            System.out.println("Mémoire allouée en " + (System.currentTimeMillis() - startTime) + "ms.");

            Graphics2D g = (Graphics2D) carte.getGraphics();
            drawLevels(g);
            drawGrid(g);
            drawConnections(g);


            g.dispose();
            System.out.println("Taille de l'image : " + imgWidth + "x" + imgHeight + "px (" + (xSize + 1) + "x" + (ySize + 1) + ").");
            startTime = System.currentTimeMillis();
            try {
                ImageIO.write(carte, "png", mapFile);
            } catch (IOException e) {
            }
            System.out.println("Image enregistrée en " + (System.currentTimeMillis() - startTime) + "ms.");
            carte.flush();
        }

        public void drawLevels(Graphics2D g) {
            g.setColor(Color.lightGray);
            g.fillRect(0, 0, imgWidth, imgHeight);

            Color[] vortexColors = {Color.red, Color.green, Color.yellow, Color.pink, Color.orange, Color.magenta, Color.white};

            g.setFont(g.getFont().deriveFont(50.0f));

            int progression = 0, quantiemes = 20;
            for (int i = 0; i < listLvls.size(); i++) {
                LevelPos lvl = listLvls.get(i);
                int imgX = imgXOffset + (lvl.posX + xOffset) * lvlWidth, imgY = imgYOffset + (lvl.posY + yOffset) * lvlHeight;
                if (lvl.path != null && lvl.isImage) {
                    Image img = (new javax.swing.ImageIcon(lvl.path.getAbsolutePath())).getImage();

                    if (eclaircir) {
                        BufferedImage adjusted = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_BGR);
                        Graphics2D ug = adjusted.createGraphics();
                        ug.drawImage(img, 0, 0, null);
                        ug.dispose();
                        BufferedImageOp filter = new RescaleOp(2.5f, 1.0f, null);
                        BufferedImage imgEclaircie = filter.filter(adjusted, null);

                        g.drawImage(imgEclaircie, imgX, imgY, null);
                        img.flush();
                        adjusted.flush();
                        imgEclaircie.flush();
                    } else {
                        g.drawImage(img, imgX, imgY, null);
                        img.flush();
                    }
                } else if (lvl.path != null) {
                    try {
                        Document XML = FileUtils.loadXML(lvl.path.toPath());
                        Level niveau = new Level(XML, resources);
                        g.translate(imgX, imgY);
                        niveau.draw(g, null);
                        g.translate(-imgX, -imgY);
                    } catch (ShowableException e) {
                        e.show();
                    }
                }

                if (drawVorticesAndRespawns) {
                    if (lvl.vortexID != ' ') {
                        g.setColor(vortexColors[(lvl.vortexID - 1) % vortexColors.length]);
                        g.drawString("" + lvl.vortexID, imgX + tileSize / 2 - 5 + tileSize * lvl.xVortex, imgY + 20 + tileSize * lvl.yVortex);
                    }
                    if (lvl.respawnID != ' ') {
                        g.setColor(vortexColors[(lvl.respawnID - 1) % vortexColors.length]);
                        g.drawString("" + lvl.respawnID, imgX + tileSize / 2 - 5 + tileSize * lvl.xRespawn, imgY + 20 + tileSize * lvl.yRespawn);
                    }
                }

                if ((progression * listLvls.size()) / quantiemes < i) {
                    progression++;
                    System.out.print('.');
                }
            }

            System.out.println();
        }

        public void drawGrid(Graphics2D g) {
            g.setColor(Color.white);
            for (int i = imgXOffset; i < imgWidth; i += lvlWidth)
                g.fillRect(i - lineThickness, 0, 2 * lineThickness, imgHeight);

            for (int j = imgXOffset; j < imgHeight; j += lvlHeight)
                g.fillRect(0, j - lineThickness, imgWidth, 2 * lineThickness);


            g.setColor(Color.black);
            g.setFont(g.getFont().deriveFont(50.0f));
            for (int i = 0; i <= xSize; i++) {
                g.drawString(String.valueOf(i - xOffset), imgYOffset + lvlWidth / 2 + i * lvlWidth - 20, 70);
                g.drawString(String.valueOf(i - xOffset), imgYOffset + lvlWidth / 2 + i * lvlWidth - 20, imgHeight - 45);
            }
            for (int j = 0; j <= ySize; j++) {
                g.drawString(String.valueOf(j - yOffset), 10, imgXOffset + lvlHeight / 2 + j * lvlHeight - 10);
                g.drawString(String.valueOf(j - yOffset), imgWidth - 80, imgXOffset + lvlHeight / 2 + j * lvlHeight - 10);
            }
        }

        public void drawConnections(Graphics2D g) {
            g.setColor(new Color(30, 30, 30));
            for (int i = 0; i < connexions.length; i++) {
                for (int j = 0; j < connexions[i].length; j++) {
                    if ((connexions[i][j] & 2) != 0) { //Connexion gauche
                        g.fillRect(imgXOffset + lvlWidth * i - lineThickness, imgYOffset + lvlHeight * j + lineThickness, 2 * lineThickness, lvlHeight - 2 * lineThickness);
                    }

                    if ((connexions[i][j] & 1) != 0) { //Connexion basse
                        g.fillRect(imgXOffset + lvlWidth * i + lineThickness, imgYOffset + lvlHeight * (j + 1) - lineThickness, lvlWidth - 2 * lineThickness, 2 * lineThickness);
                    }
                }
            }
        }

        public void createPicMap() {
            if (!hasInfoFile)
                return;

            picImgWidth = (xSize + 1) * tileSize;
            picImgHeight = (ySize + 1) * tileSize;
            BufferedImage imgPics = new BufferedImage(picImgWidth, picImgHeight, BufferedImage.TYPE_INT_BGR);
            Graphics2D g = imgPics.createGraphics();
            g.setColor(Color.lightGray);
            g.fillRect(0, 0, picImgWidth, picImgHeight);

            for (LevelPos lvl : listLvls) {
                if (lvl.path == null) continue;
                int imgX = picImgXOffset + (lvl.posX + xOffset) * tileSize, imgY = picImgYOffset + (lvl.posY + yOffset) * tileSize;

                Color color = Color.getHSBColor(0.3333333f, 1.0f, 1.0f);
                if (lvl.nbPics > 0) {
                    float interpol = ((lvl.nbPics - 1) * 1.0f) / (maxPics - 1);
                    color = Color.getHSBColor((1 - interpol) * 0.1666666f, 1.0f, 1.0f);
                }
                g.setColor(color);
                g.fillRect(imgX, imgY, tileSize, tileSize);

            }

            g.setColor(new Color(30, 30, 30));
            for (int i = picImgXOffset; i < picImgWidth; i += tileSize)
                g.fillRect(i - lineThickness, 0, 2 * lineThickness, picImgHeight);

            for (int j = picImgXOffset; j < picImgHeight; j += tileSize)
                g.fillRect(0, j - lineThickness, picImgWidth, 2 * lineThickness);

            //Dessin des connexions
            g.setColor(Color.white);
            for (int i = 0; i < connexions.length; i++) {
                for (int j = 0; j < connexions[i].length; j++) {
                    if ((connexions[i][j] & 2) != 0) { //Connexion gauche
                        g.fillRect(picImgXOffset + tileSize * i - lineThickness,
                            picImgYOffset + tileSize * j + lineThickness,
                            2 * lineThickness,
                            tileSize - 2 * lineThickness);
                    }

                    if ((connexions[i][j] & 1) != 0) { //Connexion basse
                        g.fillRect(picImgXOffset + tileSize * i + lineThickness,
                            picImgYOffset + tileSize * (j + 1) - lineThickness,
                            tileSize - 2 * lineThickness,
                            2 * lineThickness);
                    }
                }
            }


            g.dispose();
            try {
                ImageIO.write(imgPics, "png", picFile);
            } catch (IOException e) {
            }

            imgPics.flush();

        }
    }

}
