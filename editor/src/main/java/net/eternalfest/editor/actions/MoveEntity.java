package net.eternalfest.editor.actions;

import javax.swing.undo.UndoableEdit;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Entity;
import net.eternalfest.hfestlib.models.Level;

public class MoveEntity extends LevelAction {
    private Entity entity;
    private double oldX, oldY, newX, newY;

    public MoveEntity(Entity entity, double x, double y) {
        super(MODIFY_ENTITIES);
        this.entity = entity;
        newX = x;
        newY = y;
    }

    @Override
    public void doAction(Level level) {
        oldX = entity.getX();
        oldY = entity.getY();
        entity.setPos(newX, newY);
    }

    @Override
    public void undoAction(Level level) {
        entity.setPos(oldX, oldY);
    }

    @Override
    protected boolean tryMerge(UndoableEdit edit, Level level) {
        if (!(edit instanceof MoveEntity))
            return false;

        MoveEntity action = (MoveEntity) edit;

        if (action.entity != entity)
            return false;

        newX = action.newX;
        newY = action.newY;
        return true;
    }
}
