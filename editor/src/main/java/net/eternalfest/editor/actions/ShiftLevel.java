package net.eternalfest.editor.actions;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.models.LevelTiles;

public class ShiftLevel extends LevelAction {

    public static final int SHIFT_LEFT = 0b00;
    public static final int SHIFT_RIGHT = 0b01;
    public static final int SHIFT_UP = 0b10;
    public static final int SHIFT_DOWN = 0b11;

    private byte[] pfs;
    private final int shift;
    private double[] entityPos;

    public ShiftLevel(int shift) {
        super(MODIFY_ENTITIES | MODIFY_TILES);
        this.shift = shift;
    }

    private boolean isHorizontal() {
        return (shift & 0b10) == 0;
    }

    private boolean isForwards() {
        return (shift & 0b01) == 1;
    }

    private static double moveAndClamp(double x, double delta, double min, double max) {
        if(x < min || x >= max)
            return x + delta;
        x += delta;
        if(x < min)
            return min;
        if(x >= max)
            return (int) max-1;
        return x;
    }

    @Override
    public void doAction(Level level) {
        LevelTiles tiles = level.getTiles();
        int delta = isForwards() ? 1 : -1;

        //PLATEFORMES
        if (isHorizontal()) {
            int column = isForwards() ? level.getWidth() - 1 : 0;
            pfs = new byte[level.getHeight()];
            for (int i = 0; i < pfs.length; i++)
                pfs[i] = tiles.getRawAt(column, i);
            tiles.shift(delta, 0);
        } else {
            int line = isForwards() ? level.getHeight() - 1 : 0;
            pfs = new byte[level.getWidth()];
            for (int i = 0; i < pfs.length; i++)
                pfs[i] = tiles.getRawAt(i, line);
            tiles.shift(0, delta);
        }

        //SPRITES
        entityPos = level.allEntities().mapToDouble(e -> {
            if(isHorizontal()) {
                double old = e.getX();
                e.setPos(moveAndClamp(old, delta, 0, level.getWidth()), e.getY());
                return old;
            } else {
                double old = e.getY();
                e.setPos(e.getX(), moveAndClamp(old, delta, 0, level.getHeight()));
                return old;
            }
        }).toArray();
    }

    private int idx;
    @Override
    public void undoAction(Level level) {
        LevelTiles tiles = level.getTiles();

        //PLATEFORMES
        int delta = isForwards() ? 1 : -1;
        if (isHorizontal()) {
            tiles.shift(-delta, 0);
            int column = isForwards() ? level.getHeight() - 1 : 0;
            for (int i = 0; i < pfs.length; i++) {
            	tiles.setRawAt(column, i, pfs[i]);
            }
        } else {
            tiles.shift(0, -delta);
            int line = isForwards() ? level.getWidth() - 1 : 0;
            for (int i = 0; i < pfs.length; i++) {
            	tiles.setRawAt(i, line, pfs[i]);
            }
        }

        idx = 0;
        level.allEntities().forEachOrdered(e -> {
            if (isHorizontal())
                e.setPos(entityPos[idx], e.getY());
            else e.setPos(e.getX(), entityPos[idx]);
            idx++;
        });
    }
}
