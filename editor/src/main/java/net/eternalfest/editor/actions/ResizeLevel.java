package net.eternalfest.editor.actions;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Entity;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.models.LevelTiles;

public class ResizeLevel extends LevelAction {

	int width, height, oldWidth, oldHeight;
	List<MovedEntity> entities;
	byte[][] rightTiles, bottomTiles;

    public ResizeLevel(int width, int height) {
        super(MODIFY_ALL);
        this.width = width;
        this.height = height;
    }

    @Override
    public void doAction(Level level) {
    	LevelTiles tiles = level.getTiles();
    	oldWidth = tiles.getWidth();
    	oldHeight = tiles.getHeight();

    	if (width < oldWidth) {
    		rightTiles = new byte[oldWidth - width][oldHeight];
    		fetchTilesWithOffset(rightTiles, tiles, width, 0);
    	}

		if (height < oldHeight) {
    		bottomTiles = new byte[width][oldHeight - height];
    		fetchTilesWithOffset(bottomTiles, tiles, 0, height);
    	}

    	tiles.resize(width, height);

    	if (width < oldWidth || height < oldHeight) {
    		entities = level.allEntities()
    				.filter(e -> e.getX() >= width || e.getY() >= height)
    				.map(e -> {
    					MovedEntity m = new MovedEntity();
    					m.e = e;
    					m.oldX = e.getX();
    					m.oldY = e.getY();
    					e.setPos(Math.min(m.oldX, width - 1.0), Math.min(m.oldY, height - 1.0));
    					return m;
    				})
    				.collect(Collectors.toList());
    	} else {
    		entities = Collections.emptyList();
    	}
    }

    @Override
    public void undoAction(Level level) {
    	LevelTiles tiles = level.getTiles();
    	tiles.resize(oldWidth, oldHeight);

    	if (rightTiles != null)
    		setTilesWithOffset(rightTiles, tiles, width, 0);

    	if (bottomTiles != null)
    		setTilesWithOffset(bottomTiles, tiles, 0, height);

    	for (MovedEntity m: entities)
    		m.e.setPos(m.oldX, m.oldY);
    }

    private static void setTilesWithOffset(byte[][] raw, LevelTiles tiles, int xOff, int yOff) {
    	for (int i = 0; i < raw.length; i++) {
			for (int j = 0; j < raw[i].length; j++) {
				tiles.setRawAt(i + xOff, j + yOff, raw[i][j]);
			}
		}
    }

    private static void fetchTilesWithOffset(byte[][] raw, LevelTiles tiles, int xOff, int yOff) {
    	for (int i = 0; i < raw.length; i++) {
			for (int j = 0; j < raw[i].length; j++) {
				raw[i][j] = tiles.getRawAt(i + xOff, j + yOff);
			}
		}
    }

    private static class MovedEntity {
    	public Entity e;
    	public double oldX, oldY;
    }
}
