package net.eternalfest.editor.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Entity;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.models.LevelLayer;

public class DeleteAllEntities extends LevelAction {
    private List<Info> entities = new ArrayList<>();
    private Entity selected;

    private static class Info {
        Entity entity;
        LevelLayer layer;
    }

    public DeleteAllEntities() {
        super(MODIFY_ENTITY_LIST);
    }

    @Override
    public void doAction(Level level) {
        selected = level.getSelected();
        entities = level.allEntities().map(e -> {
            Info info = new Info();
            info.entity = e;
            info.layer = level.getEntityLayer(e);
            return info;
        }).collect(Collectors.toList());

        level.clearAllLayers();
    }

    @Override
    public void undoAction(Level level) {
        for(Info info: entities)
            level.addEntity(info.entity, info.layer);

        level.setSelected(selected);
    }

}
