package net.eternalfest.editor.actions;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Entity;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.models.LevelLayer;
import net.eternalfest.hfestlib.models.sprites.Sprite;

public class AddEntity extends LevelAction {

    private final double x, y;
    private final LevelLayer layer;
    private final Entity entity;

    public AddEntity(Entity entity, double x, double y, LevelLayer layer) {
        super(MODIFY_ENTITY_LIST);
        this.entity = entity;
        this.x = x;
        this.y = y;
        this.layer = layer;
    }

    public AddEntity(Sprite sprite, double x, double y, LevelLayer layer) {
        this(new Entity(sprite), x, y, layer);
    }

    @Override
    public void doAction(Level level) {
        entity.setPos(x, y);
        level.addEntity(entity, layer);
    }

    @Override
    public void undoAction(Level level) {
        level.removeEntity(entity);
    }
}
