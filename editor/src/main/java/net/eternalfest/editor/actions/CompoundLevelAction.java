package net.eternalfest.editor.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Level;

public class CompoundLevelAction extends LevelAction {

    private List<LevelAction> actions = new ArrayList<>(1);

    public CompoundLevelAction(List<LevelAction> actions) {
        super(computeModified(actions));
        this.actions = actions;
    }

    public CompoundLevelAction(LevelAction... actions) {
        this(Arrays.asList(actions));
    }

    @Override
    public void undoAction(Level level) {
        for (int i = actions.size() - 1; i >= 0; i--)
            actions.get(i).undoAction(level);
    }

    @Override
    public void doAction(Level level) {
        for (int i = 0; i < actions.size(); i++)
            actions.get(i).doAction(level);
    }

    private static int computeModified(List<LevelAction> actions) {
        int modified = 0;
        for(LevelAction action: actions) {
            modified |= action.getModified();
        }
        return modified;
    }

}
