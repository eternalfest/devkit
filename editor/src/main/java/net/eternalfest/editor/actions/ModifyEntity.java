package net.eternalfest.editor.actions;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Entity;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.models.LevelLayer;
import net.eternalfest.hfestlib.models.sprites.IAssetFrame;

public class ModifyEntity extends LevelAction {
    Entity entity;
    IAssetFrame oldFrame, newFrame;
    LevelLayer.Pos oldPos;
    LevelLayer newLayer;

    public ModifyEntity(Entity entity, IAssetFrame img, LevelLayer layer) {
        super(MODIFY_ENTITIES);
        this.entity = entity;
        newFrame = img;
        newLayer = layer;
    }

    @Override
    public void doAction(Level level) {
        oldFrame = entity.getFrame();
        oldPos = level.getEntityInfo(entity);

        entity.setFrame(newFrame);
        if(newLayer != oldPos.layer && level.removeEntity(entity) != null) {
            level.addEntity(entity, newLayer);
        }
    }

    @Override
    public void undoAction(Level level) {
        entity.setFrame(oldFrame);
        if(newLayer != oldPos.layer && level.removeEntity(entity) != null) {
            level.addEntityAt(entity, oldPos.layer, oldPos.index);
        }
    }

}
