package net.eternalfest.editor.actions;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.models.sprites.TileType;

public class SetTilesType extends LevelAction {

    private boolean isHorizontal;
    private TileType oldPf, newPf;

    public SetTilesType(TileType pf, boolean horizontal) {
        super(MODIFY_SKINS);
        newPf = pf;
        isHorizontal = horizontal;
    }

    @Override
    public void undoAction(Level level) {
        if (isHorizontal)
            level.getTiles().setTileTypeH(oldPf);
        else level.getTiles().setTileTypeV(oldPf);
    }

    @Override
    public void doAction(Level level) {
        if (isHorizontal) {
            oldPf = level.getTiles().getTileTypeH();
            level.getTiles().setTileTypeH(newPf);
        } else {
            oldPf = level.getTiles().getTileTypeV();
            level.getTiles().setTileTypeV(newPf);
        }
    }

}
