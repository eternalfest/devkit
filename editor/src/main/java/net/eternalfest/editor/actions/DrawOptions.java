package net.eternalfest.editor.actions;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Level;

public class DrawOptions extends LevelAction {
    private int oldVal;
    private final int options;
    private final boolean value;
    private final boolean replace;

    public DrawOptions(int setOptions) {
        super(MODIFY_METADATA);
        this.options = setOptions;
        this.value = false;
        this.replace = true;
    }

    public DrawOptions(int options, boolean value) {
        super(MODIFY_METADATA);
        this.options = options;
        this.value = value;
        this.replace = false;
    }


    @Override
    public void doAction(Level level) {
        oldVal = level.getDrawOptions();
        if (replace)
            level.setDrawOptions(options);
        else level.addDrawOptions(options, value);
    }

    @Override
    public void undoAction(Level level) {
        level.setDrawOptions(oldVal);
    }
}
