package net.eternalfest.editor.actions;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Level;

public class FlipLevelV extends LevelAction {

    public FlipLevelV() {
        super(MODIFY_ENTITIES | MODIFY_TILES);
    }

    @Override
    public void doAction(Level level) {
        level.getTiles().reverseV();
        level.allEntities()
            .forEach(e -> e.setPos(e.getX(), level.getHeight() - 1 - e.getY()));
    }

    @Override
    public void undoAction(Level level) {
        doAction(level);
    }

}
