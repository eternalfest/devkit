package net.eternalfest.editor.actions;

import javax.swing.undo.UndoableEdit;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Entity;
import net.eternalfest.hfestlib.models.Level;

public class SelectEntity extends LevelAction {

    private Entity entity;
    private Entity prev;

    public SelectEntity(Entity entity) {
        super(MODIFY_ENTITIES | NO_STATUS_UPDATE);
        this.entity = entity;
    }

    @Override
    public void doAction(Level level) {
        prev = level.getSelected();
        level.setSelected(entity);
    }

    @Override
    public void undoAction(Level level) {
        level.setSelected(prev);
    }

    @Override
    public boolean isSignificant() {
        return false;
    }

    @Override
    protected boolean tryMerge(UndoableEdit edit, Level level) {
        if(!(edit instanceof SelectEntity))
            return false;
        entity = ((SelectEntity) edit).entity;
        return true;
    }

}
