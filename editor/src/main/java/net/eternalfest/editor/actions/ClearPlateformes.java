package net.eternalfest.editor.actions;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.models.LevelTiles;

public class ClearPlateformes extends LevelAction {
    byte[][] pfs;

    public ClearPlateformes() {
        super(MODIFY_TILES);
    }

    @Override
    public void doAction(Level level) {
        LevelTiles tiles = level.getTiles();
        pfs = new byte[tiles.getWidth()][tiles.getHeight()];

        for (int i = 0; i < tiles.getWidth(); i++) {
            for (int j = 0; j < tiles.getHeight(); j++) {
                pfs[i][j] = tiles.getRawAt(i, j);
            }
        }

        tiles.clear();
    }

    @Override
    public void undoAction(Level level) {
        LevelTiles tiles = level.getTiles();
        for (int i = 0; i < tiles.getWidth(); i++) {
            for (int j = 0; j < tiles.getHeight(); j++) {
            	if (pfs[i][j] != 0)
            		tiles.setRawAt(i, j, pfs[i][j]);
            }
        }
    }
}
