package net.eternalfest.editor.actions;

import javax.swing.undo.UndoableEdit;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.models.LevelTiles;
import net.eternalfest.hfestlib.models.sprites.FieldType;

public class TileAction extends LevelAction {
    private byte[][] oldPfs = null, newPfs = null;
    private int posX, posY;
    private byte old;
    private FieldType type; //null = plateformes
    private boolean add;

    public TileAction(int x, int y, boolean plateforme) {
        super(MODIFY_TILES);
        posX = x;
        posY = y;
        type = null;
        add = plateforme;
    }

    public TileAction(int x, int y, FieldType type) {
        this(x, y, false);
        this.type = type;
    }

    @Override
    public void doAction(Level level) {
        LevelTiles tiles = level.getTiles();
        if (!level.isInBounds(posX, posY))
            return;

        if (newPfs != null) {
            doWithArray(level);
            return;
        }

        old = tiles.getRawAt(posX, posY);

        if (type == null) {
            if (add)
                tiles.addTileAt(posX, posY);
            else {
                tiles.removeAt(posX, posY);
            }
        } else {
            tiles.addFieldAt(posX, posY, type);
        }
    }

    private void doWithArray(Level level) {
        LevelTiles tiles = level.getTiles();
        for (int i = 0; i < newPfs.length; i++) {
            for (int j = 0; j < newPfs[i].length; j++) {
                tiles.setRawAt(i, j, newPfs[i][j]);
            }
        }
    }

    @Override
    public void undoAction(Level level) {
        LevelTiles tiles = level.getTiles();
        if (!level.isInBounds(posX, posY))
            return;

        tiles.setRawAt(posX, posY, old);

        if (oldPfs != null)
            undoWithArray(level);
    }

    private void undoWithArray(Level level) {
        LevelTiles tiles = level.getTiles();
        for (int i = 0; i < oldPfs.length; i++) {
            for (int j = 0; j < oldPfs[i].length; j++) {
                tiles.setRawAt(i, j, oldPfs[i][j]);
            }
        }
    }

    @Override
    public boolean tryMerge(UndoableEdit edit, Level level) {
        LevelTiles tiles = level.getTiles();
        if (!(edit instanceof TileAction))
            return false;

        TileAction action = (TileAction) edit;
        if (!level.isInBounds(action.posX, action.posY))
        	return true;

        if (!level.isInBounds(posX, posY) || (action.posX == posX && action.posY == posY)) {
        	this.posX = action.posX;
        	this.posY = action.posY;
        	this.type = action.type;
        	this.add = action.add;
        	return true;
        }

        if (oldPfs == null) {
        	int width = level.getWidth(), height = level.getHeight();
            oldPfs = new byte[width][height];
            newPfs = new byte[width][height];

            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    oldPfs[i][j] = tiles.getRawAt(i, j);
                    newPfs[i][j] = oldPfs[i][j];
                }
            }

            oldPfs[posX][posY] = old;
            oldPfs[action.posX][action.posY] = action.old;
        }

        newPfs[action.posX][action.posY] = tiles.getRawAt(action.posX, action.posY);

        return true;
    }
}
