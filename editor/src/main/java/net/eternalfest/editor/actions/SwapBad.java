package net.eternalfest.editor.actions;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.models.LevelLayer;

public class SwapBad extends LevelAction {

    private int i, j;

    public SwapBad(int i, int j) {
        super(MODIFY_ENTITY_LIST);
        this.i = i;
        this.j = j;
    }

    @Override
    public void doAction(Level level) {
        level.swapEntities(LevelLayer.FRUITS_ORDER, i, j);
    }

    @Override
    public void undoAction(Level level) {
        doAction(level);
    }

}
