package net.eternalfest.editor.actions;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.models.sprites.BackgroundType;

public class SetBackground extends LevelAction {
    private BackgroundType oldF, newF;

    public SetBackground(BackgroundType fond) {
        super(MODIFY_SKINS);
        newF = fond;
    }

    @Override
    public void undoAction(Level level) {
        level.setBackground(oldF);
    }

    @Override
    public void doAction(Level level) {
        oldF = level.getBackground();
        level.setBackground(newF);
    }
}
