package net.eternalfest.editor.actions;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Level;

public class FlipLevelH extends LevelAction {

    public FlipLevelH() {
        super(MODIFY_ENTITIES | MODIFY_TILES);
    }

    @Override
    public void doAction(Level level) {
        level.getTiles().reverseH();
        level.allEntities()
            .forEach(e -> e.setPos(level.getWidth() - 1 - e.getX(), e.getY()));
    }

    @Override
    public void undoAction(Level level) {
        doAction(level);
    }

}
