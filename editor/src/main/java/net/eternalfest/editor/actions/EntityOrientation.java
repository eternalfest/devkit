package net.eternalfest.editor.actions;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Entity;
import net.eternalfest.hfestlib.models.Level;

public class EntityOrientation extends LevelAction {

    private Entity entity;
    private int old;

    public EntityOrientation(Entity entity) {
        super(MODIFY_ENTITIES);
        this.entity = entity;
    }

    @Override
    public void doAction(Level level) {
        old = entity.getOrientation();
        entity.cycleOrientation();
    }

    @Override
    public void undoAction(Level level) {
        entity.setOrientation(old);
    }


}
