package net.eternalfest.editor.actions;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Entity;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.models.LevelLayer;

public class DeleteEntity extends LevelAction {
    private final Entity sprite;
    private boolean selected;
    private LevelLayer.Pos pos;

    public DeleteEntity(Entity sprite) {
        super(MODIFY_ENTITY_LIST);
        this.sprite = sprite;
    }

    @Override
    public void doAction(Level level) {
        selected = sprite == level.getSelected();
        pos = level.removeEntity(sprite);
    }

    @Override
    public void undoAction(Level level) {
        level.addEntityAt(sprite, pos.layer, pos.index);

        if (selected)
            level.setSelected(sprite);
    }

}
