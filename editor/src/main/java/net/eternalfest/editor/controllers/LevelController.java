package net.eternalfest.editor.controllers;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.undo.UndoManager;

import net.eternalfest.editor.views.LevelView;
import net.eternalfest.hfestlib.assets.Assets;
import net.eternalfest.hfestlib.models.Entity;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.tools.ResourceManager;
import net.eternalfest.hfestlib.tools.ShowableException;
import net.eternalfest.hfestlib.utils.FileUtils;
import org.jdom2.Document;
import org.jdom2.Element;

public class LevelController implements ILevelController {
    public static final String NAME_VIEW = "HammerfestLevelEditor";
    public static final String FILE_EXTENSION = "lvl";
    public static final String IMAGE_EXTENSION = "png";

    private Assets resourcesDefault;
    private Assets resources;
    private Level level;
    private Path levelFile;
    private LevelView view;

    private boolean modified = false;
    private boolean isDisposed = false;

    private UndoManager actionList = new UndoManager();


    public LevelController(Assets resources) {
        this.resourcesDefault = resources;
        this.resources = resources;
        actionList.setLimit(1000);

        level = makeDefaultLevel();
        view = new LevelView(this);
    }

    @Override
    public void showGUI() {
        view.setVisible(true);
    }

    private void reloadGUI() {
        actionList.discardAllEdits();
        view.dispose();
        view = new LevelView(this);
        view.levelChanged(level, levelFile);
        view.setModifiedStatus(modified);
        showGUI();
    }

    @Override
    public void reloadAssets() {
        view.dispose();
        ResourceManager.clearRegistry();
        resourcesDefault = null;
        if(levelFile == null) {
            resources = ResourceManager.get(resources.getRootPaths());
            reloadGUI();
        } else {
            resources = null;
            loadLevel(levelFile);
        }
    }

    @Override
    public Assets getAssetsForLevel(Path file) {
        try {
            if(file != null)
                return ResourceManager.getFromAmbientConfig(file);
        } catch (ShowableException e) {
            e.show();
        }
        if(resourcesDefault == null)
            resourcesDefault = ResourceManager.getDefault();
        return resourcesDefault;
    }

    @Override
    public LevelView getView() {
        return view;
    }

    @Override
    public Level getLevel() {
        return level;
    }

    @Override
    public Assets getAssets() {
        return resources;
    }

    @Override
    public boolean isModified() {
        return modified;
    }

    @Override
    public void setModified(boolean b) {
        if (modified != b)
            setModifiedAlways(b);
    }

    public void setModifiedAlways(boolean b) {
        modified = b;
        view.setModifiedStatus(b);
    }

    @Override
    public Path getLevelFile() {
        return levelFile;
    }

    @Override
    public void setLevel(Level lvl) {
        level = lvl == null ? makeDefaultLevel() : lvl;
        levelFile = null;
        view.levelChanged(level, null);
        setModifiedAlways(false);
        actionList.discardAllEdits();
    }

    private static Level makeDefaultLevel() {
    	return new Level(Level.DEFAULT_WIDTH, Level.DEFAULT_HEIGHT, "Sans titre", "");
    }

    @Override
    public boolean saveLevel(Path levelPath) {
        if (levelPath == null)
            return false;

        if(levelPath.getNameCount() > 0) {
            String name = levelPath.getName(levelPath.getNameCount() - 1).toString();
            if(name.endsWith("."))
                name = name.substring(0, name.length() - 1);

            if (!name.endsWith("." + FILE_EXTENSION))
                name += "." + FILE_EXTENSION;
            levelPath = levelPath.resolveSibling(name);
        }

        try {
            Assets newResources = resources;
            if(!levelPath.equals(levelFile)) {
                newResources = getAssetsForLevel(levelPath);
            }

            FileUtils.saveXML(levelPath, level.getXMLDocument());

            levelFile = levelPath;
            setModifiedAlways(false);
            if(newResources != resources) {
                resources = newResources;
                reloadGUI();
            }

            return true;

        } catch (ShowableException e) {
            e.show();
            return false;
        }
    }

    @Override
    public void loadLevel(Path levelPath) {
        Document levelXML;
        if (levelPath != null) {
            try {
                levelXML = FileUtils.loadXML(levelPath);
            } catch (ShowableException e) {
                levelXML = new Document(new Element("level"));
                e.show();
            }

            levelFile = levelPath;
            Assets newResources = getAssetsForLevel(levelPath);
            level.loadByXMLDocument(levelXML, newResources);
            level.setSelected(null);
            actionList.discardAllEdits();
            setModifiedAlways(false);
            // If resources have changed, reload editor window
            if (newResources != resources) {
                resources = newResources;
                reloadGUI();
            } else {
                view.levelChanged(level, levelFile);
            }
        }
    }

    @Override
    public void exportToImage(Path levelPath) {
        if (levelPath != null) {
            String path = levelPath.toAbsolutePath().toString();
            if (!path.endsWith("." + IMAGE_EXTENSION)) {
                path += "." + IMAGE_EXTENSION;
                levelPath = Paths.get(path);
            }

            exportLevelToImage(level, levelPath);
        }
    }

    public static void exportLevelToImage(Level level, Path levelPath) {
        BufferedImage image = new BufferedImage(level.getPixelWidth(), level.getPixelHeight(), BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g = image.createGraphics();
        Entity selected = level.getSelected();
        level.setSelected(null);
        level.draw(g, null);
        level.setSelected(selected);
        FileUtils.saveImage(levelPath, image);
    }

    public static String exportLevelToCode(Level level) throws ShowableException {
        try {
            return (String) Class.forName("net.eternalfest.hfestlib.encode.LevelEncoder")
                .getMethod("encode", Level.class)
                .invoke(null, level);
        } catch(InvocationTargetException ex) {
            if(ex.getTargetException() instanceof ShowableException)
                throw (ShowableException) ex.getTargetException();
        } catch(IllegalAccessException | IllegalArgumentException | NoSuchMethodException
                | SecurityException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        throw ShowableException.getExceptionWithMessage("<html>Couldn't export level code!</html>");
    }

    @Override
    public void setScript(String str, boolean isXML) {
        level.setScript(str);
        level.setScriptType(isXML);
        setModified(true);
    }

    @Override
    public void setName(String str) {
        level.setName(str);
        setModified(true);
    }

    @Override
    public void setDescription(String str) {
        level.setDescription(str);
        setModified(true);
    }

    @Override
    public void doAction(LevelAction action) {
        action.attachController(this);
        action.redo();
        actionList.addEdit(action);
    }

    @Override
    public void undoAction() {
        if (actionList.canUndo())
            actionList.undo();
    }

    @Override
    public void redoAction() {
        if (actionList.canRedo())
            actionList.redo();
    }

    @Override
    public void notifyView(int changes) {
        view.levelUpdated(changes);
        if(changes != 0 && (changes & LevelAction.NO_STATUS_UPDATE) == 0)
            setModified(true);
        view.setUndoEnabled(actionList.canUndo());
        view.setRedoEnabled(actionList.canRedo());
    }


    @Override
    public void dispose() {
        if (isDisposed)
            return;

        isDisposed = true;
        actionList.discardAllEdits();
        level.flush();
        view.dispose();
    }

    @Override
    public boolean isDisposed() {
        return isDisposed;
    }
}
