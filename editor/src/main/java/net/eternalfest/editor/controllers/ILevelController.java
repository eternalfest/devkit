package net.eternalfest.editor.controllers;


import java.nio.file.Path;

import net.eternalfest.editor.views.LevelView;
import net.eternalfest.hfestlib.assets.Assets;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.tools.ShowableException;


public interface ILevelController {

    public LevelView getView();

    public Level getLevel();

    public Assets getAssets();

    public Assets getAssetsForLevel(Path file);

    public void reloadAssets();

    public void showGUI();

    public boolean isModified();

    public void setModified(boolean b);

    public boolean saveLevel(Path levelPath);

    public void loadLevel(Path levelPath);

    public void exportToImage(Path levelPath);

    public Path getLevelFile();

    public void setLevel(Level lvl);

    public void setName(String str);

    public void setDescription(String str);

    public void setScript(String str, boolean isXML) throws ShowableException;

    public void doAction(LevelAction action);

    public void undoAction();

    public void redoAction();

    public void notifyView(int changes);

    public void dispose();

    public boolean isDisposed();

}
