package net.eternalfest.editor.controllers;


import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;

import net.eternalfest.hfestlib.models.Level;

public abstract class LevelAction implements UndoableEdit {
    public static final int MODIFY_NONE = 0;
    public static final int MODIFY_SKINS = 1 << 0;
    public static final int MODIFY_TILES = 1 << 1;
    public static final int MODIFY_ENTITIES = 1 << 2;
    public static final int MODIFY_ENTITY_LIST = 1 << 3;
    public static final int MODIFY_METADATA = 1 << 4;
    public static final int NO_STATUS_UPDATE = 1 << 31;
    public static final int MODIFY_ALL = -1 ^ NO_STATUS_UPDATE;

    private boolean canMerge = true;
    private int modified;

    private LevelController controller = null;


    protected LevelAction(int modified) {
        this.modified = modified;
    }

    public static LevelAction separator() {
        return new ActionSeparator();
    }

    public int getModified() {
        return modified;
    }

    public void attachController(LevelController controller) {
        if (this.controller == null)
            this.controller = controller;
    }

    @Override
    public void undo() throws CannotUndoException {
        if(controller == null)
            throw new IllegalStateException("Can't undo action with null controller!");
        if (!canUndo())
            throw new CannotUndoException();

        undoAction(controller.getLevel());
        notifyView();
    }


    @Override
    public boolean canUndo() {
        return true;
    }

    @Override
    public void redo() {
        if(controller == null)
            throw new IllegalStateException("Can't redo action with null controller!");
        doAction(controller.getLevel());
        notifyView();
    }


    @Override
    public final boolean canRedo() {
        return true;
    }

    private void notifyView() {
        controller.notifyView(modified);
    }

    public abstract void undoAction(Level level);

    public abstract void doAction(Level level);

    @Override
    public void die() {
    }

    @Override
    public final boolean addEdit(UndoableEdit anEdit) {
        if (anEdit instanceof ActionSeparator) {
            canMerge = false;
            return true;
        }

        if (canMerge)
            return tryMerge(anEdit, controller.getLevel());

        return false;
    }

    protected boolean tryMerge(UndoableEdit anEdit, Level level) {
        return false;
    }

    @Override
    public final boolean replaceEdit(UndoableEdit anEdit) {
        return false;
    }


    @Override
    public boolean isSignificant() {
        return true;
    }

    @Override
    public String getPresentationName() {
        return "";
    }

    @Override
    public String getUndoPresentationName() {
        return "Annuler";
    }


    @Override
    public String getRedoPresentationName() {
        return "Refaire";
    }


    private static class ActionSeparator extends LevelAction {
        public ActionSeparator() {
            super(MODIFY_NONE);
        }

        @Override
        public void undoAction(Level level) {
        }

        @Override
        public void doAction(Level level) {
        }

        @Override
        protected boolean tryMerge(UndoableEdit anEdit, Level level) {
            return anEdit instanceof ActionSeparator;
        }
    }

}
