package net.eternalfest.editor.views;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.function.BiFunction;
import java.util.function.Function;

import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;

import net.eternalfest.hfestlib.models.sprites.IAssetFrame;
import net.eternalfest.hfestlib.models.sprites.IThumbnail;

@FunctionalInterface
public interface ListRenderer<C extends Component, E> {

    void modify(C component, E value, int index,
            boolean isSelected, boolean cellHasFocus);


    public default<F> ListRenderer<C, F> mapping(Function<F, E> mapper) {
        return (c, value, index, isSelected, cellHasFocus) -> {
            this.modify(c, mapper.apply(value), index, isSelected, cellHasFocus);
        };
    }

    @SafeVarargs
    public static<C extends Component, E> ListCellRenderer<E> compose(
            Class<C> component, ListCellRenderer<? super E> base, ListRenderer<? super C, ? super E>... renderers) {
        return new ListCellRenderer<E>() {
            @Override
            public Component getListCellRendererComponent(JList<? extends E> list, E value, int index,
                    boolean isSelected, boolean cellHasFocus) {
                Component c = base.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                C comp = component.cast(c);
                if(value == null)
                    return comp;
                for(ListRenderer<? super C, ? super E> r: renderers) {
                    if(r != null)
                        r.modify(comp, value, index, isSelected, cellHasFocus);
                }
                return comp;
            }
        };
    }

    @SuppressWarnings("unchecked")
    public static<E> ListCellRenderer<E> background(Color... colors) {
        Color[] array = colors.length > 0 ? colors : new Color[] {
                new Color(205, 205, 205),
                new Color(230, 230, 230)
            };
        return (ListCellRenderer<E>) new DefaultListCellRenderer() {
            private static final long serialVersionUID = -1826696194906061355L;
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index,
                    boolean isSelected, boolean cellHasFocus) {
                Component c = super.getListCellRendererComponent(list, value, index, cellHasFocus, false);
                c.setForeground(Color.DARK_GRAY);
                if(!cellHasFocus) {
                    c.setBackground(array[index % array.length]);
                }
                return c;
            }
        };
    }

    public static<E extends IThumbnail> ListRenderer<JLabel, E> assetIcon(int width, int height) {
        return (c, value, index, isSelected, cellHasFocus) -> {
            boolean customSize = width >= 0 && height >= 0;
            Icon icon = customSize ? new ThumbnailIcon(value, width, height) : new ThumbnailIcon((IAssetFrame) value);

            c.setHorizontalAlignment(SwingConstants.CENTER);
            c.setVerticalAlignment(SwingConstants.CENTER);
            c.setText(null);
            c.setIcon(icon);

            if (!customSize)
                c.setPreferredSize(new Dimension(icon.getIconWidth() + 4, icon.getIconHeight() + 4));
        };
    }

    public static<E extends IThumbnail> ListRenderer<JLabel, E> assetIcon(int size) {
        return assetIcon(size, size);
    }

    public static<E extends IAssetFrame> ListRenderer<JLabel, E> assetIcon() {
        return assetIcon(-1, -1);
    }

    public static<E> ListRenderer<JLabel, E> text(BiFunction<E, Integer, String> toText) {
        return (c, value, index, isSelected, cellHasFocus) -> {
          c.setHorizontalAlignment(SwingConstants.LEADING);
          c.setText(toText.apply(value, index));
        };
    }

    public static<E> ListRenderer<JLabel, E> text(Function<E, String> toText) {
        return text((value, index) -> toText.apply(value));
    }

    public static<E> ListRenderer<JLabel, E> tooltip(BiFunction<E, Integer, String> toText) {
        return (c, value, index, isSelected, cellHasFocus) -> c.setToolTipText(toText.apply(value, index));
    }

    public static<E> ListRenderer<JLabel, E> tooltip(Function<E, String> toText) {
        return tooltip((value, index) -> toText.apply(value));
    }

    public static<E> ListRenderer<JLabel, E> emphasisSelected() {
        return (c, value, index, isSelected, cellHasFocus) -> {
            if(isSelected) {
                c.setForeground(c.getForeground().darker());
                c.setFont(c.getFont().deriveFont(Font.BOLD));
            } else {
                c.setFont(c.getFont().deriveFont(Font.PLAIN));
            }
        };
    }
}
