package net.eternalfest.editor.views;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.eternalfest.hfestlib.assets.AssetsMap;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.models.sprites.BackgroundType;
import net.eternalfest.hfestlib.models.sprites.IAsset;

public class FondDialog extends JDialog {

    private static final long serialVersionUID = -3772438249837601319L;
    private JButton cancelButton;
    private boolean hasBeenCreated = false;
    private JList<BackgroundType> listFond = null;
    private JButton okButton;
    private JLabel preview = new JLabel();
    private boolean sendData = false;

    private AssetsMap<BackgroundType> resources;

    public FondDialog(JFrame parent, String title, AssetsMap<BackgroundType> resources, boolean modal) {
        super(parent, title, modal);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.resources = resources;
    }

    public BackgroundType showFondDialog(BackgroundType currentFond) {
        sendData = false;

        if (!hasBeenCreated)
            createDialog(currentFond);

        this.setLocationRelativeTo(null);
        this.setVisible(true);

        return sendData ? listFond.getSelectedValue() : currentFond;
    }

    public void createDialog(BackgroundType currentFond) {
        Container contentPane = this.getContentPane();

        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));

        listFond = new JList<BackgroundType>(resources.list().toArray(new BackgroundType[] {}));
        listFond.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listFond.setCellRenderer(ListRenderer.compose(JLabel.class,
                ListRenderer.background(),
                ListRenderer.assetIcon(80),
                ListRenderer.text(IAsset::getName),
                ListRenderer.tooltip(Formats.assetInfo(false, resources, "#bg.", null))
            ));
        listFond.setVisibleRowCount(4);
        listFond.setSelectedIndex(0);
        listFond.setSelectedValue(currentFond, true);


        preview.setIcon(createPreview(listFond.getSelectedValue()));
        preview.setAlignmentX(CENTER_ALIGNMENT);
        preview.setVerticalAlignment(SwingConstants.CENTER);
        preview.setMinimumSize(new Dimension(Level.DEFAULT_PIXEL_WIDTH + 10, Level.DEFAULT_PIXEL_HEIGHT + 10));

        JPanel listPanel = new JPanel();
        listPanel.add(preview);
        listPanel.add(new JScrollPane(listFond));

        contentPane.add(listPanel);

        okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                sendData = true;
                setVisible(false);
            }
        });

        cancelButton = new JButton("Annuler");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                sendData = false;
                setVisible(false);
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        contentPane.add(buttonPanel);


        listFond.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent arg0) {
                preview.setIcon(createPreview(listFond.getSelectedValue()));
            }
        });

        hasBeenCreated = true;

        this.pack();

    }

    public ImageIcon createPreview(BackgroundType type) {
        if (type != null) {
            BufferedImage icon = new BufferedImage(Level.DEFAULT_PIXEL_WIDTH / 2, Level.DEFAULT_PIXEL_HEIGHT / 2, BufferedImage.TYPE_4BYTE_ABGR);
            Graphics2D g = icon.createGraphics();
            g.setClip(0, 0, icon.getWidth(), icon.getHeight());
            g.scale(0.5, 0.5);
            type.draw(g, 0, 0, null);
            g.dispose();
            return new ImageIcon(icon);
        } else return null;
    }
}

