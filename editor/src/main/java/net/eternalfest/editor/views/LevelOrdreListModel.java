package net.eternalfest.editor.views;

import java.util.List;

import javax.swing.AbstractListModel;

import net.eternalfest.hfestlib.models.Entity;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.models.LevelLayer;

public class LevelOrdreListModel extends AbstractListModel<Entity> {
    private static final long serialVersionUID = 2003224980292868618L;

    private Level level;
    private List<Entity> list;
    private int lastSize;

    public LevelOrdreListModel(Level level) {
        this.level = level;
        list = level.getLayer(LevelLayer.FRUITS_ORDER);
        lastSize = list.size();
    }

    @Override
    public Entity getElementAt(int i) {
        return list.get(i);
    }

    @Override
    public int getSize() {
        return list.size();
    }

    public void updateModel() {
        list = level.getLayer(LevelLayer.FRUITS_ORDER);
        int size = list.size();
        if(lastSize < size)
            fireIntervalAdded(this, lastSize, size);
        else if(lastSize > size)
            fireIntervalRemoved(this, size, lastSize);
        fireContentsChanged(this, 0, size);
        lastSize = size;
    }
}
