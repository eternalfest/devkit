package net.eternalfest.editor.views;

import java.awt.Container;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import net.eternalfest.hfestlib.models.Level;

public class ResizeDialog extends JDialog {

	private static final long serialVersionUID = -8684585396849033036L;
	private JButton cancelButton;
    private JButton okButton;
    private boolean hasBeenCreated = false;
    private boolean sendData = false;
    private SpinnerNumberModel widthModel;
    private SpinnerNumberModel heightModel;
    private JLabel existingSize;

    public ResizeDialog(JFrame parent, String title, boolean modal) {
        super(parent, title, modal);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
    }

    public Dimension showResizeDialog(Dimension curSize) {
        sendData = false;

        if (!hasBeenCreated)
            createDialog();

        widthModel.setValue(curSize.width);
        heightModel.setValue(curSize.height);
        existingSize.setText("Taille actuelle: " + curSize.width + "x" + curSize.height);

        this.setLocationRelativeTo(null);
        this.setVisible(true);

        return sendData ? new Dimension(widthModel.getNumber().intValue(), heightModel.getNumber().intValue()): null;
    }

    public void createDialog() {
        Container contentPane = this.getContentPane();

        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));

        existingSize = new JLabel("Taille actuelle: 999x999");
        existingSize.setAlignmentX(JLabel.LEFT_ALIGNMENT);
        existingSize.setBorder(BorderFactory.createEmptyBorder(2, 3, 2, 3));
        contentPane.add(existingSize);

        JPanel sizePanel = new JPanel();
        sizePanel.setLayout(new BoxLayout(sizePanel, BoxLayout.LINE_AXIS));
        sizePanel.setBorder(BorderFactory.createEmptyBorder(2, 3, 2, 3));
        sizePanel.setAlignmentX(JLabel.LEFT_ALIGNMENT);
        widthModel = new SpinnerNumberModel(1, 1, Level.MAX_WIDTH, 1);
        heightModel = new SpinnerNumberModel(1, 1, Level.MAX_HEIGHT, 1);
        sizePanel.add(new JLabel("Nouvelle taille: "));
        sizePanel.add(new JSpinner(widthModel));
        sizePanel.add(new JLabel("x"));
        sizePanel.add(new JSpinner(heightModel));

        contentPane.add(sizePanel);

        okButton = new JButton("OK");
        okButton.addActionListener(a -> {
            sendData = true;
            setVisible(false);
        });

        cancelButton = new JButton("Annuler");
        cancelButton.addActionListener(a -> {
            sendData = false;
            setVisible(false);
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);
        buttonPanel.setAlignmentX(JLabel.CENTER_ALIGNMENT);

        contentPane.add(buttonPanel);

        hasBeenCreated = true;
        this.pack();

    }
}

