package net.eternalfest.editor.views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

import javax.swing.JPanel;

import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.hfestlib.models.Level;


public class CenterPanel extends JPanel {

    //SERIAL ID
    private static final long serialVersionUID = -3091737081869419196L;

    //ATTRIBUTS

    private Level niv = null;
    private boolean isGrille = false;


    //CONSTRUCTEUR

    public CenterPanel() {}

    @Override
    public Dimension getPreferredSize() {
    	return new Dimension(
    		niv == null ? Level.DEFAULT_PIXEL_WIDTH : niv.getPixelWidth(),
    		niv == null ? Level.DEFAULT_PIXEL_HEIGHT : niv.getPixelHeight()
    	);
    }

    @Override
    public Dimension getMinimumSize() {
    	return getPreferredSize();
    }

    public Point getCoordOffset() {
        Dimension size = this.getSize();
    	int offsetX = (size.width - niv.getPixelWidth()) / 2;
        int offsetY = (size.height - niv.getPixelHeight()) / 2;
        return new Point(offsetX, offsetY < 7 ? 0 : offsetY);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (niv == null) return;

        Graphics2D g2d = (Graphics2D) g.create();
        Point offset = getCoordOffset();
	    g2d.translate(offset.x, offset.y);

        // TODO: limit to clip area
        niv.draw(g2d, this);

        //tracé de la grille
        if (isGrille) {
        	// TODO: limit to clip area
            g2d.setColor(new Color(150, 0, 255));
            for (int i = 0; i < niv.getWidth(); i++) {
                for (int j = 0; j < niv.getHeight(); j++)
                    g2d.drawRect(i * Level.TILES_SIZE + Level.TILES_SIZE / 2, j * Level.TILES_SIZE, Level.TILES_SIZE - 1, Level.TILES_SIZE - 1);
            }
        }

        g2d.dispose();
    }

    public void setLevel(Level level) {
        niv = level;
        this.setPreferredSize(new Dimension(level.getPixelWidth(), level.getPixelHeight()));
        this.setMinimumSize(this.getPreferredSize());
        repaint();
    }

    public LevelAction getActionGrille(boolean show) {
        return new LevelAction(LevelAction.MODIFY_NONE) {
            @Override
            public void undoAction(Level level) {
                setGrille(!show);
            }

            @Override
            public void doAction(Level level) {
                setGrille(show);
            }
        };
    }

    public boolean isGrille() {
        return isGrille;
    }

    public void setGrille(boolean show) {
        isGrille = show;
        repaint();
    }
}
