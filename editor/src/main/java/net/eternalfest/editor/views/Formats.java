package net.eternalfest.editor.views;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collectors;

import net.eternalfest.hfestlib.assets.AssetsMap;
import net.eternalfest.hfestlib.models.sprites.IAsset;

public enum Formats {
    ;

    public static<T extends IAsset> Function<T, String> assetInfo(
    		boolean showName,
    		AssetsMap<T> assets,
    		String prefix,
    		String strip
    ) {
    	String prefix2 = prefix == null ? "" : prefix;
        return asset -> assets
        	.aliasesOf(asset.getID()).stream()
        	.map(s -> {
        		if (strip == null) {
        			return prefix2 + s;
        		} else if (s.startsWith(strip)) {
					return prefix2 + s.substring(strip.length());
				} else {
					return s;
				}
			})
			.collect(Collectors.joining(", ", showName ? asset.getName() + "  [" : "[", "]"));
    }

    public static class Position extends Format {
        private static final long serialVersionUID = 481986970254563263L;

        private final DecimalFormat inner;
        private final String decimalSep;

        private Position(DecimalFormat inner) {
            this.inner = inner;
            this.decimalSep = "" + inner.getDecimalFormatSymbols().getDecimalSeparator();
        }

        @Override
        public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
            if (!(obj instanceof Number))
                throw new IllegalArgumentException("Cannot format given Object as a Number");

            double d = ((Number) obj).doubleValue();
            inner.format(d, toAppendTo, pos);

            if(Math.round(d) == d)
                deleteFractPart(toAppendTo);

            return toAppendTo;
        }

        @Override
        public Object parseObject(String source, ParsePosition pos) {
            return inner.parseObject(source, pos);
        }

        private void deleteFractPart(StringBuffer buffer) {
            buffer.delete(buffer.lastIndexOf(decimalSep), buffer.length());
        }

        public static Position from(Locale locale, int decimals) {
            DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance(locale);
            format.applyPattern("0.00");
                return new Position(format);
        }
    }
}
