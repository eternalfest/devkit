package net.eternalfest.editor.views;

import java.nio.file.Path;

import net.eternalfest.hfestlib.models.Level;

public interface ILevelView {
    public void setVisible(boolean b);

    public void setUndoEnabled(boolean v);

    public void setRedoEnabled(boolean v);

    public void setModifiedStatus(boolean modified);

    public void levelUpdated(int changes);

    public void levelChanged(Level level, Path file);

    public void dispose();
}
