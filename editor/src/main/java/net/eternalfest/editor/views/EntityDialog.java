package net.eternalfest.editor.views;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.eternalfest.hfestlib.assets.AssetsMap;
import net.eternalfest.hfestlib.models.Entity;
import net.eternalfest.hfestlib.models.LevelLayer;
import net.eternalfest.hfestlib.models.sprites.IAsset;
import net.eternalfest.hfestlib.models.sprites.IAssetFrame;

public class EntityDialog extends JDialog {

    private static final long serialVersionUID = 1311104166030513255L;
    private JButton cancelButton;
    private boolean hasBeenCreated = false;
    private JList<IAssetFrame> listFrames = null;
    private JButton okButton;
    private JRadioButton[] listChoices;
    private JLabel preview = new JLabel();
    private boolean sendData = false;
    private JPanel choicesPanel;
    private IAssetFrame[] framesTab;

    private AssetsMap<IAssetFrame> resources;

    public EntityDialog(JFrame parent, String title, AssetsMap<IAssetFrame> resources, boolean modal) {
        super(parent, title, modal);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.resources = resources;
    }

    public EntityDialog.Result showSpriteDialog(Entity entity, LevelLayer layer) {
        framesTab = entity.getSprite().getFrames().toArray(new IAssetFrame[] {});
        sendData = false;

        IAssetFrame currentImage = entity.getFrame();

        if (!hasBeenCreated)
            createDialog();

        listFrames.setListData(framesTab);
        listFrames.setSelectedValue(currentImage, true);
        if (layer == LevelLayer.BACKGROUND) {
            choicesPanel.setVisible(true);
            listChoices[0].setSelected(true);
        } else if (layer == LevelLayer.FOREGROUND) {
            choicesPanel.setVisible(true);
            listChoices[1].setSelected(true);
        } else {
            choicesPanel.setVisible(false);
        }

        this.setLocationRelativeTo(null);
        this.setVisible(true);

        EntityDialog.Result result = new EntityDialog.Result();

        result.spriteFrame = sendData ? listFrames.getSelectedValue() : currentImage;
        result.layer = layer;
        if(choicesPanel.isVisible())
            result.layer = listChoices[0].isSelected() ? LevelLayer.BACKGROUND : LevelLayer.FOREGROUND;

        return result;
    }

    public void createDialog() {
        Container contentPane = this.getContentPane();

        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));


        listFrames = new JList<IAssetFrame>();
        listFrames.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listFrames.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        listFrames.setCellRenderer(ListRenderer.compose(JLabel.class,
                ListRenderer.background(new Color(0, 0, 0, 0)),
                ListRenderer.assetIcon(),
                ListRenderer.tooltip(IAsset::getName)
            ));
        listFrames.setVisibleRowCount(-1);
        listFrames.setSelectedIndex(0);

        JScrollPane listScriptPane = new JScrollPane(listFrames);
        listScriptPane.setPreferredSize(new Dimension(400, 200));

        contentPane.add(listScriptPane);

        contentPane.add(Box.createVerticalStrut(5));

        preview.setText(getSpriteName(listFrames.getSelectedValue()));
        preview.setVerticalTextPosition(SwingConstants.TOP);
        preview.setHorizontalTextPosition(SwingConstants.CENTER);
        preview.setVerticalAlignment(SwingConstants.CENTER);
        preview.setAlignmentX(CENTER_ALIGNMENT);

        contentPane.add(preview);

        contentPane.add(Box.createVerticalStrut(5));


        listChoices = new JRadioButton[2];

        listChoices[0] = new JRadioButton("En arrière-plan");
        listChoices[0].setMnemonic(KeyEvent.VK_1);
        listChoices[1] = new JRadioButton("En avant-plan");
        listChoices[1].setMnemonic(KeyEvent.VK_2);

        ButtonGroup buttons = new ButtonGroup();
        buttons.add(listChoices[0]);
        buttons.add(listChoices[1]);

        choicesPanel = new JPanel();

        choicesPanel.add(listChoices[0]);
        choicesPanel.add(listChoices[1]);
        contentPane.add(choicesPanel);


        okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                sendData = true;
                setVisible(false);
            }
        });

        cancelButton = new JButton("Annuler");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                sendData = false;
                setVisible(false);
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        contentPane.add(buttonPanel);


        listFrames.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent arg0) {
                preview.setText(getSpriteName(listFrames.getSelectedValue()));
            }
        });

        this.pack();

        hasBeenCreated = true;

    }

    private String getSpriteName(IAssetFrame sprite) {
        if (sprite != null)
            return Formats.assetInfo(true, resources, null, null).apply(sprite);
        else return "Sprite inconnu";
    }

    public static class Result {
        public IAssetFrame spriteFrame;
        public LevelLayer layer;
    }
}
