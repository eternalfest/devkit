package net.eternalfest.editor.views;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.Icon;

import net.eternalfest.hfestlib.models.sprites.IAssetFrame;
import net.eternalfest.hfestlib.models.sprites.IThumbnail;

public class ThumbnailIcon implements Icon {
    private final IThumbnail thumb;
    private final int width;
    private final int height;

    public ThumbnailIcon(IThumbnail thumb, int width, int height) {
        this.thumb = thumb;
        this.width = width;
        this.height = height;
    }

    public ThumbnailIcon(IThumbnail thumb, Dimension size) {
        this(thumb, (int) size.getWidth(), (int) size.getHeight());
    }

    public ThumbnailIcon(IAssetFrame frame) {
        this(frame, frame.getWidth(), frame.getHeight());
    }

    @Override
    public int getIconHeight() {
        return height;
    }

    @Override
    public int getIconWidth() {
        return width;
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        if(thumb == null)
            return;
        Graphics2D g2d = (Graphics2D) g;
        Object aliasing = g2d.getRenderingHint(RenderingHints.KEY_RENDERING);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.translate(x, y);
        thumb.drawThumbnail(g2d, width, height, null);
        g.translate(-x, -y);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, aliasing);
    }
}
