package net.eternalfest.editor.views;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.eternalfest.hfestlib.assets.AssetsMap;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.models.sprites.IAsset;
import net.eternalfest.hfestlib.models.sprites.TileType;

public class PlateformeDialog extends JDialog {

    private static final long serialVersionUID = -5904447330061938065L;
    private JButton cancelButton;
    private boolean hasBeenCreated = false;
    private JList<TileType> listPlateforme = null;
    private JButton okButton;
    private JLabel preview = new JLabel();
    private boolean sendData = false;

    private AssetsMap<TileType> resources;

    public PlateformeDialog(JFrame parent, String title, AssetsMap<TileType> resources, boolean modal) {
        super(parent, title, modal);
        this.setSize(480, 405);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.resources = resources;
    }

    public TileType showPlateformeDialog(TileType currentPf) {
        sendData = false;

        if (!hasBeenCreated)
            createDialog(currentPf);

        this.validate();
        this.setVisible(true);

        return sendData ? listPlateforme.getSelectedValue() : currentPf;
    }

    public void createDialog(TileType currentPf) {
        Container contentPane = this.getContentPane();

        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));


        listPlateforme = new JList<TileType>(resources.list().toArray(new TileType[] {}));
        listPlateforme.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listPlateforme.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        listPlateforme.setCellRenderer(ListRenderer.compose(JLabel.class,
                ListRenderer.background(new Color(0, 0, 0, 0)),
                ListRenderer.assetIcon(90, 30),
                ListRenderer.tooltip(IAsset::getName)
            ));
        listPlateforme.setVisibleRowCount(0);
        listPlateforme.setSelectedIndex(0);
        listPlateforme.setSelectedValue(currentPf, true);

        contentPane.add(new JScrollPane(listPlateforme));

        preview.setIcon(createPreview(listPlateforme.getSelectedValue()));
        preview.setText(getPlateformeName(listPlateforme.getSelectedValue()));
        preview.setVerticalTextPosition(SwingConstants.TOP);
        preview.setHorizontalTextPosition(SwingConstants.CENTER);
        preview.setVerticalAlignment(SwingConstants.CENTER);
        preview.setAlignmentX(CENTER_ALIGNMENT);
        //preview.setMinimumSize(new Dimension(400, 60));

        contentPane.add(preview);

        okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                sendData = true;
                setVisible(false);
            }
        });

        cancelButton = new JButton("Annuler");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                sendData = false;
                setVisible(false);
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        contentPane.add(buttonPanel);


        listPlateforme.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent arg0) {
                preview.setIcon(createPreview(listPlateforme.getSelectedValue()));
                preview.setText(getPlateformeName(listPlateforme.getSelectedValue()));
            }
        });

        hasBeenCreated = true;

    }

    private Icon createPreview(TileType type) {
        if (type == null)
            return null;
        return new ThumbnailIcon(type, Level.TILES_SIZE * Level.DEFAULT_WIDTH, Level.TILES_SIZE);
    }

    private String getPlateformeName(TileType type) {
        if (type != null)
            return Formats.assetInfo(true, resources, "#tile.", null).apply(type);
        else return "Plateforme inconnue";
    }
}
