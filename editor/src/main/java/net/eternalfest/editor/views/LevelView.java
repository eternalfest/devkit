package net.eternalfest.editor.views;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Format;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;

import net.eternalfest.editor.actions.AddEntity;
import net.eternalfest.editor.actions.ClearPlateformes;
import net.eternalfest.editor.actions.CompoundLevelAction;
import net.eternalfest.editor.actions.DeleteAllEntities;
import net.eternalfest.editor.actions.DeleteEntity;
import net.eternalfest.editor.actions.DrawOptions;
import net.eternalfest.editor.actions.EntityOrientation;
import net.eternalfest.editor.actions.FlipLevelH;
import net.eternalfest.editor.actions.FlipLevelV;
import net.eternalfest.editor.actions.ModifyEntity;
import net.eternalfest.editor.actions.MoveEntity;
import net.eternalfest.editor.actions.ResizeLevel;
import net.eternalfest.editor.actions.SelectEntity;
import net.eternalfest.editor.actions.SetBackground;
import net.eternalfest.editor.actions.SetTilesType;
import net.eternalfest.editor.actions.ShiftLevel;
import net.eternalfest.editor.actions.SwapBad;
import net.eternalfest.editor.actions.TileAction;
import net.eternalfest.editor.controllers.LevelAction;
import net.eternalfest.editor.controllers.LevelController;
import net.eternalfest.hfestlib.assets.Assets;
import net.eternalfest.hfestlib.models.Entity;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.models.LevelLayer;
import net.eternalfest.hfestlib.models.sprites.FieldType;
import net.eternalfest.hfestlib.models.sprites.IAsset;
import net.eternalfest.hfestlib.models.sprites.IAssetFrame;
import net.eternalfest.hfestlib.models.sprites.Sprite;
import net.eternalfest.hfestlib.models.sprites.TileType;
import net.eternalfest.hfestlib.script.ScriptParseException;
import net.eternalfest.hfestlib.script.ScriptParser;
import net.eternalfest.hfestlib.tools.FileExtensionFilter;
import net.eternalfest.hfestlib.tools.GBC;
import net.eternalfest.hfestlib.utils.FileUtils;

public class LevelView extends JFrame implements ILevelView {

    private static final long serialVersionUID = -2171354941743627609L;
    private static final Format POSITION_FORMAT = PositionFormat.from(Locale.ENGLISH, 2);

    private final JPanel left = new JPanel();
    private final JPanel right = new JPanel();
    private final JPanel bottom = new JPanel();
    private final JLabel preview = new JLabel();
    private final JList<FieldType> listRayons = new JList<>();
    private final JList<Entity> listOrdre = new JList<>();
    private final JList<Sprite> listFruits = new JList<>();
    private final JList<Sprite> listSprites = new JList<>();
    private final JMenuBar menuBar = new JMenuBar();
    private final Map<String, Action> actionMap = new HashMap<>();
    private final JLabel posCaseLabel = new JLabel();
    private final JLabel posPixelLabel = new JLabel();
    private final JTextField levelName = new JTextField();
    private final JTextArea levelDesc = new JTextArea();
    private final CenterPanel center = new CenterPanel();
    private final JScrollPane centerScroll = new JScrollPane();
    private final PlateformeDialog pfDialog;
    private final FondDialog fondDialog;
    private final FileDialog fileDialog = new FileDialog(this);
    private final FileDialog imageDialog = new FileDialog(this);
    private final EntityDialog spriteDialog;
    private final ResizeDialog resizeDialog;

    private final ScriptFrame scriptFrame;
    private final WindowAdapter winAdapter;
    private final PlateformeListener pfListener;
    private LevelOrdreListModel listOrdreModel;

    private final Assets resources;
    private final LevelController controller;
    private Level level;
    private Sprite lastSelectedSprite = null;
    private boolean lastSelectedSpriteIsFruit = false;
    private Entity copiedEntity = null;
    private LevelLayer copiedEntityLayer = null;

    public LevelView(LevelController c) {
        controller = c;
        resources = controller.getAssets();

        scriptFrame = new ScriptFrame(c);

        pfDialog = new PlateformeDialog(this, "Choix de l'apparence des plateformes", resources.tiles(), true);
        fondDialog = new FondDialog(this, "Choix de l'apparence du fond", resources.backgrounds(), true);
        spriteDialog = new EntityDialog(this, "Modification du sprite", resources.frames(), true);
        resizeDialog = new ResizeDialog(this, "Redimensionner le niveau", true);
        initActions();
        initComponents();

        //ON PLACE LES ECOUTEURS
        pfListener = new PlateformeListener();
        center.setFocusable(true);
        center.addMouseListener(pfListener);
        center.addMouseMotionListener(pfListener);
        center.addKeyListener(pfListener);

        //Autres
        addListClickListener(listFruits, (idx, e) -> {
            lastSelectedSprite = e;
            lastSelectedSpriteIsFruit = true;
            getAction("ordre_add").setEnabled(lastSelectedSprite != null);
        });
        listFruits.clearSelection();

        addListClickListener(listSprites, (idx, e) -> {
            lastSelectedSprite = e;
            lastSelectedSpriteIsFruit = false;
            IAssetFrame framePreview = null;
            if(e != null)
                framePreview = e.getDefaultFrame();
            preview.setIcon(new ThumbnailIcon(framePreview, preview.getPreferredSize()));

            boolean enabled = idx >= 0;
            getAction("add_sprite").setEnabled(enabled);
            getAction("add_sprite_layer0").setEnabled(enabled);
            getAction("add_sprite_layer1").setEnabled(enabled);
        });
        listSprites.clearSelection();

        addListClickListener(listOrdre, (idx, e) -> {
            updateOrdreButtons();
            if(e != null && level.getSelected() != e)
                controller.doAction(new SelectEntity(e));
        });

        //ecouteur de la fermeture de la fenêtre
        winAdapter = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (confirmLevelClose()) {
                    controller.dispose();
                }
            }
        };
        this.addWindowListener(winAdapter);

        //ecouteur clavier global
        Toolkit.getDefaultToolkit().addAWTEventListener(new GlobalListener(), AWTEvent.KEY_EVENT_MASK);


        this.pack();
        this.setMinimumSize(this.getSize());

        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.setLocationRelativeTo(null);
        pfListener.mouseExited(null);
        center.requestFocusInWindow();

        this.pack(); // Pack *before* setting level to get default window size.
        levelChanged(controller.getLevel(), null);
        setTitle(null, false);
    }

    private static <T> void addListClickListener(JList<T> list, BiConsumer<Integer, T> listener) {
        list.addListSelectionListener(e -> {
            if(!e.getValueIsAdjusting())
                listener.accept(list.getSelectedIndex(), list.getSelectedValue());
        });

        list.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                listener.accept(list.getSelectedIndex(), list.getSelectedValue());
            }

            @Override
            public void focusLost(FocusEvent e) {
            }
        });
    }

    public static KeyStroke getMenuShortcut(int key) {
        return getMenuShortcut(key, 0);
    }

    public static KeyStroke getMenuShortcut(int key, int mask) {
        return KeyStroke.getKeyStroke(key, mask + getMenuShorcutMask());
    }

    public static int getMenuShorcutMask() {
        return Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();
    }

    @Override
    public void dispose() {
        scriptFrame.dispose();
        super.dispose();
    }

    private void initComponents() {
        fileDialog.setFilenameFilter(new FileExtensionFilter(LevelController.FILE_EXTENSION, "Niveau Hammerfest (*." + LevelController.FILE_EXTENSION + ")"));
        imageDialog.setFilenameFilter(new FileExtensionFilter(LevelController.IMAGE_EXTENSION, "Image PNG (*." + LevelController.IMAGE_EXTENSION + ")"));

        initMenuBar();
        initLeftPanel();
        initRightPanel();
        initBottomPanel();

        Container contentPane = this.getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(left, BorderLayout.WEST);
        contentPane.add(right, BorderLayout.EAST);
        contentPane.add(bottom, BorderLayout.SOUTH);

        centerScroll.setViewportView(center);
        centerScroll.setViewportBorder(BorderFactory.createEmptyBorder());
        contentPane.add(centerScroll, BorderLayout.CENTER);
    }

    private void initMenuBar() {
        menuBar.add(createMenu("Fichier",
            "file_open",
            "file_new",
            "",
            "file_save",
            "file_save_as",
            "",
            "file_export",
            "",
            "file_quit"
        ));

        menuBar.add(createMenu("Edition",
            "undo",
            "redo",
            "",
            "edition_copy",
            "edition_cut",
            "edition_paste",
            "edition_suppr",
            "",
            "plateformes_up",
            "plateformes_down",
            "plateformes_right",
            "plateformes_left",
            "",
            "edition_flip_h",
            "edition_flip_v",
            "edition_resize"
        ));

        menuBar.add(createMenu("Affichage",
            "show_script",
            "",
            "@show_grid",
            "@show_borders",
            "@show_sprites",
            "@show_bads",
            "@show_spawns",
            "@show_ordre",
            "",
            "show_default",
            "@show_bottom"
        ));

        menuBar.add(createMenu("Ressources",
            "show_current_assets",
            "reload_assets"
        ));

        this.setJMenuBar(menuBar);

    }

    private void initLeftPanel() {
        ////PARTIE GAUCHE////
        left.setBorder(BorderFactory.createEmptyBorder(10, 15, 0, 15));
        left.setLayout(new BoxLayout(left, BoxLayout.PAGE_AXIS));

        //AJOUT DES BOUTONS DE CHOIX FONDS/PLATEFORMES
        JPanel panelChoixPf = new JPanel(new GridBagLayout());

        panelChoixPf.add(createButton("skin_pfH"), new GBC(0, 1, 1, 1));
        panelChoixPf.add(createButton("skin_pfV"), new GBC(1, 0, 1, 1));
        panelChoixPf.add(createButton("skin_fond"), new GBC(0, 0, 1, 1));

        panelChoixPf.setBorder(BorderFactory.createLineBorder(Color.black));

        panelChoixPf.setMaximumSize(panelChoixPf.getPreferredSize());
        left.add(panelChoixPf);

        left.add(Box.createRigidArea(new Dimension(0, 10)));

        //AJOUT DES BOUTONS D'ACTION SUR LES PLATEFORMES
        JPanel panelActionPf = new JPanel(new GridLayout(3, 3));

        panelActionPf.add(new JPanel());
        panelActionPf.add(createButton("plateformes_up"));
        panelActionPf.add(new JPanel());
        panelActionPf.add(createButton("plateformes_left"));
        panelActionPf.add(createButton("plateformes_clear"));
        panelActionPf.add(createButton("plateformes_right"));
        panelActionPf.add(new JPanel());
        panelActionPf.add(createButton("plateformes_down"));
        panelActionPf.add(new JPanel());

        panelActionPf.setBorder(BorderFactory.createLineBorder(Color.black));

        panelActionPf.setMaximumSize(panelActionPf.getPreferredSize());
        left.add(panelActionPf);

        left.add(Box.createRigidArea(new Dimension(0, 10)));

        //AJOUT DE LA LISTE DES RAYONS
        JLabel rayonsLabel = new JLabel("Rayons");
        rayonsLabel.setToolTipText("Utilisez le clic droit pour placer des rayons");
        rayonsLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        left.add(rayonsLabel);
        listRayons.setListData(resources.fields().list().toArray(new FieldType[]{}));
        listRayons.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listRayons.setCellRenderer(ListRenderer.compose(JLabel.class,
                ListRenderer.background(),
                ListRenderer.assetIcon(20),
                ListRenderer.text(IAsset::getName),
                ListRenderer.emphasisSelected(),
                ListRenderer.tooltip(Formats.assetInfo(false, resources.fields(), "#ray.", null))
            ));
        listRayons.setSelectedIndex(0);


        JScrollPane rayonsPane = new JScrollPane(listRayons);
        rayonsPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        rayonsPane.setPreferredSize(new Dimension(120, 180));
        left.add(rayonsPane);

        left.add(Box.createRigidArea(new Dimension(0, 5)));

        Font posFont = posCaseLabel.getFont().deriveFont(Font.PLAIN);
        posCaseLabel.setFont(posFont);
        posPixelLabel.setFont(posFont);
        posCaseLabel.setAlignmentX(CENTER_ALIGNMENT);
        posPixelLabel.setAlignmentX(CENTER_ALIGNMENT);

        // Set a text with maximum width to inform the layout
        posCaseLabel.setText("Case : 9999; 9999");
        posPixelLabel.setText("Px. : 999999; 999999");

        left.add(posCaseLabel);
        left.add(posPixelLabel);
    }

    private void initRightPanel() {
        ////PARTIE DROITE////
        right.setBorder(BorderFactory.createEmptyBorder(0, 15, 0, 15));
        right.setLayout(new BoxLayout(right, BoxLayout.PAGE_AXIS));

        //BOUTONS DES SPRITES
        JPanel buttonPane = new JPanel(new GridBagLayout());

        buttonPane.add(createButton("set_player"), new GBC(0, 0).setInsets(0, 3, 0, 3));
        buttonPane.add(createButton("set_obj_effet"), new GBC(1, 0).setInsets(0, 3, 0, 3));
        buttonPane.add(createButton("set_obj_points"), new GBC(2, 0).setInsets(0, 3, 0, 3));
        buttonPane.add(createButton("entity_suppr"), new GBC(0, 1).setInsets(0, 3, 0, 3));
        buttonPane.add(createButton("entity_suppr_all"), new GBC(1, 1).setInsets(0, 3, 0, 3));
        buttonPane.add(createButton("entity_modify"), new GBC(2, 1).setInsets(0, 3, 0, 3));

        buttonPane.setMaximumSize(buttonPane.getPreferredSize());
        right.add(buttonPane);

        //ORDRE DU NIVEAU
        JLabel ordreLabel = new JLabel("Ordre");
        ordreLabel.setToolTipText("Utilisez les boutons pour modifier l'ordre");
        ordreLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        right.add(ordreLabel);
        listOrdre.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listOrdre.setCellRenderer(ListRenderer.compose(JLabel.class,
                ListRenderer.background(),
                ListRenderer.assetIcon(30).mapping(Entity::getFrame),
                ListRenderer.emphasisSelected(),
                ListRenderer.text((sp, index) ->
                    (index+1) + ". " + sp.getSprite().getName() +
                    " (" + POSITION_FORMAT.format(sp.getX()) +
                    " ; " + POSITION_FORMAT.format(sp.getY()) + ")"
                )
            ));
        listOrdre.setSelectedIndex(0);

        JScrollPane ordrePane = new JScrollPane(listOrdre);
        ordrePane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        ordrePane.setPreferredSize(new Dimension(190, 160));
        ordrePane.setMaximumSize(ordrePane.getPreferredSize());
        right.add(ordrePane);

        //BOUTONS POUR GERER L'ORDRE
        JPanel ordreButtonPane = new JPanel();
        ordreButtonPane.add(createButton("ordre_down"));
        ordreButtonPane.add(createButton("ordre_up"));
        ordreButtonPane.add(createButton("ordre_add"));

        ordreButtonPane.setMaximumSize(ordreButtonPane.getPreferredSize());
        right.add(ordreButtonPane);

        //LISTE DES FRUITS POSSIBLES
        JLabel fruitLabel = new JLabel("Fruits");
        fruitLabel.setToolTipText("Utilisez Maj+clic pour placer le sprite sélectionné");
        fruitLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        right.add(fruitLabel);
        listFruits.setListData(resources.bads().list().toArray(new Sprite[]{}));
        listFruits.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listFruits.setCellRenderer(ListRenderer.compose(JLabel.class,
                ListRenderer.background(),
                ListRenderer.assetIcon(30).mapping(Sprite::getDefaultFrame),
                ListRenderer.text(IAsset::getName),
                ListRenderer.emphasisSelected(),
                ListRenderer.tooltip(Formats.assetInfo(false, resources.bads(), "#bad.", "bad_"))
            ));
        listFruits.setSelectedIndex(0);

        JScrollPane fruitPane = new JScrollPane(listFruits);
        fruitPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        fruitPane.setPreferredSize(new Dimension(190, 200));
        right.add(fruitPane);
    }

    private void initBottomPanel() {

        //METADATAS
        JPanel metaPanel = new JPanel();
        metaPanel.setLayout(new BoxLayout(metaPanel, BoxLayout.PAGE_AXIS));

        JPanel levelNamePane = new JPanel();
        levelNamePane.add(new JLabel("Nom : "));
        levelName.setText("Nom du niveau");
        levelName.setColumns(15);
        levelNamePane.add(levelName);

        levelDesc.setText("Description");
        levelDesc.setColumns(25);
        levelDesc.setRows(8);
        levelDesc.setLineWrap(true);
        levelDesc.setWrapStyleWord(true);
        JScrollPane descPane = new JScrollPane(levelDesc);

        metaPanel.add(levelNamePane);
        metaPanel.add(Box.createRigidArea(new Dimension(5, 0)));
        metaPanel.add(descPane);


        metaPanel.setBorder(BorderFactory.createCompoundBorder(
            BorderFactory.createTitledBorder("Infos sur le niveau"),
            BorderFactory.createEmptyBorder(5, 7, 5, 7)));


        bottom.add(metaPanel);

        bottom.add(Box.createRigidArea(new Dimension(10, 0)));

        //SPRITES
        JPanel spriteListPanel = new JPanel();
        spriteListPanel.setLayout(new BoxLayout(spriteListPanel, BoxLayout.PAGE_AXIS));

        JLabel spritesListLabel = new JLabel("Sprites");
        spritesListLabel.setToolTipText("Utilisez les boutons pour modifier l'ordre");
        spritesListLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        spriteListPanel.add(spritesListLabel);

        listSprites.setListData(resources.sprites().list().toArray(new Sprite[]{}));
        listSprites.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listSprites.setCellRenderer(ListRenderer.compose(JLabel.class,
                ListRenderer.background(),
                ListRenderer.assetIcon(20).mapping(Sprite::getDefaultFrame),
                ListRenderer.text(IAsset::getName),
                ListRenderer.emphasisSelected(),
                ListRenderer.tooltip(Formats.assetInfo(false, resources.sprites(), null, null))
            ));
        listSprites.setFixedCellHeight(25);
        listSprites.setSelectedIndex(0);


        JScrollPane spriteListPane = new JScrollPane(listSprites);
        spriteListPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        spriteListPane.setPreferredSize(new Dimension(190, 160));


        JPanel spriteButtonPanel = new JPanel();

        spriteButtonPanel.add(createButton("add_sprite"));
        spriteButtonPanel.add(createButton("add_sprite_layer0"));
        spriteButtonPanel.add(createButton("add_sprite_layer1"));

        spriteListPanel.add(spriteListPane);
        spriteListPanel.add(spriteButtonPanel);

        bottom.add(spriteListPanel);

        bottom.add(Box.createHorizontalStrut(15));

        JPanel previewPanel = new JPanel();
        previewPanel.setLayout(new BoxLayout(previewPanel, BoxLayout.PAGE_AXIS));

        JLabel previewLabel = new JLabel("Aperçu");
        previewLabel.setToolTipText("Aperçu du sprite sélectionné");
        previewLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        previewPanel.add(previewLabel);

        previewPanel.add(Box.createVerticalStrut(5));
        preview.setPreferredSize(new Dimension(190, 180));
        preview.setAlignmentX(Component.CENTER_ALIGNMENT);
        preview.setBackground(Color.WHITE);
        preview.setBorder(BorderFactory.createLineBorder(Color.black));
        previewPanel.add(preview);

        bottom.add(previewPanel);
    }

    private JButton createButton(String action) {
        JButton button = new JButton(getAction(action));
        button.setHideActionText(true);

        // Calculate correct dimensions (the needed margin depends on the L&F)
        Insets margin = button.getMargin();
        Dimension size = button.getMinimumSize();
        Icon icon = (Icon) getAction(action).getValue(Action.LARGE_ICON_KEY);
        int borderW = size.width - icon.getIconWidth();
        int borderH = size.height - icon.getIconHeight();
        if(borderW > 10) {
            margin.left -= (borderW-10)/2;
            margin.right -= (borderW-10)/2;
        }
        if(borderH > 10) {
            margin.top -= (borderH-10)/2;
            margin.bottom -= (borderH-10)/2;
        }
        button.setMargin(margin);
        return button;
    }

    private JMenuItem createMenu(String name, String... actions) {
        JMenu menu = new JMenu(name);

        for (String a : actions) {
            //Separator
            if (a.isEmpty()) {
                menu.addSeparator();

                //Checkbox
            } else if (a.startsWith("@")) {
                JCheckBoxMenuItem item = new JCheckBoxMenuItem(getAction(a.substring(1)));
                item.setToolTipText(null);
                menu.add(item);

                //Menu normal
            } else {
                JMenuItem item = new JMenuItem(getAction(a));
                item.setToolTipText(null);
                menu.add(item);
            }
        }

        return menu;
    }

    private void createToggleAction(String id, String name, String desc, KeyStroke raccourci,
            String icon, Function<Boolean, LevelAction> action) {
        createAction(id, name, desc, raccourci, icon, e -> {
            boolean state = Boolean.TRUE.equals(getAction(id).getValue(Action.SELECTED_KEY));
            LevelAction a = action.apply(state);
            if(a != null)
                controller.doAction(a);
        });
    }

    private void createAction(String id, String name, String desc, KeyStroke raccourci, String icon, Supplier<LevelAction> action) {
        createAction(id, name, desc, raccourci, icon, e -> {
        	LevelAction a = action.get();
        	if (a != null)
                controller.doAction(a);
        });
    }

    private void createAction(String id, String name, String desc, KeyStroke raccourci, String icon, ActionListener action) {
        AbstractAction a = new AbstractAction(name) {
            private static final long serialVersionUID = 14659145612305L;

            @Override
            public void actionPerformed(ActionEvent e) {
                action.actionPerformed(e);
            }
        };

        a.putValue(Action.ACTION_COMMAND_KEY, id);
        a.putValue(Action.ACCELERATOR_KEY, raccourci);

        if (icon != null)
            a.putValue(Action.LARGE_ICON_KEY, new ImageIcon(FileUtils.loadImageResource(icon)));

        a.putValue(Action.SHORT_DESCRIPTION, desc);
        actionMap.put(id, a);
    }

    private void initActions() {

        //Plateformes
        createAction("plateformes_clear", "Effacer", "Effacer toutes les plateformes et tout les rayons",
            null, "icons/plateforme_clear.png", ClearPlateformes::new);

        createAction("plateformes_up", "Déplacer vers le haut", "Déplacer le niveau vers le haut",
            getMenuShortcut(KeyEvent.VK_UP), "icons/arrow_up.png", () -> new ShiftLevel(ShiftLevel.SHIFT_UP));

        createAction("plateformes_down", "Déplacer vers le bas", "Déplacer le niveau vers le bas",
            getMenuShortcut(KeyEvent.VK_DOWN), "icons/arrow_down.png", () -> new ShiftLevel(ShiftLevel.SHIFT_DOWN));

        createAction("plateformes_left", "Déplacer vers la gauche", "Déplacer le niveau vers la gauche",
            getMenuShortcut(KeyEvent.VK_LEFT), "icons/arrow_left.png", () -> new ShiftLevel(ShiftLevel.SHIFT_LEFT));

        createAction("plateformes_right", "Déplacer vers la droite", "Déplacer le niveau vers la droite",
            getMenuShortcut(KeyEvent.VK_RIGHT), "icons/arrow_right.png", () -> new ShiftLevel(ShiftLevel.SHIFT_RIGHT));


        //Skins
        createAction("skin_pfH", "Style plateformes H", "Choisir l'apparence des plateformes horizontales",
            null, "icons/plateformeH_default.png",
            () -> new SetTilesType(pfDialog.showPlateformeDialog(level.getTiles().getTileTypeH()), true));

        createAction("skin_pfV", "Style plateformes V", "Choisir l'apparence des plateformes verticales",
            null, "icons/plateformeV_default.png",
            () -> new SetTilesType(pfDialog.showPlateformeDialog(level.getTiles().getTileTypeV()), false));

        createAction("skin_fond", "Style fond", "Choisir l'apparence du fond",
            null, "icons/fond_default.png",
            () -> new SetBackground(fondDialog.showFondDialog(level.getBackground())));


        //Sprites 1
        createAction("set_player", "Placer le spawn", "Placer le spawn d'Igor", null,
            "icons/objet_spawn.png", () -> new AddEntity(resources.getSpecial("spawnIgor"), 10, 7, LevelLayer.PLAYER_SPAWN));

        createAction("set_obj_effet", "Placer l'objet à effet", "Placer le spawn de l'objet à effet", null,
            "icons/objet_effet.png", () -> new AddEntity(resources.getSpecial("spawnEffet"), 5, 7, LevelLayer.SPEC_ITEM_SPAWN));

        createAction("set_obj_points", "Placer l'objet à points", "Placer le spawn de l'objet à points", null,
            "icons/objet_points.png", () -> new AddEntity(resources.getSpecial("spawnPoints"), 15, 7, LevelLayer.SCORE_ITEM_SPAWN));

        createAction("entity_suppr", "Supprimer le sprite", "Supprimer le sprite sélectionné (Suppr.)", null,
            "icons/entity_suppr.png", () -> new DeleteEntity(level.getSelected()));

        createAction("entity_suppr_all", "Supprimer tous les sprites", "Supprimer tous les sprites du niveau", null,
            "icons/entity_suppr_all.png", DeleteAllEntities::new);

        createAction("entity_modify", "Modifier le sprite", "Modifier l'image du sprite", null, "icons/entity_modify.png", e -> {
            Entity sprite = level.getSelected();
            if (sprite != null) {
                EntityDialog.Result result = spriteDialog.showSpriteDialog(sprite, level.getEntityLayer(sprite));
                controller.doAction(new ModifyEntity(sprite, result.spriteFrame, result.layer));
            }
        });

        getAction("entity_suppr").setEnabled(false);
        getAction("entity_modify").setEnabled(false);


        //Ordre
        createAction("ordre_down", "Ordre bas", "Déplacer le fruit d'un cran vers le bas", null, "icons/green_arrow_down.png", e -> {
            int selIndex = listOrdre.getSelectedIndex();
            controller.doAction(new SwapBad(selIndex, selIndex + 1));
            listOrdre.setSelectedIndex(selIndex + 1);
            listOrdre.requestFocusInWindow();
        });

        createAction("ordre_up", "Ordre haut", "Déplacer le fruit d'un cran vers le haut", null, "icons/green_arrow_up.png", e -> {
            int selIndex = listOrdre.getSelectedIndex();
            controller.doAction(new SwapBad(selIndex, selIndex - 1));
            listOrdre.setSelectedIndex(selIndex - 1);
            listOrdre.requestFocusInWindow();
        });

        createAction("ordre_add", "Ordre ajout", "Ajouter le fruit sélectionné (Maj+clic)", null, "icons/entity_add.png", e -> {
            Sprite sprite = listFruits.getSelectedValue();
            controller.doAction(new AddEntity(sprite, 10, 10, LevelLayer.preferredFruitLayerFor(sprite)));
            listOrdre.setSelectedIndex(listOrdre.getModel().getSize() - 1);
            listOrdre.requestFocusInWindow();
        });

        getAction("ordre_down").setEnabled(false);
        getAction("ordre_up").setEnabled(false);


        //Sprites 2
        createAction("add_sprite", "Ajouter le sprite", "Ajouter le sprite sélectionné (Maj+clic)",
            null, "icons/entity_add.png",
            () -> {
                Sprite sprite = listSprites.getSelectedValue();
                return new AddEntity(sprite, 10, 10, LevelLayer.preferredSpriteLayerFor(sprite));
            });

        createAction("add_sprite_layer0", "Ajouter le sprite", "Ajouter le sprite sélectionné en arrière-plan",
            null, "icons/entity_add_layer0.png",
            () -> new AddEntity(listSprites.getSelectedValue(), 10, 10, LevelLayer.BACKGROUND));

        createAction("add_sprite_layer1", "Ajouter le sprite", "Ajouter le sprite sélectionné en avant-plan",
            null, "icons/entity_add_layer1.png",
            () -> new AddEntity(listSprites.getSelectedValue(), 10, 10, LevelLayer.FOREGROUND));

        getAction("add_sprite").setEnabled(false);
        getAction("add_sprite_layer0").setEnabled(false);
        getAction("add_sprite_layer1").setEnabled(false);

        //MENUS

        //Actions de fichiers
        createAction("file_open", "Ouvrir...", null, getMenuShortcut(KeyEvent.VK_O), null, e -> {
            fileDialog.setMode(FileDialog.LOAD);
            fileDialog.setVisible(true);
            if (fileDialog.getFile() != null && confirmLevelClose()) {
                controller.loadLevel(Paths.get(fileDialog.getDirectory(), fileDialog.getFile()));
            }
        });

        createAction("file_new", "Nouveau", null, getMenuShortcut(KeyEvent.VK_N), null, e -> {
            if (confirmLevelClose())
                controller.setLevel(null);
        });

        createAction("file_save", "Enregistrer", null, getMenuShortcut(KeyEvent.VK_S), null, e -> {
        	if (validateScript())
        		saveLevel(false);
        });

        createAction("file_save_as", "Enregistrer sous...", null, getMenuShortcut(KeyEvent.VK_S, InputEvent.SHIFT_DOWN_MASK), null, e -> {
        	if (validateScript())
        	saveLevel(true);
        });

        createAction("file_export", "Exporter...", "Exporte le niveau sous forme d'image", getMenuShortcut(KeyEvent.VK_W), null, e -> {
            if (controller.getLevelFile() != null) {
                imageDialog.setDirectory(controller.getLevelFile().getParent().toString());
                imageDialog.setFile(controller.getLevelFile().getFileName().toString().replaceFirst("\\.[^.]+$", ".png"));
            }

            Path file = getSaveFile(imageDialog);
            if (file != null)
                controller.exportToImage(file);
        });

        createAction("file_quit", "Quitter", null, getMenuShortcut(KeyEvent.VK_Q), null, e -> {
            LevelView.this.winAdapter.windowClosing(null);
        });

        //Edition
        createAction("undo", "Annuler", null, getMenuShortcut(KeyEvent.VK_Z), null, e -> controller.undoAction());
        createAction("redo", "Refaire", null, getMenuShortcut(KeyEvent.VK_Y), null, e -> controller.redoAction());
        setUndoEnabled(false);
        setRedoEnabled(false);

        createAction("edition_copy", "Copier", null, getMenuShortcut(KeyEvent.VK_C), null, e -> {
           if(markEntityCopied() != null)
               getAction("edition_paste").setEnabled(true);
        });

        createAction("edition_cut", "Couper", null, getMenuShortcut(KeyEvent.VK_X), null, () -> {
           Entity selected = markEntityCopied();
           if(selected != null) {
               getAction("edition_paste").setEnabled(true);
               return new DeleteEntity(level.getSelected());
           }
           return null;
        });

        createAction("edition_paste", "Coller", null, getMenuShortcut(KeyEvent.VK_V), null, () -> {
           Entity copied = copiedEntity.copy();
           return new CompoundLevelAction(
               new AddEntity(copied, copied.getX(), copied.getY(), copiedEntityLayer),
               new SelectEntity(copied)
           );
        });

        createAction("edition_suppr", "Effacer", null, getMenuShortcut(KeyEvent.VK_DELETE), null, () -> {
            return new CompoundLevelAction(new ClearPlateformes(), new DeleteAllEntities());
        });
        createAction("edition_flip_h", "Inverser horizontalement", null, getMenuShortcut(KeyEvent.VK_I), null, FlipLevelH::new);
        createAction("edition_flip_v", "Inverser verticalement", null, getMenuShortcut(KeyEvent.VK_I, InputEvent.SHIFT_DOWN_MASK), null, FlipLevelV::new);
        createAction("edition_resize", "Redimensionner...", null, getMenuShortcut(KeyEvent.VK_R), null, () -> {
        	Dimension size = new Dimension(level.getWidth(), level.getHeight());
        	size = resizeDialog.showResizeDialog(size);
        	return size == null ? null : new ResizeLevel(size.width, size.height);
        });

        //Affichage
        createAction("show_script", "Afficher le script", null, getMenuShortcut(KeyEvent.VK_T), null, e -> {
            scriptFrame.setVisible(true);
        });

        createToggleAction("show_grid", "Afficher la grille", null, getMenuShortcut(KeyEvent.VK_G, InputEvent.ALT_DOWN_MASK),
            null, state -> center.getActionGrille(state));

        createToggleAction("show_borders", "Afficher les bordures", null, getMenuShortcut(KeyEvent.VK_B, InputEvent.ALT_DOWN_MASK),
            null, state -> new DrawOptions(Level.SHOW_BORDURES_MASK, state));

        createToggleAction("show_sprites", "Afficher les sprites", null, getMenuShortcut(KeyEvent.VK_S, InputEvent.ALT_DOWN_MASK),
            null, state -> new DrawOptions(Level.SHOW_SPRITES_MASK, state));

        createToggleAction("show_bads", "Afficher les fruits", null, getMenuShortcut(KeyEvent.VK_F, InputEvent.ALT_DOWN_MASK),
            null, state -> new DrawOptions(Level.SHOW_FRUITS_MASK, state));

        createToggleAction("show_spawns", "Afficher R-E-P", null, getMenuShortcut(KeyEvent.VK_R, InputEvent.ALT_DOWN_MASK),
            null, state -> new DrawOptions(Level.SHOW_SPAWNS_MASK, state));

        createToggleAction("show_ordre", "Afficher l'ordre", null, getMenuShortcut(KeyEvent.VK_O, InputEvent.ALT_DOWN_MASK),
            null, state -> new DrawOptions(Level.SHOW_ORDRE_MASK, state));

        createToggleAction("show_ordre", "Afficher l'ordre", null, getMenuShortcut(KeyEvent.VK_O, InputEvent.ALT_DOWN_MASK),
            null, state -> new DrawOptions(Level.SHOW_ORDRE_MASK, state));

        createAction("show_default", "Affichage par défaut", null, null, null, () -> {
            LevelAction draw = new DrawOptions(Level.SHOW_BORDURES_MASK |
                Level.SHOW_SPRITES_MASK |
                Level.SHOW_FRUITS_MASK |
                Level.SHOW_SPAWNS_MASK |
                Level.SHOW_ORDRE_MASK);
            return new CompoundLevelAction(center.getActionGrille(false), draw);
        });

        createAction("show_bottom", "Afficher infos/sprites", null, null, null, e -> {
            bottom.setVisible(!bottom.isVisible());
            this.pack();
        });
        getAction("show_bottom").putValue(Action.SELECTED_KEY, bottom.isVisible());

        //Ressources
        createAction("show_current_assets", "Afficher les ressources", null, null, null, e -> {
            List<Path> roots = resources.getRootPaths();

            StringBuilder message = new StringBuilder("<html>");
            if(roots.size() == 0) {
                message.append("Aucun dossier de ressources chargé.");
            } else if(roots.size() == 1) {
                message.append("1 dossier de ressources chargé :");
            } else {
                message.append(roots.size());
                message.append(" dossiers de ressources chargés :");
            }
            for(Path root: roots) {
                message.append("<br/>&nbsp;&nbsp;&nbsp;- ");
                message.append(root.toAbsolutePath().toString());
            }
            message.append("</html>");

            JOptionPane.showMessageDialog(LevelView.this, message.toString(),
                "Ressources chargées", JOptionPane.INFORMATION_MESSAGE);
        });

        createAction("reload_assets", "Recharger les ressources", null, null, null, e -> {
            if(confirmLevelClose())
                controller.reloadAssets();
        });

    }

    public Action getAction(String name) {
        Action action = actionMap.get(name);
        if (action == null)
            throw new IllegalArgumentException("Unknown action " + name);
        return action;
    }

    private Path getSaveFile(FileDialog dialog) {
        dialog.setMode(FileDialog.SAVE);
        dialog.setVisible(true);
        if (dialog.getDirectory() == null)
            return null;

        String path = dialog.getDirectory();
        if (dialog.getFile() != null)
            path += dialog.getFile();

        return Paths.get(path);
    }

    private boolean saveLevel(boolean askFileLocation) {
    	controller.setScript(scriptFrame.getText(), scriptFrame.isScriptXML());
        controller.setName(levelName.getText());
        controller.setDescription(levelDesc.getText());

        Path currentFile = controller.getLevelFile();
        Path file = askFileLocation ? null : currentFile;

        if(file == null)
            file = getSaveFile(fileDialog);

        if(file == null)
            return false;

        if(!file.equals(currentFile)) {
            if(controller.getAssetsForLevel(file) != resources) {
                int reply = JOptionPane.showConfirmDialog(this,
                    "Enregistrer le niveau à cet endroit modifiera les ressources actives.\nVoulez-vous continuer ?",
                    "Enregistrer", JOptionPane.YES_NO_OPTION);
                if(reply != JOptionPane.OK_OPTION)
                    return false;
            }
        }

        controller.saveLevel(file);
        return true;
    }

    private boolean validateScript() {
    	// On vérifie pas les scripts XML.
    	if (scriptFrame.isScriptXML())
    		return true;

    	String script = scriptFrame.getText();
    	try {
			ScriptParser.parse(new StringReader(script));
		} catch (ScriptParseException e) {
			int reply = JOptionPane.showConfirmDialog(
				this,
				e.getFullMessageWithHeader("Le script du niveau contient des erreurs. Voulez-vous quand même sauvegarder ?"),
				"Erreur",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE);
			return reply == JOptionPane.YES_OPTION;
		}

		return true;
    }

    private boolean confirmLevelClose() {
    	if (!controller.isModified())
            return true;

        int reply = JOptionPane.showConfirmDialog(this, "Le niveau n'a pas été enregistré.\nVoulez-vous enregistrer les modifications ?");

        if (reply == JOptionPane.OK_OPTION) {
            return validateScript() && saveLevel(false);

        } else if (reply == JOptionPane.NO_OPTION)
            return true;

        return false;
    }

    private Entity markEntityCopied() {
        Entity selected = level.getSelected();
        LevelLayer layer = level.getEntityLayer(selected);
        if(layer == LevelLayer.PLAYER_SPAWN) // Can't copy player
            return null;
        copiedEntity = selected.copy();
        copiedEntityLayer = layer;
        return selected;
    }

    //ECOUTE DU MODELE DU NIVEAU
    @Override
    public void levelChanged(Level lvl, Path path) {
        level = lvl;

        levelName.setText(lvl.getName());
        levelDesc.setText(lvl.getDescription());
        if(scriptFrame != null) {
            scriptFrame.setText(lvl.getScript());
            scriptFrame.setScriptType(lvl.isScriptXML());
        }

        listOrdreModel = new LevelOrdreListModel(lvl);
        listOrdre.setModel(listOrdreModel);
        copiedEntity = null;
        copiedEntityLayer = null;

        center.setLevel(lvl);
        levelUpdated(LevelAction.MODIFY_ALL);
    }

    @Override
    public void levelUpdated(int changes) {
        if((changes & LevelAction.MODIFY_SKINS) != 0)
            updateSkinButtons();

        if((changes & LevelAction.MODIFY_ENTITY_LIST) != 0) {
            listOrdreModel.updateModel();
            updateSpriteButtons();
            updateOrdreButtons();
        } else if((changes & LevelAction.MODIFY_ENTITIES) != 0) {
            listOrdre.repaint();
            updateSpriteButtons();
        }

        if((changes & LevelAction.MODIFY_METADATA) != 0)
            updateDrawOptions();

        getAction("show_grid").putValue(Action.SELECTED_KEY, center.isGrille());
        centerScroll.setViewportView(center); // this updates scrollbars if required
    }

    private void updateSkinButtons() {
        int pfWidth = 80;
        int pfHeight = 20;

        TileType tilesV = level.getTiles().getTileTypeV();
        Icon iconBg = new ThumbnailIcon(level.getBackground(), pfWidth, pfWidth);
        Icon iconPfH = new ThumbnailIcon(level.getTiles().getTileTypeH(), pfWidth, pfHeight);
        Icon iconPfV = new ThumbnailIcon((g, width, height, obs) -> {
            AffineTransform saveAT = g.getTransform();
            g.rotate(Math.PI / 2, 0, 0);
            g.scale(1.0, -1.0);
            tilesV.drawThumbnail(g, height, width, obs);
            g.setTransform(saveAT);
        }, pfHeight, pfWidth);

        getAction("skin_pfH").putValue(Action.LARGE_ICON_KEY, iconPfH);
        getAction("skin_pfV").putValue(Action.LARGE_ICON_KEY, iconPfV);
        getAction("skin_fond").putValue(Action.LARGE_ICON_KEY, iconBg);
    }

    private void updateSpriteButtons() {
        //On désactive les boutons si le sprite correspondant est déjà placé
        getAction("set_player").setEnabled(level.getLayerSize(LevelLayer.PLAYER_SPAWN) == 0);

        //Si aucun sprite n'est selectionné, on désactive les boutons "de sélection"
        getAction("entity_suppr").setEnabled(level.getSelected() != null);
        getAction("entity_modify").setEnabled(level.getSelected() != null);
        getAction("edition_copy").setEnabled(level.getSelected() != null);
        getAction("edition_cut").setEnabled(level.getSelected() != null);
        getAction("edition_paste").setEnabled(copiedEntity != null);
    }

    private void updateOrdreButtons() {
        //Gestion des boutons de l'ordre
        int curIndex = listOrdre.getSelectedIndex();
        int badSize = listOrdreModel.getSize();

        getAction("ordre_down").setEnabled(curIndex >= 0 && curIndex < badSize - 1);
        getAction("ordre_up").setEnabled(curIndex > 0 && badSize > 1);
    }

    private void updateDrawOptions() {
        int options = level.getDrawOptions();
        getAction("show_borders").putValue(Action.SELECTED_KEY, (options & Level.SHOW_BORDURES_MASK) != 0);
        getAction("show_sprites").putValue(Action.SELECTED_KEY, (options & Level.SHOW_SPRITES_MASK) != 0);
        getAction("show_bads").putValue(Action.SELECTED_KEY, (options & Level.SHOW_FRUITS_MASK) != 0);
        getAction("show_spawns").putValue(Action.SELECTED_KEY, (options & Level.SHOW_SPAWNS_MASK) != 0);
        getAction("show_ordre").putValue(Action.SELECTED_KEY, (options & Level.SHOW_ORDRE_MASK) != 0);
    }

    private void setTitle(Path file, boolean modified) {
        if (file == null) {
            super.setTitle("HammerfestCreator");
            if(scriptFrame != null)
                scriptFrame.setTitle("Script");
        } else {
            String name = file.getFileName().toString();
            if(scriptFrame != null)
                scriptFrame.setTitle("Script - " + name);
            if (modified)
                super.setTitle("HammerfestCreator - " + name + "*");
            else super.setTitle("HammerfestCreator - " + name);
        }
    }


    @Override
    public void setModifiedStatus(boolean modified) {
        setTitle(controller.getLevelFile(), modified);
    }

    @Override
    public void setUndoEnabled(boolean v) {
        getAction("undo").setEnabled(v);
    }

    @Override
    public void setRedoEnabled(boolean v) {
        getAction("redo").setEnabled(v);
    }

    //**********
    //GESTION DES EVENEMENTS
    //**********
    //LISTENER GLOBAL
    class GlobalListener implements AWTEventListener {
        @Override
        public void eventDispatched(AWTEvent event) {
            //POUR LE CENTER
            if (event.getID() == KeyEvent.KEY_PRESSED) {
                KeyEvent keyEvent = (KeyEvent) event;
                if (keyEvent.isShiftDown()) {
                    pfListener.isShiftDown = true;
                }
                if (keyEvent.isAltDown()) {
                    pfListener.isAltDown = true;
                }
            } else if (event.getID() == KeyEvent.KEY_RELEASED) {
                pfListener.isShiftDown = false;
                pfListener.isAltDown = false;
            }
        }
    }

    //POUR LE CORPS DU NIVEAU
    class PlateformeListener implements MouseListener, MouseMotionListener, KeyListener {

        double x = 0;
        double y = 0;
        double dragOffsetX = 0;
        double dragOffsetY = 0;
        boolean isPlateforme = false, isRayon = false;
        boolean isAltDown = false, isShiftDown = false;
        boolean isMousePressed;
        boolean tileChanged;
        int mouseButton;

        private void addPlateforme(double x, double y, boolean pf) {
            if (level.isInBounds((int) x, (int) y))
                controller.doAction(new TileAction((int) x, (int) y, pf));
        }

        private void addRayon(double x, double y, FieldType type) {
            if (level.isInBounds((int) x, (int) y))
                controller.doAction(new TileAction((int) x, (int) y, type));
        }

        private double getStep() {
            if(isAltDown)
                return isShiftDown ? 0.5 : 1 / (double) Level.TILES_SIZE;
            return 1;
        }

        private double roundCoord(double x) {
            double step = getStep();
            return step * Math.round(x/step);
        }

        private double clampAndRoundCoord(double x, double min, double max) {
            if(x < min)
                x = min;
            else if(x > max)
                x = max;
            x = roundCoord(x);
            if(x < min)
                x += getStep();
            else if(x > max)
                x -= getStep();
            return x;
        }

        private double processCoordX(double x, Entity selected) {
            Rectangle bbox = selected.getBoundingBox();
            double xMin = bbox.getMinX() / Level.TILES_SIZE - selected.getX();
            double xMax = bbox.getMaxX() / Level.TILES_SIZE - selected.getX();
            return clampAndRoundCoord(x, - xMax, level.getWidth() - xMin);
        }

        private double processCoordY(double y, Entity selected) {
            Rectangle bbox = selected.getBoundingBox();
            double yMin = bbox.getMinY() / Level.TILES_SIZE - selected.getY();
            double yMax = bbox.getMaxY() / Level.TILES_SIZE - selected.getY();
            return clampAndRoundCoord(y, 0.25 - yMax, level.getHeight() - yMin - 0.25);
        }

        @Override
        public void mouseDragged(MouseEvent arg0) {
            moved(arg0);
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            moved(e);
        }

        private void moved(MouseEvent e) {
            calculateCoord(e);

            //e.getModifiersEx() == MouseEvent.BUTTON1_DOWN_MASK
            if (!isMousePressed)
                return;

            Entity selected = level.getSelected();

            if (selected == null) {
                //Si rien n'est selectionné et si rien n'est enfoncé, on modifie les plateformes/rayons
                if (!isShiftDown && tileChanged) {
                    if (mouseButton == MouseEvent.BUTTON1) {
                        addPlateforme(x, y, !isPlateforme);

                    } else if (mouseButton == MouseEvent.BUTTON3) {
                        addRayon(x, y, isRayon ? null : (FieldType) listRayons.getSelectedValue());
                    }
                }
            } else {
                //Sinon, on déplace le sprite
                double newX = processCoordX(x + dragOffsetX, selected);
                double newY = processCoordY(y + dragOffsetY, selected);
                controller.doAction(new MoveEntity(selected, newX, newY));
            }
        }

        private void calculateCoord(MouseEvent e) {
        	Point offset = center.getCoordOffset();
            double newX = (e.getX() - offset.x - Level.TILES_SIZE / 2) / (double) Level.TILES_SIZE;
            double newY = (e.getY() - offset.y) / (double) Level.TILES_SIZE;

            tileChanged = (int) x != (int) newX || (int) y != (int) newY;

            x = newX;
            y = newY;

            if (level.isInBounds((int) x, (int) y)) {
                posCaseLabel.setText("Case : " + (int) x + "; " + (int) y);
                posPixelLabel.setText("Px. : " + e.getX() + "; " + e.getY());
            } else {
                posCaseLabel.setText("-");
                posPixelLabel.setText("-");
            }
        }

        @Override
        public void mouseClicked(MouseEvent arg0) {
        }

        @Override
        public void mouseEntered(MouseEvent arg0) {
        }

        @Override
        public void mouseExited(MouseEvent arg0) {
            posCaseLabel.setText("-");
            posPixelLabel.setText("-");
        }

        @Override
        public void mousePressed(MouseEvent arg0) {
            isMousePressed = true;
            if (mouseButton == 0)
                mouseButton = arg0.getButton();
            calculateCoord(arg0);
            center.requestFocusInWindow();
            Entity selected = level.getSelected();
            if (selected == null) {
                //Si rien n'était sélectionné auparavant
                selected = level.getEntityAt(x, y);
                if (selected == null) {
                    //Si on clique dans le vide, on dessine les plateformes/rayons, ou on place un nouveau sprite si shift est enfoncé
                    if (isShiftDown) {
                        if (lastSelectedSprite != null)
                            controller.doAction(new AddEntity(
                                lastSelectedSprite,
                                roundCoord(x - 0.5), roundCoord(y - 0.5),
                                getLastSelectedLayer()
                            ));
                    } else {
                        isPlateforme = level.getTiles().isTileAt((int) x, (int) y);
                        isRayon = level.getTiles().isFieldAt((int) x, (int) y);

                        if (mouseButton == MouseEvent.BUTTON1) {
                            addPlateforme(x, y, !isPlateforme);
                        } else if (mouseButton == MouseEvent.BUTTON3) {
                            addRayon(x, y, isRayon ? null : (FieldType) listRayons.getSelectedValue());
                        }
                    }
                } else {
                    //Si on a cliqué sur un sprite
                    onEntityClick(selected);
                }
            } else {
                //Si qqchose était sélectionné, et si on sélectionne autre chose
                selected = level.getEntityAt(x, y);
                onEntityClick(selected);
            }


        }

        private LevelLayer getLastSelectedLayer() {
            if(lastSelectedSpriteIsFruit)
                return LevelLayer.preferredFruitLayerFor(lastSelectedSprite);
            return LevelLayer.preferredSpriteLayerFor(lastSelectedSprite);
        }

        @Override
        public void mouseReleased(MouseEvent arg0) {
            isMousePressed = false;
            mouseButton = 0;
            controller.doAction(LevelAction.separator());
        }

        private void onEntityClick(Entity selected) {
            if(selected != null) {
                //On prépare le drag
                dragOffsetX = selected.getX() - x;
                dragOffsetY = selected.getY() - y;

                if (isShiftDown && !isAltDown)
                    controller.doAction(new EntityOrientation(selected));
            }
            if(selected != level.getSelected()) {
                controller.doAction(new SelectEntity(selected));
                if(selected != null && level.getEntityLayer(selected) == LevelLayer.FRUITS_ORDER)
                    listOrdre.setSelectedValue(selected, false);
            }
        }

        //Gestion du clavier
        @Override
        public void keyPressed(KeyEvent arg0) {
            Entity selected = level.getSelected();
            if (selected == null)
                return;

            double x = selected.getX(), y = selected.getY();
            switch (arg0.getKeyCode()) {
                case KeyEvent.VK_LEFT:
                    x = selected.getX() - getStep();
                    break;
                case KeyEvent.VK_RIGHT:
                    x = selected.getX() + getStep();
                    break;
                case KeyEvent.VK_UP:
                    y = selected.getY() - getStep();
                    break;
                case KeyEvent.VK_DOWN:
                    y = selected.getY() + getStep();
                    break;
            }

            x = processCoordX(x, selected);
            y = processCoordY(y, selected);

            if (x != selected.getX() || y != selected.getY()) {
                controller.doAction(new MoveEntity(selected, x, y));
                controller.doAction(LevelAction.separator());
            }


            if (arg0.getKeyCode() == KeyEvent.VK_DELETE || arg0.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                controller.doAction(new DeleteEntity(selected));
            }

        }

        @Override
        public void keyReleased(KeyEvent arg0) {
        }

        @Override
        public void keyTyped(KeyEvent arg0) {
        }

    }
}
