package net.eternalfest.editor.views;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.Locale;

public class PositionFormat extends Format {
    private static final long serialVersionUID = 481986970254563263L;

    private final DecimalFormat inner;
    private final String decimalSep;

    private PositionFormat(DecimalFormat inner) {
        this.inner = inner;
        this.decimalSep = "" + inner.getDecimalFormatSymbols().getDecimalSeparator();
    }

    @Override
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        if (!(obj instanceof Number))
            throw new IllegalArgumentException("Cannot format given Object as a Number");

        double d = ((Number) obj).doubleValue();
        inner.format(d, toAppendTo, pos);

        if(Math.round(d) == d)
            deleteFractPart(toAppendTo);

        return toAppendTo;
    }

    @Override
    public Object parseObject(String source, ParsePosition pos) {
        return inner.parseObject(source, pos);
    }

    private void deleteFractPart(StringBuffer buffer) {
        buffer.delete(buffer.lastIndexOf(decimalSep), buffer.length());
    }

    public static PositionFormat from(Locale locale, int decimals) {
        DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance(locale);
        format.applyPattern("0.00");
        return new PositionFormat(format);
    }

}
