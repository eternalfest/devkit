package net.eternalfest.editor.views;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.io.StringReader;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import net.eternalfest.editor.controllers.LevelController;
import net.eternalfest.hfestlib.script.ScriptParseException;
import net.eternalfest.hfestlib.script.ScriptParser;
import net.eternalfest.hfestlib.script.XMLConverter;
import net.eternalfest.hfestlib.tools.ShowableException;

public class ScriptFrame extends JFrame {

    private static final long serialVersionUID = 1574928458182706475L;

    private JTextArea scriptArea;
    private JRadioButton scriptXML;
    private JRadioButton scriptNew;
    private JButton scriptVerify;

    private boolean isScriptXML;
    private boolean propagateChanges = true;

    ScriptFrame(LevelController controller) {
        this.setTitle("Script");

        scriptArea = new JTextArea();
        scriptArea.setTabSize(2);
        scriptArea.setFont(new Font("monospaced", Font.PLAIN, 12));

        Container contentPane = this.getContentPane();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));


        addButtons();
        contentPane.add(new JScrollPane(scriptArea));

        scriptArea.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void changedUpdate(DocumentEvent e) {
                onDocumentEvent(e);
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                onDocumentEvent(e);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                onDocumentEvent(e);
            }

            private void onDocumentEvent(DocumentEvent e) {
                if(propagateChanges)
                    controller.setModified(true);
            }
        });

        scriptNew.addActionListener(e -> {
            if (!isScriptXML)
                return;

            isScriptXML = false;
            int res = JOptionPane.showConfirmDialog(this, "Voulez-vous convertir le XML dans la nouvelle syntaxe ?");
            switch (res) {
                case JOptionPane.OK_OPTION:
                    try {
                        scriptArea.setText(XMLConverter.convert(scriptArea.getText()).toCode());
                    } catch (ShowableException ex) {
                        ex.show();
                        setScriptType(true);
                    }
                    break;

                case JOptionPane.CLOSED_OPTION:
                case JOptionPane.CANCEL_OPTION:
                    setScriptType(true);
                    break;
            }

            controller.setModified(true);
        });

        scriptXML.addActionListener(e -> {
            if (isScriptXML)
                return;

            controller.setModified(true);
        });

        scriptVerify.addActionListener(e -> {
        	if (isScriptXML)
        		throw new IllegalStateException();

        	try {
				ScriptParser.parse(new StringReader(getText()));
	        	JOptionPane.showMessageDialog(this, "Le script a été vérifié avec succès !");
			} catch (ScriptParseException ex) {
				ex.show();
			}
        });

        this.setPreferredSize(new Dimension(400, 300));
        this.setMinimumSize(new Dimension(400, 300));
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
    }

    private void addButtons() {
        ButtonGroup group = new ButtonGroup();
        JPanel buttonPane = new JPanel();

        scriptXML = new JRadioButton("Syntaxe XML");
        scriptNew = new JRadioButton("Nouvelle syntaxe");

        group.add(scriptXML);
        group.add(scriptNew);

        scriptVerify = new JButton("Vérifier");

        buttonPane.add(scriptXML);
        buttonPane.add(scriptNew);
        buttonPane.add(scriptVerify);
        buttonPane.setMaximumSize(new Dimension(400, 25));

        setScriptType(true);

        this.getContentPane().add(buttonPane);

    }

    public void setScriptType(boolean isXML) {
        propagateChanges = false;
        isScriptXML = isXML;
        (isXML ? scriptXML : scriptNew).setSelected(true);
        scriptVerify.setEnabled(!isXML);
        propagateChanges = true;
    }

    public String getText() {
        return scriptArea.getText();
    }

    public void setText(String str) {
        propagateChanges = false;
        scriptArea.setText(str);
        propagateChanges = true;
    }

    public boolean isScriptXML() {
        return isScriptXML;
    }
}
