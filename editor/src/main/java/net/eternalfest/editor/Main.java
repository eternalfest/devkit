package net.eternalfest.editor;

public class Main {
    public static void main(String[] args) {
        if(!Application.makeArgsParser().parse(args))
            System.exit(1);
    }

}
