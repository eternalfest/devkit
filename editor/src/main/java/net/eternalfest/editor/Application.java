package net.eternalfest.editor;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
import javax.swing.UIManager;

import net.eternalfest.editor.controllers.LevelController;
import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.tools.ExceptionHandler;
import net.eternalfest.hfestlib.tools.ResourceManager;
import net.eternalfest.hfestlib.tools.ShowableException;
import net.eternalfest.hfestlib.utils.ArgumentsParser;
import net.eternalfest.hfestlib.utils.FileUtils;
import org.jdom2.Document;

public class Application {

    public static final String versionID = "1.8.2";

    public static ArgumentsParser makeArgsParser() {
        ArgumentsParser parser = new ArgumentsParser(Application.class);
        parser.setDefaultAction(args -> {
            LevelController control = loadGUI();
            control.showGUI();
            return true;
        });

        parser.addHelpAndVersionActions(versionID);

        parser.addAction("open", "file", args -> {
            Path file = getPath(args.get(0));
            LevelController control = loadGUI();

            if(file == null) {
                try {
                    control.setLevel(loadLevel(null));
                } catch (ShowableException e) {
                    e.show();
                return false;
                }
            } else {
                control.loadLevel(file);
            }
            control.showGUI();
            return true;
        },
        "open a level in the editor",
        "use - instead of a filename to read from stdin");

        parser.addAction("export", "file", "out", args -> {
            ShowableException.setGUI(false);

            try {
                Level lvl = loadLevel(args.get(0));
                BufferedImage image = new BufferedImage(lvl.getPixelWidth(), lvl.getPixelHeight(), BufferedImage.TYPE_4BYTE_ABGR);
                lvl.draw(image.createGraphics(), null);

                String imageFormat = args.get("format");
                if(imageFormat == null) {
                    imageFormat = "png";
                }

                Path outPath = getPath(args.get(1));
                if(outPath == null) {
                    ImageIO.write(image, imageFormat, System.out);
                } else {
                    ImageIO.write(image, imageFormat, outPath.toFile());
                }

                return true;
            } catch(ShowableException e) {
                e.show();
                return false;
            } catch(IOException e) {
                new ShowableException(e).show();
                return false;
            }
        },
        "export a .lvl file to an image (default format: png)",
        "use - instead of a filename to read from stdin or write to stdout");
        parser.addNamedArg("export", "format", true);

        return parser;
    }

    public static Level loadLevel(String path) throws ShowableException {
        Path loc;
        Document doc;
        if (path != null) {
            loc = getPath(path);
            doc = FileUtils.loadXML(loc);
        } else {
            loc = Paths.get(System.getProperty("user.dir"));
            doc = FileUtils.loadXML(System.in);
        }

        return new Level(doc, ResourceManager.getFromAmbientConfig(loc));
    }

    public static LevelController loadGUI() {

        //ON INSTALLE LE HANDLER CUSTOM
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());

        //MAC : le menu se place bien sur la barre de menus
        System.setProperty("apple.laf.useScreenMenuBar", "true");
        System.setProperty("com.apple.mrj.application.apple.menu.about.name", "HammerfestCreator");
        //MAC

        //On met le look&feel du systéme
        try {
            String lookAndFeel = UIManager.getSystemLookAndFeelClassName();
            System.err.println("Using look&feel: " + lookAndFeel);
            UIManager.setLookAndFeel(lookAndFeel);
        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return new LevelController(ResourceManager.getDefault());
    }

    private static Path getPath(String path) {
        if("-".equals(path))
            return null;
        Path file = Paths.get(path);
        if (!file.isAbsolute())
            file = Paths.get(System.getProperty("user.dir"), path);
        return file;
    }
}
