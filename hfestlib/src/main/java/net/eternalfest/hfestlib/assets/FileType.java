package net.eternalfest.hfestlib.assets;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public enum FileType {
    IMAGE("image", "png", "jpg", "jpeg", "gif"),
    SVG("SVG", "svg"),
    UNKNOWN("inconnu");

    private static final Map<String, FileType> extensionMap = makeExtensionMap();

    private final String name;
    private final String prettyName;
    private final String[] extensions;

    private FileType(String name, String... extensions) {
        this.name = name;
        if(extensions.length == 0) {
            prettyName = name;
        } else {
            prettyName = Arrays.stream(extensions)
                .collect(Collectors.joining(", .", name + " (.", ")"));
        }
        this.extensions = extensions;
    }


    public String getName() {
        return name;
    }

    public String getPrettyName() {
        return prettyName;
    }

    public static FileType fromExtension(String ext) {
        return extensionMap.getOrDefault(ext.toLowerCase(), UNKNOWN);
    }

    public static FileType fromPath(Path path) {
        Path filename = path.getFileName();
        if(filename == null)
            return UNKNOWN;
        String name = filename.toString();
        int dot = name.lastIndexOf('.');
        if(dot < 0)
            return UNKNOWN;
        return fromExtension(name.substring(dot+1));
    }

    private static Map<String, FileType> makeExtensionMap() {
        Map<String, FileType> map = new HashMap<>();
        for(FileType type: values()) {
            for(String ext: type.extensions) {
                if(map.put(ext, type) != null)
                    throw new IllegalStateException("duplicate extension " + ext);
            }
        }
        return map;
    }
}
