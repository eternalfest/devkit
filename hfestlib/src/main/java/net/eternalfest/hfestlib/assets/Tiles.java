package net.eternalfest.hfestlib.assets;

import java.awt.Image;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import net.eternalfest.hfestlib.assets.AssetsMap.AssetAlias;
import net.eternalfest.hfestlib.models.sprites.TileType;
import net.eternalfest.hfestlib.tools.InvalidXMLNodeException;
import net.eternalfest.hfestlib.tools.ShowableException;
import net.eternalfest.hfestlib.utils.Lazy;
import net.eternalfest.hfestlib.utils.MultiResult;
import org.jdom2.Document;
import org.jdom2.Element;

/*
 * Retro-compatibility notes.
 *
 * Legacy names for attributes and elements:
 * - "id" was previously "ID"
 */
public enum Tiles {
    ;

    public static MultiResult<AssetsMap<TileType>, ShowableException> fromDocument(Document doc, Path root) {
        List<ShowableException> errors = new ArrayList<>();
        List<TileType> tiles = new ArrayList<>();
        List<AssetAlias> aliases = new ArrayList<>();

        Element xmlRoot = doc.getRootElement();
        String dirAttr = xmlRoot.getAttributeValue("dir");
        Path bodyDir = dirAttr == null ? root : root.resolve(dirAttr);
        dirAttr = xmlRoot.getAttributeValue("endDir");
        Path endDir = dirAttr == null ? bodyDir : root.resolve(dirAttr);

        for (Element el : xmlRoot.getChildren()) {
            switch(el.getName()) {
            case "alias":
            	AssetAlias.fromElement(el, errors).ifPresent(aliases::add);
                break;
            case "type":
            	String id = AssetsUtils.getAttrValue(el, "id", "ID");
            	if (!AssetsUtils.isIdValid(id, errors))
            		break;

                String name = el.getAttributeValue("name");
                String file = el.getAttributeValue("file");
                String endFile = el.getAttributeValue("endFile");
                if (endFile == null)
                	endFile = file;

                Lazy<Image> body = AssetsUtils.loadBitmap(bodyDir.resolve(file), errors);
                Lazy<Image> end = AssetsUtils.loadBitmap(endDir.resolve(endFile), errors);

                tiles.add(new TileType(body, end, name, id));
                break;
            default:
                errors.add(new InvalidXMLNodeException(el));
            }
        }

        return AssetsMap.from(tiles, TileType::unknownWith, TileType.UNKNOWN).addErrors(errors);
    }

    public static AssetsMap<TileType> empty() {
        return AssetsMap.empty(TileType::unknownWith, TileType.UNKNOWN);
    }
}
