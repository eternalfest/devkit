package net.eternalfest.hfestlib.assets;

import java.awt.Image;
import java.net.MalformedURLException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.regex.Pattern;

import com.kitfox.svg.SVGDiagram;
import com.kitfox.svg.SVGUniverse;
import net.eternalfest.hfestlib.tools.FileNotExistingException;
import net.eternalfest.hfestlib.tools.FileUnknownException;
import net.eternalfest.hfestlib.tools.ShowableException;
import net.eternalfest.hfestlib.utils.FileUtils;
import net.eternalfest.hfestlib.utils.Lazy;
import org.jdom2.Element;

public enum AssetsUtils {
    ;

    private static ShowableException checkFileType(Path path, FileType type) {
        if (!Files.isReadable(path))
            return new FileNotExistingException(path);
        FileType actual = FileType.fromPath(path);
        if(actual != type)
            return ShowableException.getExceptionWithMessage(
                "<html>Le fichier " + path.toString() + " de type "
                + actual.getPrettyName() + " n'est pas supporté!</html>");
        return null;
    }

    public static Lazy<Image> loadBitmap(Path path, List<ShowableException> errors) {
        ShowableException error = checkFileType(path, FileType.IMAGE);
        if(error != null) {
            errors.add(error);
            return Lazy.fromValue(null);
        }

        return Lazy.from(() -> {
            try {
                return FileUtils.loadImage(path);
            } catch (ShowableException e) {
                e.show();
                return null;
            }
        });
    }

    public static Lazy<SVGDiagram> loadSVG(Path path, SVGUniverse universe, List<ShowableException> errors) {
        ShowableException error = checkFileType(path, FileType.SVG);
        if(error != null) {
            errors.add(error);
            return Lazy.fromValue(null);
        }

        return Lazy.from(() -> {
            try {
                URI uri = universe.loadSVG(path.toUri().toURL());
                SVGDiagram svg = uri == null ? null : universe.getDiagram(uri);
                if(uri == null) {
                    new FileUnknownException(path).show();
                    return null;
                }
                return svg;
            } catch(MalformedURLException e) {
                ShowableException.getExceptionWithMessage(
                    "<html>Impossible de créer l'URL pour <b>" + path.toString() + "</b>: " + e.getMessage()
                ).show();
                return null;
            }
        });
    }

    public static String getAttrValue(Element el, String attrName, String attrNameFallback) {
    	String val = el.getAttributeValue(attrName);
    	if (val == null)
    		val = el.getAttributeValue(attrNameFallback);
    	return val;
    }

    private static Pattern ID_PATTERN = Pattern.compile("[A-Za-z0-9_\\-$]+");
    public static boolean isIdValid(String id, List<ShowableException> errors) {
        if (id == null || id.isEmpty()) {
            errors.add(ShowableException.getExceptionWithMessage("<html>Un ID ne peut être vide !</html>"));
            return false;
        }
        if (!ID_PATTERN.matcher(id).matches()) {
            errors.add(ShowableException.getExceptionWithMessage("<html>L'ID <b>" + id + "</b> est invalide !</html>"));
            return false;
        }
        return true;
    }
}
