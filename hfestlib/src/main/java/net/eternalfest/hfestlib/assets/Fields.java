package net.eternalfest.hfestlib.assets;

import java.awt.Image;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import net.eternalfest.hfestlib.assets.AssetsMap.AssetAlias;
import net.eternalfest.hfestlib.models.sprites.FieldTeleport;
import net.eternalfest.hfestlib.models.sprites.FieldType;
import net.eternalfest.hfestlib.models.sprites.FieldVortex;
import net.eternalfest.hfestlib.tools.InvalidXMLNodeException;
import net.eternalfest.hfestlib.tools.ShowableException;
import net.eternalfest.hfestlib.utils.Lazy;
import net.eternalfest.hfestlib.utils.MultiResult;
import org.jdom2.Document;
import org.jdom2.Element;

/*
 * Retro-compatibility notes.
 *
 * Legacy names for attributes and elements:
 * - "id" was previously "ID"
 */
public enum Fields {
    ;

    public static MultiResult<AssetsMap<FieldType>, ShowableException> fromDocument(Document doc, Path root) {
        List<ShowableException> errors = new ArrayList<>();
        List<FieldType> fields = new ArrayList<>();
        List<AssetAlias> aliases = new ArrayList<>();

        Element xmlRoot = doc.getRootElement();
        String dirAttr = xmlRoot.getAttributeValue("dir");
    	Path fieldDir = dirAttr == null ? root : root.resolve(dirAttr);

        for (Element el : xmlRoot.getChildren()) {
            switch(el.getName()) {
            case "alias":
            	AssetAlias.fromElement(el, errors).ifPresent(aliases::add);
                break;
            case "type":
            	FieldType field = elementToField(el, fieldDir, errors);
            	if (field != null)
            		fields.add(field);
                break;
            default:
                errors.add(new InvalidXMLNodeException(el));
            }
        }

        return AssetsMap.from(fields, aliases, FieldType::unknownWith, FieldType.UNKNOWN).addErrors(errors);
    }

    private static FieldType elementToField(Element el, Path fieldDir, List<ShowableException> errors) {
        Path file = fieldDir.resolve(el.getAttributeValue("file"));
        String id = AssetsUtils.getAttrValue(el, "id", "ID");
        if (!AssetsUtils.isIdValid(id, errors))
        	return null;

        String name = el.getAttributeValue("name");
        String skin = el.getAttributeValue("skin");
        skin = skin == null ? "" : skin;

        if(skin.isEmpty() && ("teleport".equals(id) || "vortex".equals(id))) {
            skin = id;
        }

        Lazy<Image> image = AssetsUtils.loadBitmap(fieldDir.resolve(file), errors);

        switch (skin) {
            case "teleport":
                return new FieldTeleport(image, name, id);
            case "vortex":
                return new FieldVortex(image, name, id);
            default:
                return new FieldType(image, name, id);
        }
    }

    public static AssetsMap<FieldType> empty() {
        return AssetsMap.empty(FieldType::unknownWith, FieldType.UNKNOWN);
    }
}
