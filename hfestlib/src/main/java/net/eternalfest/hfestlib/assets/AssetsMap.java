package net.eternalfest.hfestlib.assets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import net.eternalfest.hfestlib.models.sprites.IAsset;
import net.eternalfest.hfestlib.tools.DuplicateIDException;
import net.eternalfest.hfestlib.tools.ShowableException;
import net.eternalfest.hfestlib.tools.UnknownIDException;
import net.eternalfest.hfestlib.utils.MultiResult;
import org.jdom2.Element;

public class AssetsMap<T extends IAsset> implements Iterable<T> {
    public static class AssetAlias {
        public final String id;
        public final String ref;
        public final boolean isHidden;

        public AssetAlias(String id, String ref, boolean isHidden) {
            this.id = id;
            this.ref = ref;
            this.isHidden = isHidden;
        }

        public static Optional<AssetAlias> fromElement(Element el, List<ShowableException> errors) {
        	return fromElement(el, false, errors);
        }

        public static Optional<AssetAlias> fromElement(Element el, boolean defaultRef, List<ShowableException> errors) {
        	String aliasId = AssetsUtils.getAttrValue(el, "id", "ID");
            String refId = el.getAttributeValue("ref");
            boolean isHidden = el.getAttributeValue("hidden") != null;
            if (refId == null && defaultRef)
            	refId = aliasId;

            if(AssetsUtils.isIdValid(aliasId, errors) && AssetsUtils.isIdValid(refId, errors)) {
            	return Optional.of(new AssetAlias(aliasId, refId, isHidden));
            } else {
            	return Optional.empty();
            }
        }
    }

    private final Map<String, T> assetsById;
    private final List<T> assets;
    private final List<AssetAlias> aliases;
    private final Function<String, T> defaultById;
    private final T defaultNoId;
    private Map<String, List<String>> visibleAliases = null;
    private Map<String, T> fallbackById = null;

    private AssetsMap(Map<String, T> byId, List<T> assets, List<AssetAlias> aliases, Function<String, T> defaultById, T defaultNoId) {
        this.defaultById = defaultById;
        this.defaultNoId = defaultNoId;
        this.assetsById = byId;
        this.assets = assets;
        this.aliases = aliases;
    }

    private static <T extends IAsset> Map<String, T> makeMap(List<T> assets, List<AssetAlias> aliases) {
        Map<String, T> byId = assets.stream().collect(Collectors.toMap(T::getID, e -> e));

        for(AssetAlias alias: aliases) {
            if(byId.put(alias.id, byId.get(alias.ref)) != null)
                throw new IllegalArgumentException("alias " + alias.id + "is already an asset");
        }

        return byId;
    }

    public static <T extends IAsset> AssetsMap<T> empty(
            Function<String, T> defaultById, T defaultNoId) {
        return new AssetsMap<>(Collections.emptyMap(), Collections.emptyList(), Collections.emptyList(), defaultById, defaultNoId);
    }

    public static <T extends IAsset> AssetsMap<T> empty() {
        return empty(null, null);
    }

    public static <T extends IAsset> AssetsMap<T> withDefault(AssetsMap<T> assets, Function<String, T> defaultById) {
        return new AssetsMap<>(assets.assetsById, assets.assets, assets.aliases, defaultById, assets.defaultNoId);
    }

    public static <T extends IAsset> MultiResult<AssetsMap<T>, ShowableException> from(
            Iterable<T> assets, Iterable<AssetAlias> aliases,
            Function<String, T> defaultById, T defaultNoId) {
        ArrayList<ShowableException> errors = new ArrayList<>();
        List<T> list = new ArrayList<>();
        Set<String> ids = new HashSet<>();
        for (T asset : assets) {
            if (ids.add(asset.getID())) {
                list.add(asset);
            } else {
                errors.add(new DuplicateIDException(asset.getID()));
            }
        }

        List<AssetAlias> aliasesList = new ArrayList<>();
        for(AssetAlias alias: aliases) {
            int errLen = errors.size();
            if(!ids.add(alias.id))
                errors.add(new DuplicateIDException(alias.id));
            if(!ids.contains(alias.ref))
                errors.add(new UnknownIDException(alias.ref));
            if(errLen == errors.size())
                aliasesList.add(alias);
        }

        Map<String, T> byId = makeMap(list, aliasesList);
        return MultiResult.withErrors(new AssetsMap<>(byId, list, aliasesList, defaultById, defaultNoId), errors);
    }

    public static <T extends IAsset> MultiResult<AssetsMap<T>, ShowableException> from(
            Iterable<T> assets, Function<String, T> defaultById, T defaultNoId) {
        return from(assets, Collections.emptyList(), defaultById, defaultNoId);
    }

    public static <T extends IAsset> MultiResult<AssetsMap<T>, ShowableException> from(Iterable<T> assets) {
        return from(assets, null, null);
    }

    // Merge by always using the new assets
    public static <T extends IAsset> AssetsMap<T> merge(AssetsMap<T> base, AssetsMap<T> extra) {
        return merge(base, extra, (baseAsset, extraAsset) -> extraAsset);
    }

    public static <T extends IAsset> AssetsMap<T> merge(AssetsMap<T> base, AssetsMap<T> extra, BinaryOperator<T> merger) {
    	if(base.isEmpty())
    		return extra;
    	if(extra.isEmpty())
    		return base;

        List<T> assets = new ArrayList<>(base.assets);
        Map<String, Integer> indices = new HashMap<>();
        for(int i = 0; i < assets.size(); i++)
            indices.put(assets.get(i).getID(), i);

        // Add new assets, and merge them if needed (don't take aliases
        // into account, this is why we need the separate map)
        for(T newAsset: extra.assets) {
            Integer idx = indices.get(newAsset.getID());
            if(idx != null) {
                T oldAsset = assets.get(idx);
                assets.set(idx, merger.apply(oldAsset, newAsset));
            } else {
                assets.add(newAsset);
                indices.put(newAsset.getID(), -1);
            }
        }

        // Add aliases, if they don't conflict with existing assets or other aliases.
        // Note: we use 1's complement to store indices of already-added aliases.
        List<AssetAlias> aliases = new ArrayList<>();
        Stream.concat(base.aliases.stream(), extra.aliases.stream()).forEachOrdered(a -> {
        	Integer idx = indices.get(a.id);
        	if (idx == null) {
        		indices.put(a.id, ~indices.size());
        		aliases.add(a);
        	} else if (idx < 0) {
        		idx = ~idx;
        		// If we have two aliases with the same referent, merge them.
        		AssetAlias alias = aliases.get(idx);
        		if (alias.ref.equals(a.ref)) {
        			aliases.set(idx, new AssetAlias(alias.id, alias.ref, alias.isHidden && a.isHidden));
        		}
        	}
        });

        Map<String, T> byId = makeMap(assets, aliases);
        return new AssetsMap<>(byId, assets, aliases,
                extra.defaultById == null ? base.defaultById : extra.defaultById,
                extra.defaultNoId == null ? base.defaultNoId : extra.defaultNoId);
    }

    public static<K, V> Map<K, V> merge(Map<K, V> base, Map<K, V> extra) {
        return merge(base, extra, (baseVal, extraVal) -> extraVal);
    }

    private static<K, V> Map<K, V> merge(Map<K, V> base, Map<K, V> extra, BinaryOperator<V> merger) {
        Map<K, V> map = new HashMap<>(base);
        for(Map.Entry<K, V> entry: extra.entrySet())
            map.merge(entry.getKey(), entry.getValue(), merger);
        return map;
    }

    public boolean isEmpty() {
    	return assets.isEmpty() && aliases.isEmpty();
    }

    public boolean has(String id) {
    	// Don't look in fallback map.
    	return assetsById.containsKey(id);
    }

    public T byID(String id) {
    	T asset = assetsById.get(id);
    	if(asset == null) {
    		if(fallbackById != null)
    			asset = fallbackById.get(id);

    		if(defaultById != null) {
        		asset = defaultById.apply(id);
        		if(fallbackById == null)
        			fallbackById = new HashMap<>();
        		fallbackById.put(id, asset);
        	}
    	}

    	return asset;
    }

    public List<String> aliasesOf(String id) {
    	if (visibleAliases == null)
    		visibleAliases = computeVisibleAliases();

    	List<String> names = visibleAliases.get(id);
    	return names == null ? Collections.emptyList() : Collections.unmodifiableList(names);
    }

    private Map<String, List<String>> computeVisibleAliases() {
    	Map<String, List<String>> map = new HashMap<>();

    	// Primary ids are always visible
    	for (T asset: assets) {
    		List<String> list = new ArrayList<>();
    		String id = asset.getID();
    		list.add(id);
    		map.put(id, list);
    	}

    	for (AssetAlias alias: aliases) {
    		List<String> list = map.get(alias.ref);
    		if (list != null && !alias.isHidden) {
    			list.add(alias.id);
    		}
    	}

    	return map;
    }

    public List<T> list() {
        return Collections.unmodifiableList(assets);
    }

    @Override
    public Iterator<T> iterator() {
        return assets.iterator();
    }
}
