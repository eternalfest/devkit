package net.eternalfest.hfestlib.assets;

import java.awt.Image;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.kitfox.svg.SVGDiagram;
import com.kitfox.svg.SVGUniverse;
import net.eternalfest.hfestlib.assets.AssetsMap.AssetAlias;
import net.eternalfest.hfestlib.models.sprites.DefaultFrame;
import net.eternalfest.hfestlib.models.sprites.FrameImage;
import net.eternalfest.hfestlib.models.sprites.FrameSVG;
import net.eternalfest.hfestlib.models.sprites.IAssetFrame;
import net.eternalfest.hfestlib.models.sprites.Sprite;
import net.eternalfest.hfestlib.tools.DuplicateIDException;
import net.eternalfest.hfestlib.tools.InvalidXMLNodeException;
import net.eternalfest.hfestlib.tools.ShowableException;
import net.eternalfest.hfestlib.utils.Lazy;
import net.eternalfest.hfestlib.utils.MultiResult;
import net.eternalfest.hfestlib.utils.Utils;
import org.jdom2.Document;
import org.jdom2.Element;


/*
 * Retro-compatibility notes.
 *
 * Legacy names for attributes and elements:
 * - "attach_flip" was previously "attach2"
 * - "bad" was previously "fruit"
 * - "behavior" was previously "comportement"
 * - "id" was previously "ID"
 * - "order" was previously "ordre"
 */
public class Sprites {

    public final AssetsMap<Sprite> sprites;
    public final AssetsMap<Sprite> bads;
    public final AssetsMap<IAssetFrame> frames;
    public final Map<String, Sprite> specials;

    private Sprites(AssetsMap<Sprite> sprites, AssetsMap<Sprite> bads,
            AssetsMap<IAssetFrame> frames, Map<String, Sprite> specials) {
        this.sprites = sprites;
        this.bads = bads;
        this.frames = frames;
        this.specials = specials;
    }

    public static Sprite mergeSprites(Sprite base, Sprite extra) {
        List<IAssetFrame> frames = new ArrayList<>(base.getFrames());

        Map<String, Integer> oldById = new HashMap<>();
        for(int i = 0; i < frames.size(); i++)
            oldById.put(frames.get(i).getID(), i);

        for(IAssetFrame frame: extra.getFrames()) {
            Integer i = oldById.get(frame.getID());
            if(i == null)
                frames.add(frame);
            else frames.set(i, frame);
        }

        return new Sprite(extra.getName(), extra.getID(), frames, extra.getCapabilities());
    }

    public static Sprites empty() {
        return new Sprites(
                AssetsMap.empty(Sprite::unknownWith, Sprite.UNKNOWN),
                AssetsMap.empty(Sprite::unknownFruitWith, Sprite.UNKNOWN_FRUIT),
                AssetsMap.empty(DefaultFrame::with, DefaultFrame.DEFAULT),
                Collections.emptyMap());
    }

	public static MultiResult<Sprites, ShowableException> fromDocument(Document doc, Path root) {
        List<ShowableException> errors = new ArrayList<>();
        ArrayList<Sprite> sprites = new ArrayList<>();
        ArrayList<Sprite> bads = new ArrayList<>();
        HashMap<String, Sprite> specials = new HashMap<>();
        ArrayList<AssetAlias> spriteAliases = new ArrayList<>();
        ArrayList<AssetAlias> badAliases = new ArrayList<>();
        ArrayList<AssetAlias> frameAliases = new ArrayList<>();
        HashSet<String> badsSet = new HashSet<>(); // used for managing aliases

        SVGUniverse svgUniverse = new SVGUniverse();
        Element xmlRoot = doc.getRootElement();
        String dirAttr = xmlRoot.getAttributeValue("dir");
    	Path spriteDir = dirAttr == null ? root : root.resolve(dirAttr);

        for (Element el : xmlRoot.getChildren()) {
            String elemName = el.getName();
            switch(elemName) {
            case "alias":
            	AssetAlias alias = AssetAlias.fromElement(el, errors).orElse(null);
            	if (alias == null)
            		break;

            	if(badsSet.contains(alias.ref)) {
            		badAliases.add(alias);
            		badsSet.add(alias.id);
                } else {
                    spriteAliases.add(alias);
                }

                for(Element child: el.getChildren()) {
                    if(!child.getName().equals("image")) {
                        errors.add(new InvalidXMLNodeException(child));
                        continue;
                    }
                    AssetAlias.fromElement(child, true, errors).ifPresent(f -> {
                    	frameAliases.add(new AssetAlias(alias.id + ":" + f.id, alias.ref + ":" + f.ref, f.isHidden));
                    });
                }
                break;
            case "sprite":
                elementToSprite(el, spriteDir, false, errors, frameAliases, svgUniverse).ifPresent(sprites::add);
                break;
            case "fruit":
            case "bad":
                elementToSprite(el, spriteDir, true, errors, frameAliases, svgUniverse).ifPresent(b -> {
                    bads.add(b);
                    badsSet.add(b.getID());
                });
                break;
            default:
                elementToSprite(el, spriteDir, false, errors, frameAliases, svgUniverse).ifPresent(sp -> {
                    if(specials.containsKey(elemName))
                        errors.add(new DuplicateIDException(elemName));
                    else specials.put(elemName, sp);
                });
            }
        }

        List<IAssetFrame> frames = Stream.concat(
            Stream.concat(sprites.stream(), bads.stream()),
            specials.values().stream()
        ).flatMap(sp -> sp.getFrames().stream())
        .collect(Collectors.toList());


        return AssetsMap.from(frames, frameAliases, null, DefaultFrame.DEFAULT).merge(
        		AssetsMap.from(sprites, spriteAliases, Sprite::unknownWith, Sprite.UNKNOWN),
                AssetsMap.from(bads, badAliases, Sprite::unknownFruitWith, Sprite.UNKNOWN_FRUIT),
                (f, s, b) -> new Sprites(s, b, withFallbackFrames(f, s, b), specials)
            ).addErrors(errors);
    }

	// Allows frame ids to work across sprite aliases; required for proper support of old .lvl files.
	public static AssetsMap<IAssetFrame> withFallbackFrames(
			AssetsMap<IAssetFrame> frames,
			AssetsMap<Sprite> sprites,
			AssetsMap<Sprite> bads) {
		return AssetsMap.withDefault(frames, id -> {
            IAssetFrame frame = frames.byID(findFallbackFrameId(sprites, bads, id));
    		return frame == null ? DefaultFrame.with(id) : frame;
		});
	}

	private static String findFallbackFrameId(AssetsMap<Sprite> sprites, AssetsMap<Sprite> bads, String frameId) {
		int prefix = frameId.indexOf(':');
		if (prefix < 0)
			return frameId; // Give up and return original id.

		String id = frameId.substring(0, prefix);
		Sprite real;
		if (bads.has(id)) {
			real = bads.byID(id);
		} else if (sprites.has(id)) {
			real = sprites.byID(id);
		} else {
			return frameId; // Give up and return original id.
		}
		return real.getID() + frameId.substring(prefix);
	}

    private static Optional<Sprite> elementToSprite(Element elem, Path spriteDir, boolean isBad,
            List<ShowableException> errors, List<AssetAlias> frameAliases, SVGUniverse svgUniverse) {
    	String id = AssetsUtils.getAttrValue(elem, "id", "ID");
        String name = elem.getAttributeValue("name");
        if(!AssetsUtils.isIdValid(id, errors))
            return Optional.empty();

        List<IAssetFrame> spriteFrames = elem.getChildren().stream()
            .map(e -> elementToFrame(e, id, spriteDir, errors, frameAliases, svgUniverse))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
        if(spriteFrames.isEmpty()) {
            errors.add(ShowableException.getExceptionWithMessage("<html>Le sprite <b>" + id + "</b> n'a aucune image !</html>"));
            spriteFrames.add(DefaultFrame.with(id + ":<default>"));
        }

        int capabilities = 0;
        if(isBad) {
            capabilities |= Sprite.TYPE_FRUIT;
            String behavior = AssetsUtils.getAttrValue(elem, "behavior", "comportement");
            if(behavior != null) {
                switch(behavior) {
                case "attach":
                    capabilities |= Sprite.CAN_ROTATE | Sprite.AUTO_ORIENT_ATTACH;
                    break;
                case "attach2":
                case "attach_flip":
                    capabilities |= Sprite.CAN_ROTATE | Sprite.AUTO_ORIENT_ATTACH_FLIP;
                    break;
                case "attach_noflip":
                    capabilities |= Sprite.CAN_ROTATE | Sprite.AUTO_ORIENT_ATTACH_NOFLIP;
                    break;
                case "none":
                    break;
                default:
                    errors.add(ShowableException.getExceptionWithMessage("<html>Comportement inconnu <b>" + behavior + "</b> !</html>"));
                }
            }
            if(elem.getAttributeValue("flip") != null)
                capabilities |= Sprite.CAN_FLIP_H;
            if(AssetsUtils.getAttrValue(elem,  "order", "ordre") != null)
                capabilities |= Sprite.TYPE_FRUIT_ORDER;
        } else {
            String flip = elem.getAttributeValue("orientation");
            if("flipH".equals(flip) || "both".equals(flip))
                capabilities |= Sprite.CAN_FLIP_H;
            if("flipV".equals(flip) || "both".equals(flip))
                capabilities |= Sprite.CAN_FLIP_V;
            if(elem.getAttributeValue("rotate") != null)
                capabilities |= Sprite.CAN_ROTATE;
            if(Utils.parseInt(elem.getAttributeValue("layer")) == 1)
                capabilities |= Sprite.DEFAULT_LAYER_FRONT;
        }

        return Optional.of(new Sprite(name, id, spriteFrames, capabilities));
    }

    private static IAssetFrame elementToFrame(Element el, String spriteId, Path spriteDir,
            List<ShowableException> errors, List<AssetAlias> frameAliases, SVGUniverse svgUniverse) {
        switch(el.getName()) {
        case "alias":
        	AssetAlias.fromElement(el, errors).ifPresent(a -> {
        		frameAliases.add(new AssetAlias(spriteId + ":" + a.id, spriteId + ":" + a.id, a.isHidden));
        	});
            return null;

        case "image":
            String frameId = AssetsUtils.getAttrValue(el, "id", "ID");
            if(!AssetsUtils.isIdValid(frameId, errors))
                return null;
            String frameName = el.getAttributeValue("name");
            String fullId = spriteId + ":" + frameId;
            Path file = spriteDir.resolve(el.getAttributeValue("file"));
            int centerX = Utils.parseInt(el.getAttributeValue("centerX"));
            int centerY = Utils.parseInt(el.getAttributeValue("centerY"));

            if(FileType.fromPath(file) == FileType.SVG) {
                Lazy<SVGDiagram> svg = AssetsUtils.loadSVG(file, svgUniverse, errors);
                return new FrameSVG(frameName, fullId, svg, centerX, centerY);
            } else {
                Lazy<Image> image = AssetsUtils.loadBitmap(file, errors);
                return new FrameImage(frameName, fullId, image, centerX, centerY);
            }

        default:
            errors.add(new InvalidXMLNodeException(el));
            return null;
        }
    }
}
