package net.eternalfest.hfestlib.assets;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import net.eternalfest.hfestlib.models.sprites.BackgroundType;
import net.eternalfest.hfestlib.models.sprites.FieldType;
import net.eternalfest.hfestlib.models.sprites.IAssetFrame;
import net.eternalfest.hfestlib.models.sprites.Sprite;
import net.eternalfest.hfestlib.models.sprites.TileType;
import net.eternalfest.hfestlib.tools.ShowableException;
import net.eternalfest.hfestlib.utils.FileUtils;
import net.eternalfest.hfestlib.utils.MultiResult;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;

/*
 * Retro-compatibility notes.
 *
 * The legacy 'directory-style' configuration is supported; see
 * `resources/.../assets-legacy-bridge.xml` for more details.
 */
public class Assets {
    private final List<Path> rootPaths;
    private final AssetsMap<BackgroundType> backgrounds;
    private final AssetsMap<TileType> tiles;
    private final AssetsMap<FieldType> fields;
    private final AssetsMap<IAssetFrame> frames;
    private final AssetsMap<Sprite> sprites;
    private final AssetsMap<Sprite> bads;
    private final Map<String, Sprite> specials;

    private Assets(
        List<Path> rootPaths,
        AssetsMap<BackgroundType> backgrounds,
        AssetsMap<TileType> tiles,
        AssetsMap<FieldType> fields,
        AssetsMap<IAssetFrame> frames,
        AssetsMap<Sprite> sprites,
        AssetsMap<Sprite> bads,
        Map<String, Sprite> specials) {
        this.rootPaths = rootPaths;
        this.backgrounds = backgrounds;
        this.tiles = tiles;
        this.fields = fields;
        this.frames = frames;
        this.sprites = sprites;
        this.bads = bads;
        this.specials = specials;
    }

    public static MultiResult<Assets, ShowableException> fromPath(Path path) {
    	boolean isLegacy = Files.isDirectory(path);

    	Path root;
    	Document doc;
    	if (isLegacy) {
    		root = path;
    		doc = FileUtils.loadXMLResource("assets-legacy-bridge.xml");
    	} else {
    		root = path.getParent();
    		try {
				doc = FileUtils.loadXML(path);
			} catch (ShowableException e) {
				return MultiResult.withError(empty(), e);
			}
    	}

        ArrayList<ShowableException> errors = new ArrayList<>();

        Sprites sprites = loadAssetKind(root, doc, "sprites", isLegacy, errors, Sprites::fromDocument);
        AssetsMap<BackgroundType> bgs = loadAssetKind(root, doc, "backgrounds", isLegacy, errors, Backgrounds::fromDocument);
        AssetsMap<TileType> tiles = loadAssetKind(root, doc, "tiles", isLegacy, errors, Tiles::fromDocument);
        AssetsMap<FieldType> fields = loadAssetKind(root, doc, "rays", isLegacy, errors, Fields::fromDocument);

        return MultiResult.withErrors(new Assets(
            Collections.singletonList(path),
            bgs,
            tiles,
            fields,
            sprites.frames,
            sprites.sprites,
            sprites.bads,
            sprites.specials
        ), errors);
    }

	private static<T> T loadAssetKind(
		Path root,
		Document rootDoc,
		String kind,
		boolean copyAttributes,
		List<ShowableException> errors,
		BiFunction<Document, Path, MultiResult<T, ShowableException>> loader
    ) {
		Element el = rootDoc.getRootElement().getChild(kind);
		if (el == null) {
			// Treat missing nodes as empty.
			el = new Element(kind);
		}

		Document doc;
		String pathAttr = el.getAttributeValue("path");
		if (pathAttr == null) {
			doc = new Document(el.detach());
		} else {
			Path path = root.resolve(pathAttr);
			root = path.getParent();
			try {
				doc = FileUtils.loadXML(path);
			} catch (ShowableException e) {
				errors.add(e);
				doc = new Document(new Element(kind));
			}
		}

		if (copyAttributes) {
			el.removeAttribute("path");
			Element xml = doc.getRootElement();
			for (Attribute attr: el.getAttributes()) {
				xml.setAttribute(attr.clone());
			}
		}

		MultiResult<T, ShowableException> result = loader.apply(doc, root);
		errors.addAll(result.errors());
		return result.value();
    }

    public static Assets empty() {
        Sprites sprites = Sprites.empty();
        return new Assets(
            Collections.emptyList(),
            Backgrounds.empty(),
            Tiles.empty(),
            Fields.empty(),
            sprites.frames,
            sprites.sprites,
            sprites.bads,
            sprites.specials
        );
    }

    public static Assets merge(Assets base, Assets extra) {
        List<Path> rootPaths = new ArrayList<>(base.rootPaths);
        rootPaths.addAll(extra.rootPaths);
        AssetsMap<Sprite> sprites = AssetsMap.merge(base.sprites, extra.sprites, Sprites::mergeSprites);
        AssetsMap<Sprite> bads = AssetsMap.merge(base.bads, extra.bads, Sprites::mergeSprites);
        AssetsMap<IAssetFrame> frames = AssetsMap.merge(base.frames, extra.frames);
        return new Assets(
            rootPaths,
            AssetsMap.merge(base.backgrounds, extra.backgrounds),
            AssetsMap.merge(base.tiles, extra.tiles),
            AssetsMap.merge(base.fields, extra.fields),
            Sprites.withFallbackFrames(frames, sprites, bads),
            sprites,
            bads,
            AssetsMap.merge(base.specials, extra.specials)
        );
    }

    public AssetsMap<BackgroundType> backgrounds() {
        return backgrounds;
    }

    public AssetsMap<TileType> tiles() {
        return tiles;
    }

    public AssetsMap<FieldType> fields() {
        return fields;
    }

    public AssetsMap<Sprite> sprites() {
        return sprites;
    }

    public AssetsMap<IAssetFrame> frames() {
        return frames;
    }

    public AssetsMap<Sprite> bads() {
        return bads;
    }

    public Sprite getSpecial(String id) {
        return specials.computeIfAbsent(id, Sprite::unknownWith);
    }

    public List<Path> getRootPaths() {
        return Collections.unmodifiableList(rootPaths);
    }
}
