package net.eternalfest.hfestlib.assets;

import java.awt.Image;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import net.eternalfest.hfestlib.assets.AssetsMap.AssetAlias;
import net.eternalfest.hfestlib.models.sprites.BackgroundType;
import net.eternalfest.hfestlib.tools.InvalidXMLNodeException;
import net.eternalfest.hfestlib.tools.ShowableException;
import net.eternalfest.hfestlib.utils.Lazy;
import net.eternalfest.hfestlib.utils.MultiResult;
import org.jdom2.Document;
import org.jdom2.Element;

/*
 * Retro-compatibility notes.
 *
 * Legacy names for attributes and elements:
 * - "id" was previously "ID"
 */
public enum Backgrounds {
    ;

    public static MultiResult<AssetsMap<BackgroundType>, ShowableException> fromDocument(Document doc, Path root) {
        List<ShowableException> errors = new ArrayList<>();
        List<BackgroundType> backgrounds = new ArrayList<>();
        List<AssetAlias> aliases = new ArrayList<>();

        Element xmlRoot = doc.getRootElement();
        String dirAttr = xmlRoot.getAttributeValue("dir");
    	Path bgDir = dirAttr == null ? root : root.resolve(dirAttr);

        for (Element el : xmlRoot.getChildren()) {
            switch(el.getName()) {
            case "alias":
            	AssetAlias.fromElement(el, errors).ifPresent(aliases::add);
                break;
            case "type":
            	String id = AssetsUtils.getAttrValue(el, "id", "ID");
            	if (!AssetsUtils.isIdValid(id, errors))
            		break;

                String name = el.getAttributeValue("name");
                Path file = bgDir.resolve(el.getAttributeValue("file"));

                Lazy<Image> image = AssetsUtils.loadBitmap(file, errors);
                backgrounds.add(new BackgroundType(image, name, id));
                break;
            default:
                errors.add(new InvalidXMLNodeException(el));
            }
        }

        return AssetsMap.from(backgrounds, aliases, BackgroundType::unknownWith, BackgroundType.UNKNOWN)
                .addErrors(errors);
    }

    public static AssetsMap<BackgroundType> empty() {
        return AssetsMap.empty(BackgroundType::unknownWith, BackgroundType.UNKNOWN);
    }
}
