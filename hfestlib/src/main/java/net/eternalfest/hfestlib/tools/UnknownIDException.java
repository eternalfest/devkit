package net.eternalfest.hfestlib.tools;

public class UnknownIDException extends ShowableException {

    private static final long serialVersionUID = 7022730166808210305L;

    public UnknownIDException(String path) {
        super(path);
    }

    @Override
    public String getFullMessage() {
        return "<html>L'ID <b>" + this.getMessage() + "</b> ne correspond à aucune ressource !</html>";
    }
}
