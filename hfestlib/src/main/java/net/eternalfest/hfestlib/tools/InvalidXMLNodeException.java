package net.eternalfest.hfestlib.tools;

import org.jdom2.Element;

public class InvalidXMLNodeException extends ShowableException {
    private static final long serialVersionUID = 1L;

    public InvalidXMLNodeException(Element node) {
        super(node.getName());
    }

    @Override
    public String getFullMessage() {
        return "<html>Le nom de balise <b>" + this.getMessage() + "</b> est invalide !</html>";
    }
}
