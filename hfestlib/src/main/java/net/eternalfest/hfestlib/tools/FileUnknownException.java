package net.eternalfest.hfestlib.tools;

import java.nio.file.Path;

public class FileUnknownException extends ShowableException {

    private static final long serialVersionUID = -2447958553355702697L;

    public FileUnknownException(Path path) {
        super(path.toAbsolutePath().toString());
    }

    @Override
    public String getFullMessage() {
        return "<html>Le fichier <b>" + this.getMessage() + "</b> est illisible !</html>";
    }

}
