package net.eternalfest.hfestlib.tools;

import java.nio.file.Path;

public class FileNotWritableException extends ShowableException {

    private static final long serialVersionUID = 3704204256559333802L;

    public FileNotWritableException(Path path) {
        super(path.toAbsolutePath().toString());
    }

    @Override
    public String getFullMessage() {
        return "<html>Il est impossible d'écrire dans le fichier <b>" + this.getMessage() + "</b> !</html>";
    }

}
