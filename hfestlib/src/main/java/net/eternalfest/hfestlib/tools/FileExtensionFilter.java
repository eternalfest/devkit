package net.eternalfest.hfestlib.tools;

import javax.swing.filechooser.FileFilter;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Vector;


public class FileExtensionFilter extends FileFilter implements FilenameFilter {

    public static final int ADD_MODE = 1;
    public static final int REMOVE_MODE = 2;

    private Vector<String> listExtensions;
    private String description;
    private int mode;

    public FileExtensionFilter(int type) {
        listExtensions = new Vector<String>();
        mode = type;
    }

    public FileExtensionFilter() {
        this(ADD_MODE);
    }

    public FileExtensionFilter(String ext, String desc, int mode) {
        listExtensions = new Vector<String>();
        addExtension(ext);
        description = desc;
        this.mode = mode;
    }

    public FileExtensionFilter(String ext, String desc) {
        this(ext, desc, ADD_MODE);
    }

    public void addExtension(String ext) {
        boolean matches = false;
        for (int i = 0; i < listExtensions.size(); i++) {
            if (listExtensions.get(i).equals(ext))
                matches = true;
        }
        if (!matches)
            listExtensions.add(ext);
    }

    public void removeExtension(String ext) {
        for (int i = 0; i < listExtensions.size(); i++) {
            if (listExtensions.get(i).equals(ext))
                listExtensions.remove(i);
        }
    }


    @Override
    public boolean accept(File arg0) {
        String str = arg0.getAbsolutePath();

        if (arg0.isDirectory())
            return (mode == ADD_MODE);

        for (int i = 0; i < listExtensions.size(); i++) {
            if (str.matches(".+\\." + listExtensions.get(i)))
                return (mode == ADD_MODE);
        }
        return !(mode == ADD_MODE);
    }

    @Override
    public String getDescription() {
        return new String(description);
    }

    @Override
    public boolean accept(File arg0, String arg1) {
        for (int i = 0; i < listExtensions.size(); i++) {
            if (arg1.matches(".+\\." + listExtensions.get(i)))
                return (mode == ADD_MODE);
        }
        return !(mode == ADD_MODE);
    }

}
