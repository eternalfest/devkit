package net.eternalfest.hfestlib.tools;


import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import net.eternalfest.hfestlib.assets.Assets;
import net.eternalfest.hfestlib.utils.FileUtils;
import net.eternalfest.hfestlib.utils.MultiResult;
import org.jdom2.Element;


public enum ResourceManager {
    ;

    public static final String XML_ASSETS_NAME = "assets";
    public static final String XML_ASSET_NAME = "asset";
    public static final String XML_NO_DEFAULT_ATTR = "no-default";

    private static final Map<List<Path>, Assets> ASSETS_REGISTRY = new HashMap<>();
    private static final Optional<Path> DEFAULT_ASSETS_PATH = getDefaultAssetsPath(
    	"HFESTLIB_ASSETS_DEFAULT",
    	"assets/assets.xml",
    	"assets"
    );

    public static void clearRegistry() {
        System.err.println("Clearing assets registry");
        ASSETS_REGISTRY.clear();
    }

    private static Path getJarPath() throws RuntimeException {
        try {
            return Paths.get(Assets.class.getProtectionDomain().getCodeSource().getLocation().toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private static Optional<Path> getDefaultAssetsPath(String envVar, String... locations) throws RuntimeException {
        String defaultAssetPath = getEnvVar(envVar);
        if(defaultAssetPath != null)
            return defaultAssetPath.isEmpty() ? Optional.empty() : Optional.of(Paths.get(defaultAssetPath));
        Path jar = getJarPath();

        for (String loc: locations) {
        	Path assets = jar.resolveSibling(loc);
            if(Files.isReadable(assets))
                return Optional.of(assets);
            Path assetsLegacy = jar.getParent().resolveSibling(loc);

            if(Files.isReadable(assetsLegacy))
                return Optional.of(assetsLegacy);
        }
        // Better than returning empty; this will cause an error showing the path of the missing default assets.
        return Optional.of(jar.resolveSibling(locations[0]));
    }

    /**
     * Creates a new `Assets` instance using the default algorithm based on environment variables.
     * <p>
     * If `HFESTLIB_ASSETS_ROOT` is defined, it is used as the only assets directory.
     * Otherwise, the default directory is merged with the directories at `HFESTLIB_ASSETS_PATHS`.
     * This variable is an ordered colon separated list of paths. If it is missing, it is treated as
     * an empty list.
     *
     * The default path is `{., ./..}/{assets/assets.xml, assets}`, relative to the JAR file.
     * It can be overriden with the environment variable `HFESTLIB_ASSETS_DEFAULT` (if it is empty,
     * no default assets will be used).
     *
     * @return A resolved `Assets` instance.
     */
    public static Assets getDefault() {
        return get(getPathsFromEnv());
    }

    private static List<Path> getPathsFromEnv() {
        String exclusiveAssetsPath = getEnvVar("HFESTLIB_ASSETS_ROOT", "efest_ressources_root");
        if (exclusiveAssetsPath != null) {
            return new ArrayList<>(Collections.singleton(Paths.get(exclusiveAssetsPath)));
        }

        List<Path> paths = new ArrayList<>();
        DEFAULT_ASSETS_PATH.ifPresent(paths::add);
        String extraPathsStr = getEnvVar("HFESTLIB_ASSETS_PATHS", "ETERNALFEST_ASSETS_PATH");
        if (extraPathsStr != null) {
            for (String path : extraPathsStr.split(":")) {
                paths.add(Paths.get(path));
            }
        }

        return paths;
    }

    private static String getEnvVar(String... keys) {
        for(String key: keys) {
            String val = System.getenv(key);
            if(val != null)
                return val;
        }
        return null;
    }

    public static Assets getFromXml(Element assets, Path config) throws BadConfigException {
        List<Path> paths = assets.getAttribute(XML_NO_DEFAULT_ATTR) == null ? getPathsFromEnv() : new ArrayList<>();

        for (Element asset : assets.getChildren()) {
            if ("asset".equals(asset.getName())) {
                String file = asset.getTextTrim();
                if (file.isEmpty()) {
                    throw new BadConfigException(config + ": Chemin vide dans nœud 'asset'!");
                }
                paths.add(config.resolveSibling(file));
            }
        }
        if (paths.isEmpty()) {
            throw new BadConfigException(config + ": Aucun dossier de ressource n'a été défini!");
        }
        return get(paths);
    }

    public static Assets getFromAmbientConfig(Path location) throws ShowableException {
        return getFromAmbientConfig(location, "hfest-editor-config.xml", "config.xml");
    }

    public static Assets getFromAmbientConfig(Path location, String... configFileNames) throws ShowableException {
        Path configPath = FileUtils.findInAncestors(location, configFileNames).orElse(null);
        if (configPath == null)
            return getDefault();
        Element xml = FileUtils.loadXML(configPath).getRootElement().getChild("assets");
        if (xml == null) {
            return getDefault();
        }
        return getFromXml(xml, configPath);
    }

    public static synchronized Assets get(List<Path> paths) {
        System.err.println(paths.stream()
            .map(Path::toString)
            .collect(Collectors.joining(", ", "Loading assets at [", "]")));

        Assets assets = ASSETS_REGISTRY.get(paths);
        if (assets != null) return assets;

        assets = Assets.empty();
        for (Path path : paths) {
            assets = Assets.merge(assets, getInner(path));
        }

        ASSETS_REGISTRY.put(paths, assets);
        return assets;
    }

    public static synchronized Assets get(Path path) {
        System.err.println("Loading assets at " + path.toAbsolutePath());
        return getInner(path);
    }


    private static Assets getInner(Path path) {
        return ASSETS_REGISTRY.computeIfAbsent(Collections.singletonList(path), d -> {
            MultiResult<Assets, ShowableException> assets = Assets.fromPath(path);
            if (!assets.errors().isEmpty()) {
                new MultiShowableException(
                    "le chargement des ressources de <b>" + path.toAbsolutePath() + "</b>.",
                    assets.errors()
                ).show();
            }
            return assets.value();
        });
    }
}
