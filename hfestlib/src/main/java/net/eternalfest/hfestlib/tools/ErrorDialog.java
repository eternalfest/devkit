package net.eternalfest.hfestlib.tools;

import javax.swing.*;
import java.awt.*;

public class ErrorDialog extends JDialog {
    private static final long serialVersionUID = -8771373666963273841L;

    JLabel text = new JLabel();
    JTextArea messageArea = new JTextArea();
    JScrollPane messagePane;
    JButton displayDetails = new JButton(), okButton = new JButton();

    public ErrorDialog(String title, String message, String details) {
        super((JFrame) null, true);

        this.setTitle(title);

        createComponents(message, details);

        displayDetails.addActionListener(event -> {
            if (messagePane.isVisible()) {
                messagePane.setVisible(false);
                displayDetails.setText("Plus de détails");
            } else {
                messagePane.setVisible(true);
                //messagePane.getViewport().setViewPosition(new java.awt.Point(0, 0));
                displayDetails.setText("Masquer les détails");
            }

            this.pack();
        });

        okButton.addActionListener(event -> this.setVisible(false));

        messagePane.setVisible(false);
        displayDetails.setText("Plus de détails");
        okButton.requestFocusInWindow();


        this.setMinimumSize(new Dimension(400, 0));
        this.pack();
        this.setResizable(false);
        this.setLocationRelativeTo(null);

    }

    private void createComponents(String msg, String details) {
        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));

        text.setAlignmentX(LEFT_ALIGNMENT);
        text.setBorder(BorderFactory.createEmptyBorder(7, 7, 5, 7));
        this.add(text);

        this.add(Box.createVerticalStrut(10));

        JPanel buttonPane = new JPanel(new BorderLayout());
        buttonPane.setAlignmentX(LEFT_ALIGNMENT);

        okButton.setText("OK");

        buttonPane.add(displayDetails, BorderLayout.WEST);
        buttonPane.add(okButton, BorderLayout.EAST);

        this.add(buttonPane);

        messageArea.setEditable(false);
        messageArea.setLineWrap(true);
        messageArea.setFont(messageArea.getFont().deriveFont(11f));


        messagePane = new JScrollPane(messageArea);
        messagePane.setAlignmentX(LEFT_ALIGNMENT);
        messagePane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        //messagePane.setPreferredSize(new Dimension(450, 300));
        this.add(messagePane);

        text.setText(msg);
        text.setIcon(javax.swing.UIManager.getIcon("OptionPane.errorIcon"));

        messageArea.setRows((int) (Math.min(10, Math.max(4, details.chars().filter(c -> c == '\n').count() * 1.5))));
        messageArea.setText(details);
    }
}


