package net.eternalfest.hfestlib.tools;

import javax.swing.JOptionPane;

public class ShowableException extends Exception {

    private static final long serialVersionUID = -8306881123581122621L;

    private static boolean isGUI = true;

    public ShowableException(String msg) {
        super(msg);
    }

    public ShowableException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public ShowableException(Throwable cause) {
        super(cause);
    }

    public static boolean isGUI() {
        return isGUI;
    }

    public static void setGUI(boolean v) {
        isGUI = v;
    }

    public static ShowableException getExceptionWithMessage(final String msg) {
        return new ShowableException((String) null) {
            private static final long serialVersionUID = -3894517220092115934L;

            @Override
            public String getFullMessage() {
                return msg;
            }
        };
    }

    public String getFullMessage() {
        return "<html>Erreur inconnue !<br/>Message : <pre>" + this.getMessage() + "</pre></html>";
    }

    public String getFullMessageNoHTML() {
        return getFullMessage().replaceAll("<br/>", "\n").replaceAll("<[^>]*>", "")
            .replaceAll("&gt;", ">").replaceAll("&lt;", "<");
    }

    public void show() {
        System.err.println("Erreur: " + getFullMessageNoHTML());
        this.printStackTrace();
        if (isGUI)
            JOptionPane.showMessageDialog(null, getFullMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
    }
}
