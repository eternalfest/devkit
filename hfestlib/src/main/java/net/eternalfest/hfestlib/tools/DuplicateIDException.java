package net.eternalfest.hfestlib.tools;

public class DuplicateIDException extends ShowableException {

    private static final long serialVersionUID = -8902060256844697886L;

    public DuplicateIDException(String path) {
        super(path);
    }

    @Override
    public String getFullMessage() {
        return "<html>L'ID <b>" + this.getMessage() + "</b> est utilisé par plusieurs ressources différentes !</html>";
    }
}
