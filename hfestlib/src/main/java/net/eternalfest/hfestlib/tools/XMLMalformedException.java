package net.eternalfest.hfestlib.tools;

import org.jdom2.JDOMException;

import java.util.regex.Pattern;

public class XMLMalformedException extends ShowableException {

    private static final long serialVersionUID = -1235513355868392226L;

    JDOMException xmlEx;

    public XMLMalformedException(String path, JDOMException e) {
        super(path, e);
        if (e == null)
            xmlEx = new JDOMException("Raison inconnue.");
        else xmlEx = e;
    }

    @Override
    public String getFullMessage() {
        String msg = xmlEx.getMessage().replaceFirst(" of document file:.*/" + Pattern.quote(this.getMessage()), "");
        return "<html><p>Le fichier <b>" + this.getMessage() + "</b> est malformé !</p><br/><pre>" + msg + "</pre></html>";
    }

}
