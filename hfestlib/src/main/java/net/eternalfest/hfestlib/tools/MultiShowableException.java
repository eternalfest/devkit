package net.eternalfest.hfestlib.tools;

import java.util.Collections;
import java.util.List;

public class MultiShowableException extends ShowableException {
    private static final long serialVersionUID = 2833299178602129761L;

    private final List<ShowableException> errors;

    public MultiShowableException(String msg, List<ShowableException> errors) {
        super(errors.size() + " erreur(s) durant " + msg);
        this.errors = errors;
    }

    public List<ShowableException> getErrors() {
        return Collections.unmodifiableList(errors);
    }

    @Override
    public String getFullMessage() {
        return "<html>" + this.getMessage() + "</html>";
    }

    @Override
    public String getFullMessageNoHTML() {
        StringBuilder builder = new StringBuilder();
        builder.append(super.getFullMessageNoHTML());
        builder.append('\n');
        for (ShowableException error : errors) {
            builder.append("    - ");
            builder.append(error.getFullMessageNoHTML());
            builder.append('\n');
        }
        return builder.toString();
    }

    @Override
    public void show() {
        System.err.println("Erreur: " + getFullMessageNoHTML());
        if (isGUI())
            new ErrorDialog("Erreur", getFullMessage(), getFullMessageNoHTML()).setVisible(true);
    }
}
