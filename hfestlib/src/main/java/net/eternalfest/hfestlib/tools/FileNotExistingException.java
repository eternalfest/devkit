package net.eternalfest.hfestlib.tools;

import java.nio.file.Path;

// TODO(demurgos): Rename to FileNotFoundException
public class FileNotExistingException extends ShowableException {

    private static final long serialVersionUID = 433429571979342138L;

    public FileNotExistingException(Path path) {
        super(path.toAbsolutePath().toString());
    }

    @Override
    public String getFullMessage() {
        return "<html>Le fichier <b>" + this.getMessage() + "</b> est introuvable !</html>";
    }
}
