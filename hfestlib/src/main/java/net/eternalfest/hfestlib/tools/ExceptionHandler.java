package net.eternalfest.hfestlib.tools;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        StringWriter stackTraceWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stackTraceWriter));
        String stackTrace = stackTraceWriter.toString();

        System.err.println("CRITICAL ERROR : unhandled exception");
        System.err.println(stackTrace);

        String error = "<html>Une erreur inattendue est survenue : <br/><pre>" + e.toString() + "</pre></html>";
        new ErrorDialog("Erreur critique", error, stackTrace).setVisible(true);

        System.exit(-1);
    }

}
