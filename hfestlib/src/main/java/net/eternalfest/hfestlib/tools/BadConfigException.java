package net.eternalfest.hfestlib.tools;

public class BadConfigException extends ShowableException {


    private static final long serialVersionUID = 1112779802238144687L;

    public BadConfigException(String msg) {
        super(msg);
    }

    @Override
    public String getFullMessage() {
        return "Config invalide : " + this.getMessage();
    }


}
