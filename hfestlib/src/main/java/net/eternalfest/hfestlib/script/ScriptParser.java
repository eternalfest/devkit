package net.eternalfest.hfestlib.script;

import java.io.IOException;
import java.io.Reader;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import net.eternalfest.hfestlib.data.AbstractData;
import net.eternalfest.hfestlib.data.DataBool;
import net.eternalfest.hfestlib.data.DataDouble;
import net.eternalfest.hfestlib.data.DataInt;
import net.eternalfest.hfestlib.parser.Parser;
import net.eternalfest.hfestlib.script.ScriptEvent.Parameter;
import net.eternalfest.hfestlib.script.ScriptToken.Spanned;
import net.eternalfest.hfestlib.script.ScriptToken.Type;
import net.eternalfest.hfestlib.utils.Result;
import net.eternalfest.hfestlib.utils.Utils;

public enum ScriptParser {
    ;

    private static Parser<ScriptToken, Script> INSTANCE = null;

    public static Script parse(Reader reader) throws ScriptParseException {
        try (ScriptLexer lexer = new ScriptLexer(reader)) {
            Parser<ScriptToken, Script> parser = ScriptParser.get();

            Parser.ExceptionFactory<ScriptToken, ScriptParseException> tempFactory = e -> {
                return new ScriptParseException(ScriptParseException.Type.SYNTAX, e.getMessage(), e.getToken().getSpan());
            };

            Script result = parser.parse(lexer.asSupplier(), tempFactory);
            return result;

        } catch (IOException e) {
            throw new ScriptParseException(ScriptParseException.Type.LEXER, "IO error while reading file", e);
        }
    }

    /*
     EBNF grammar :
           Literal      := <NumberLit> | <StringLit> | <BoolLit>

           VariablePart := ( '.' <Identifier> ) | ( '[' Expression ']' )
           ExprVariable := <Identifier> VariablePart*
           ExprCall     := <Identifier> '(' [Expression (',' Expression)*] ')'
           ExprConst    := ('#' <ExprCall>) | ('#' <ExprVariable>) | ('#(' <Expression> ')')
           Expression   := Literal | ExprVariable | ExprCall | ExprConst | '(' Expression ')'

           Parameter    := <Identifier> ['=' Expression]
           ParamList    := ( '(' [Parameter (',' Parameter)*] ')' ) | Expression
           Event        := <Identifier> [ParamList] Parameter* (';' | Block)
           VarAssign    := ExprVariable <AssignOp> Expression ';'

           Block        := '{' ( VarAssign | ('(' Expr ')') | Event )* '}'

           Script       := Statement* <EOF>

       */
    public static Parser<ScriptToken, Script> get() {
        if (INSTANCE != null)
            return INSTANCE;


        Parser.Dummy<ScriptToken, ScriptExpr> expression = Parser.dummy();

        Parser<ScriptToken, ScriptValue> literal = token(Type.NUMBER_LIT)
            .or(token(Type.STRING_LIT))
            .or(token(Type.BOOL_LIT_TRUE))
            .or(token(Type.BOOL_LIT_FALSE))
            .map(ScriptValue::of);

        Parser<ScriptToken, ScriptCall> exprCall =
            token(Type.IDENTIFIER, ScriptToken::getValue)
            .thenIgnore(token(Type.L_PAREN))
            .then(expression.list(token(Type.COMMA)), ScriptCall::new)
            .thenIgnore(token(Type.R_PAREN));

        Parser<ScriptToken, ScriptVariable> exprVariable =
            token(Type.IDENTIFIER, ScriptToken::getValue).then(
                token(Type.DOT).ignoreThen(token(Type.IDENTIFIER,
                    tok -> new ScriptVariable.Access(tok.getValue())))
                .or(token(Type.L_BRACKET).ignoreThen(expression).thenIgnore(token(Type.R_BRACKET))
                    .map(ScriptVariable.Access::new))
                .zeroOrMore(),
                ScriptVariable::new);

        Parser<ScriptToken, ScriptExpr> exprVarOrCall =
    		Parser.lookAhead(Type.IDENTIFIER.matcher(), Type.L_PAREN.matcher())
            	.ignoreThen(Parser.cast(exprCall, ScriptExpr.class))
            	.or(exprVariable);

        Parser<ScriptToken, ScriptConst> exprConst =
        	token(Type.HASH).then(exprVarOrCall, ScriptParser::makeExprConst)
        		.or(token(Type.HASH_L_PAREN).then(expression, ScriptParser::makeExprConst).thenIgnore(token(Type.R_PAREN)));

        expression.set(parseExpression(
            Parser.cast(literal, ScriptExpr.class)
            .or(
                Parser.lookAhead(Type.IDENTIFIER.matcher(), Type.L_PAREN.matcher())
                .ignoreThen(Parser.cast(exprCall, ScriptExpr.class))
            )
            .or(exprVariable)
            .or(exprConst)
            .or(token(Type.L_PAREN).ignoreThen(expression).thenIgnore(token(Type.R_PAREN)))
        ));


        Parser.Dummy<ScriptToken, List<ScriptNode>> block = Parser.dummy();

        ScriptValue paramTrueValue = new ScriptValue(DataBool.TRUE);
        Parser<ScriptToken, Parameter<ScriptExpr>> parameter =
            token(Type.IDENTIFIER).then(token(Type.ASSIGN)
                .ignoreThen(expression).maybe(), (name, value) -> {
                return new Parameter<>(name.toSpanned(), value.orElse(paramTrueValue));
            });

        Parser<ScriptToken, Result<List<Parameter<ScriptExpr>>, ScriptExpr>> paramsOrExpr =
            Parser.lookAhead(Type.IDENTIFIER.matcher(), Type.ASSIGN.matcher().or(Type.COMMA.matcher())).then(
                parameter.list(token(Type.COMMA)),
                (x, list) -> Result.<List<Parameter<ScriptExpr>>, ScriptExpr>ok(list)
            ).or(expression.map(Result::fail));

        Parser<ScriptToken, ScriptEvent> event = token(Type.IDENTIFIER)
            .then(
                token(Type.L_PAREN).ignoreThen(paramsOrExpr.maybe()).thenIgnore(token(Type.R_PAREN))
                .maybe(),
                (name, params) -> params.flatMap(x -> x)
                .orElseGet(() -> Result.ok(Collections.emptyList()))
                .<ScriptEvent>biFlatMap(
                    list -> makeEvent(name, list),
                    expr -> new ScriptEvent(name.toSpanned(), expr)
                )
            ).then(parameter.zeroOrMore(), ScriptParser::addModifiersToEvent)
            .then(token(Type.SEMICOLON, tok -> Collections.<ScriptNode>emptyList()).or(block), (ev, children) -> {
                ev.setChildren(children);
                return ev;
            });

        Parser<ScriptToken, ScriptToken> assignOp = token(Type.ASSIGN)
            .or(token(Type.ADD_ASSIGN)).or(token(Type.SUB_ASSIGN))
            .or(token(Type.MUL_ASSIGN)).or(token(Type.DIV_ASSIGN))
            .or(token(Type.MOD_ASSIGN)).or(token(Type.AND_ASSIGN))
            .or(token(Type.OR_ASSIGN)).or(token(Type.XOR_ASSIGN));

        Parser<ScriptToken, ScriptAssign> assign = exprVariable
            .then(assignOp, expression, ScriptAssign::new)
            .thenIgnore(token(Type.SEMICOLON));

        Parser<ScriptToken, ScriptNode> node = Parser.lookAhead(
                Type.IDENTIFIER.matcher(),
                Type.L_PAREN.matcher().or(Type.IDENTIFIER.matcher()).or(Type.SEMICOLON.matcher()).or(Type.L_BRACE.matcher())
            ).ignoreThen(Parser.cast(event, ScriptNode.class))
            .or(token(Type.L_PAREN)
            	.ignoreThen(expression)
            	.thenIgnore(token(Type.R_PAREN))
            	.thenIgnore(token(Type.SEMICOLON))
            ).or(assign);

        block.set(token(Type.L_BRACE)
            .ignoreThen(node.zeroOrMore())
            .thenIgnore(token(Type.R_BRACE)));

        INSTANCE = node.zeroOrMore().then(token(Type.EOF), (s, x) -> new Script(s));
        return INSTANCE;
    }

    private static ScriptConst makeExprConst(ScriptToken constTok, ScriptExpr expr) {
    	expr.visit(e -> {
    		if (e instanceof ScriptConst) {
    			throw Utils.sneakyThrow(new ScriptParseException(
    				ScriptParseException.Type.SEMANTIC,
    				"Constant expressions cannot be nested.",
    				((ScriptConst) e).getTok().getSpan()
    			));
    		}
    		return true;
    	});
    	return new ScriptConst(expr, constTok);
    }

    private static ScriptEvent makeEvent(ScriptToken name, List<Parameter<ScriptExpr>> params) {
        ScriptEvent ev = new ScriptEvent(name.toSpanned());
        for (Parameter<ScriptExpr> p : params) {
            Spanned<String> id = p.getName();
            if (ev.getParameter(id.get()) != null)
                throw Utils.sneakyThrow(getDuplicateParameterError(id));
            ev.setParameter(p);

        }
        return ev;
    }

    private static ScriptEvent addModifiersToEvent(ScriptEvent ev, List<Parameter<ScriptExpr>> modifiers) {
        for (Parameter<ScriptExpr> mod : modifiers) {
            Spanned<String> id = mod.getName();
            if (ev.getParameter(id.get()) != null)
                throw Utils.sneakyThrow(getDuplicateParameterError(id));

            try {
                ev.setModifier(mod);
            } catch (IllegalArgumentException ex) {
                throw Utils.sneakyThrow(new ScriptParseException(
                	ScriptParseException.Type.SEMANTIC,
                    ex.getMessage(),
                    id.getSpan()));
            }
        }
        return ev;
    }

    private static Parser<ScriptToken, ScriptExpr> parseExpression(Parser<ScriptToken, ScriptExpr> atom) {
        PrecedenceTable<Type, ScriptToken, ScriptExpr> table = new PrecedenceTable<>(
            Type.class,
            ScriptToken::getType,
            atom,
            (left, op, right) -> new ScriptBinOp(left, op, right),
            (op, expr) -> new ScriptUnaryOp(op, expr));

        table.addInfix(Type.MUL, 300);
        table.addInfix(Type.DIV, 300);
        table.addInfix(Type.MOD, 300);

        table.addInfix(Type.ADD, 200);
        table.addInfix(Type.SUB, 200);

        table.addInfix(Type.BITWISE_AND, 83);
        table.addInfix(Type.BITWISE_XOR, 82);
        table.addInfix(Type.BITWISE_OR, 81);

        table.addInfix(Type.GREATER_THAN, 100);
        table.addInfix(Type.LESSER_THAN, 100);
        table.addInfix(Type.GREATER_THAN_EQUALS, 100);
        table.addInfix(Type.LESSER_THAN_EQUALS, 100);
        table.addInfix(Type.EQUALS, 90);
        table.addInfix(Type.NOT_EQUALS, 90);

        table.addInfix(Type.LOGICAL_AND, 70);
        table.addInfix(Type.LOGICAL_OR, 60);

        table.addPrefix(Type.LOGICAL_NOT);
        table.addPrefix(Type.BITWISE_NOT);
        table.addPrefix(Type.SUB, (sub, expr) -> {
            if(expr instanceof ScriptValue) {
                AbstractData val = negateValue(((ScriptValue) expr).get());
                if(val != null)
                    return new ScriptValue(val);
            }

            return new ScriptUnaryOp(sub, expr);
        });

        return table;

    }

    private static AbstractData negateValue(AbstractData v) {
        if(v instanceof DataDouble)
            return DataDouble.of(-((DataDouble) v).get());

        if(v instanceof DataInt) {
            int i = ((DataInt) v).get();
            return i == Integer.MIN_VALUE ? DataDouble.of(-(double) i) : DataInt.of(-i);
        }
        return null;
    }

    private static Parser<ScriptToken, ScriptToken> token(Type tokenType) {
        return Parser.of(tokenType.matcher());
    }

    private static<T> Parser<ScriptToken, T> token(Type tokenType, Function<ScriptToken, T> mapper) {
        return Parser.of(tokenType.matcher(), mapper);
    }

    private static ScriptParseException getDuplicateParameterError(Spanned<String> id) {
        return new ScriptParseException(
        	ScriptParseException.Type.SEMANTIC,
            "The parameter '" + id.get() + "' already exists.",
            id.getSpan());
    }

}
