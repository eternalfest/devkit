package net.eternalfest.hfestlib.script;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class ScriptToken {

    private Type type;
    private Span span;
    private String value;

    public ScriptToken(Type type, Span span) {
        this.type = type;
        this.span = span;
    }

    public Type getType() {
        return type;
    }

    public String getTypeName() {
        return type.toString();
    }

    public Span getSpan() {
        return span;
    }

    public String getValue() {
        if (value != null)
            return value;

        value = span.getLineString().substring(span.getStart(), span.getEnd());
        return value;
    }

    public Spanned<String> toSpanned() {
        return new Spanned<>(getValue(), span);
    }

    @Override
    public String toString() {
        return "[" + span.getLine() + ":" + (span.getStart() + 1) + "-" + (span.getEnd() + 1) + ", " + type.toString() + "('" + getValue() + "')]";
    }

    public static enum Type {
        NUMBER_LIT(Pattern.compile("[0-9]*\\.?[0-9]+" + "(?:e[+-]?[0-9]+)?" + "\\b")),
        STRING_LIT(Pattern.compile("(?:\"[^\"]*\")|(?:'[^']*')")), //Accept single- and double-quoted strings
        BOOL_LIT_TRUE(Pattern.compile("true\\b")),
        BOOL_LIT_FALSE(Pattern.compile("false\\b")),
        IDENTIFIER(Pattern.compile("[A-Za-z_][A-Za-z0-9_]*")),

        L_PAREN("("),
        R_PAREN(")"),
        L_BRACKET("["),
        R_BRACKET("]"),
        L_BRACE("{"),
        R_BRACE("}"),

        COMMA(","),
        SEMICOLON(";"),
        DOT("."),
        HASH_L_PAREN("#("),
        HASH("#"),

        ADD_ASSIGN("+="),
        SUB_ASSIGN("-="),
        DIV_ASSIGN("/="),
        MUL_ASSIGN("*="),
        MOD_ASSIGN("%="),
        OR_ASSIGN("|="),
        AND_ASSIGN("&="),
        XOR_ASSIGN("^="),

        EQUALS("=="),
        NOT_EQUALS("!="),
        GREATER_THAN_EQUALS(">="),
        LESSER_THAN_EQUALS("<="),
        GREATER_THAN(">"),
        LESSER_THAN("<"),
        LOGICAL_OR("||"),
        LOGICAL_AND("&&"),
        LOGICAL_NOT("!"),

        ADD("+"),
        SUB("-"),
        MUL("*"),
        DIV("/"),
        MOD("%"),

        BITWISE_OR("|"),
        BITWISE_AND("&"),
        BITWISE_XOR("^"),
        BITWISE_NOT("~"),

        ASSIGN("="),

        EOF(Pattern.compile("$a")); //Unmatchable regex for end of file


        private Pattern pattern;

        private Type(Pattern regex) {
            pattern = regex;
        }

        private Type(String str) {
            this(Pattern.compile(Pattern.quote(str)));
        }

        public Pattern getPattern() {
            return pattern;
        }

        public Predicate<ScriptToken> matcher() {
            return tok -> tok.getType() == this;
        }
    }

    public static class Span {
        private String lineString;
        private int line;
        private int start, end;

        public Span(String lineString, int line, int start, int end) {
            this.lineString = lineString;
            this.line = line;
            this.start = start;
            this.end = end;
        }

        public String getLineString() {
            return lineString;
        }

        public int getLine() {
            return line;
        }

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }
    }

    public static class Spanned<T> {
        private Span span;
        private T data;

        public Spanned(T data, Span span) {
            this.span = span;
            this.data = data;
        }

        public Span getSpan() {
            return span;
        }

        public T get() {
            return data;
        }

        public<U> Spanned<U> map(Function<T, U> mapper) {
            return new Spanned<>(mapper.apply(data), span);
        }
    }
}
