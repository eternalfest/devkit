package net.eternalfest.hfestlib.script;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import net.eternalfest.hfestlib.data.DataBool;
import net.eternalfest.hfestlib.script.ScriptToken.Spanned;

public class ScriptEvent extends ScriptNode {
    public static final Set<String> FORBIDDEN_MODIFIER_NAMES =
        Collections.unmodifiableSet(new HashSet<>(Arrays.asList(
            "name",
            "params",
            "children"
        )));

    Spanned<String> name;
    Map<String, Parameter<ScriptExpr>> parameters = new HashMap<>();
    ScriptExpr paramExpr = null;
    Map<String, Parameter<ScriptExpr>> modifiers = new HashMap<>();
    List<ScriptNode> children = new ArrayList<>();

    public ScriptEvent(Spanned<String> name) {
        this.name = name;
    }

    public ScriptEvent(Spanned<String> name, ScriptExpr expr) {
        this.name = name;
        this.paramExpr = expr;
    }

    public ScriptToken.Span getSpan() {
        return name.getSpan();
    }

    public Parameter<ScriptExpr> getParameter(String name) {
        return parameters.get(name);
    }

    public Parameter<ScriptExpr> getModifier(String name) {
        return modifiers.get(name);
    }

    public Map<String, Parameter<ScriptExpr>> getParameters() {
        return Collections.unmodifiableMap(parameters);
    }

    public Map<String, Parameter<ScriptExpr>> getModifiers() {
        return Collections.unmodifiableMap(modifiers);
    }

    public Parameter<ScriptExpr> setParameter(Parameter<ScriptExpr> param) {
        if(paramExpr != null)
            throw new IllegalArgumentException("Can't add named parameter: expr parameter already exists!");
        String name = param.getName().get();
        if (param.getValue() == null)
            return parameters.remove(name);
        return parameters.put(name, param);
    }

    public Parameter<ScriptExpr> setParameter(String name, ScriptExpr value) {
        if (value == null)
            return parameters.remove(name);

        return setParameter(new Parameter<>(new Spanned<>(name, null), value));
    }

    public Parameter<ScriptExpr> setModifier(Parameter<ScriptExpr> param) {
        String name = param.getName().get();
        if (FORBIDDEN_MODIFIER_NAMES.contains(name))
            throw new IllegalArgumentException("There cannot be a modifier called '" + name + "'");
        ScriptExpr expr = param.getValue();
        if(!(expr instanceof ScriptValue || expr instanceof ScriptConst)) {
        	throw new IllegalArgumentException("Node modifiers must be literals or constant expressions.");
        }

        if (param.getValue() == null)
            return modifiers.remove(name);
        return modifiers.put(name, param);
    }

    public Parameter<ScriptExpr> setModifier(String name, ScriptValue value) {
        if (value == null)
            return modifiers.remove(name);
        return setModifier(new Parameter<>(new Spanned<>(name, null), value));
    }

    public List<ScriptNode> getChildren() {
        return children;
    }

    public void setChildren(List<ScriptNode> children) {
        this.children = children;
    }

    @Override
    public List<String> toCodeLines() {
        StringBuilder builder = new StringBuilder();
        builder.append(name.get());

        if(paramExpr != null) {
            builder.append('(');
            builder.append(paramExpr.toCode());
            builder.append(')');
        } else {
            builder.append(parameters.entrySet().stream()
                .map(e -> e.getValue().toCode())
                .collect(Collectors.joining(", ", "(", ")")));
        }

        if (!modifiers.isEmpty())
            builder.append(modifiers.entrySet().stream()
                .map(e -> e.getValue().toCode())
                .collect(Collectors.joining(" ", " ", "")));


        if (children.isEmpty()) {
            builder.append(';');
            return Collections.singletonList(builder.toString());
        } else {
            builder.append(" {");
            List<String> lines = children.stream()
                .map(c -> c.toCodeLines().stream())
                .flatMap(Function.identity())
                .map(l -> "\t" + l)
                .collect(Collectors.toCollection(() -> {
                    List<String> l = new ArrayList<>();
                    l.add(builder.toString());
                    return l;
                }));
            lines.add("}");
            return lines;
        }

    }

    public static final class Parameter<T extends ScriptExpr> {
        private Spanned<String> name;
        private T value;

        public Parameter(Spanned<String> name, T value) {
            this.name = name;
            this.value = value;
        }

        public Spanned<String> getName() {
            return name;
        }

        public T getValue() {
            return value;
        }

        public String toCode() {
            if (value instanceof ScriptValue) {
            	ScriptValue v = (ScriptValue) value;
            	if (v.get() == DataBool.TRUE) {
            		return name.get();
            	}
            }
            return name.get() + "=" + value.toCode();
        }

        public<U extends ScriptExpr> Parameter<U> map(Function<T, U> mapper) {
            return new Parameter<>(name, mapper.apply(value));
        }

    }


}
