package net.eternalfest.hfestlib.script;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Matcher;

import net.eternalfest.hfestlib.utils.Utils;

public class ScriptLexer implements AutoCloseable {

    private static final ScriptParseException.Type ERROR_TYPE = ScriptParseException.Type.LEXER;

    BufferedReader reader;

    int curLine = 0; //line of character to be read
    int curCol = 1; //col of character to be read
    String line = "";
    Matcher[] matchers;

    public ScriptLexer(Reader reader) {
        if (reader instanceof BufferedReader)
            this.reader = (BufferedReader) reader;
        else this.reader = new BufferedReader(reader);

        matchers = Arrays.stream(ScriptToken.Type.values())
            .map(type -> type.getPattern().matcher(""))
            .toArray(Matcher[]::new);
    }

    private static ScriptToken.Span posAsSpan(String lineStr, int line, int col) {
        return new ScriptToken.Span(lineStr, line, col, col + 1);
    }

    public ScriptToken nextToken() throws ScriptParseException {
        skipWhitespaceAndComments();

        if (line == null)
            return new ScriptToken(ScriptToken.Type.EOF, new ScriptToken.Span("", curLine + 1, 0, 0));

        for (int i = 0; i < matchers.length; i++) {
            Matcher matcher = matchers[i];
            if (matcher.region(curCol, line.length()).lookingAt()) {
                int start = curCol;
                curCol = matcher.end();
                return new ScriptToken(ScriptToken.Type.values()[i], new ScriptToken.Span(line, curLine, start, curCol));
            }
        }

        throw new ScriptParseException(ERROR_TYPE, "Unexpected character", posAsSpan(line, curLine, curCol));

    }

    private void readNextLine() throws ScriptParseException {
        curLine++;
        curCol = 0;
        try {
            line = reader.readLine();
        } catch (IOException e) {
            throw new ScriptParseException(ERROR_TYPE, "IO exception while reading script", e);
        }

        if (line != null) {
            for (Matcher m : matchers)
                m.reset(line);
        }
    }

    private boolean skipUntil(Predicate<Character> predicate) throws ScriptParseException {
        while (line != null) {
            while (curCol < line.length()) {
                char c = line.charAt(curCol);
                if (predicate.test(c)) {
                    return true;
                }

                curCol++;
            }

            readNextLine();
        }

        return false;
    }

    private boolean skipWhile(Predicate<Character> predicate) throws ScriptParseException {
        return skipUntil(predicate.negate());
    }

    private void skipWhitespaceAndComments() throws ScriptParseException {

        while (line != null) {
            if (!skipWhile(Character::isWhitespace))
                return;

            if (line.charAt(curCol) != '/' || line.length() <= curCol + 1)
                return;

            switch (line.charAt(curCol + 1)) {
                case '/': //Single line comment
                    readNextLine();
                    break;

                case '*': //Multiline comment
                    skipMultilineComment();
                    break;

                default:
                    return;
            }
        }
    }

    private void skipMultilineComment() throws ScriptParseException {
        int lineStart = curLine, colStart = curCol;
        String oldLine = line;

        curCol += 2; //skipping "/*"

        try {
            while (skipUntil(c -> c == '*')) {
                curCol++;
                if (line.length() > curCol && line.charAt(curCol) == '/') {
                    curCol++;
                    return;
                }
            }
        } catch (ScriptParseException e) {
            if (e.getCause() == null)
                throw e;
        }

        throw new ScriptParseException(ERROR_TYPE, "Unterminated comment", posAsSpan(oldLine, lineStart, colStart));
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }

    public Supplier<ScriptToken> asSupplier() {
        return () -> {
            try {
                return this.nextToken();
            } catch (ScriptParseException e) {
                throw Utils.sneakyThrow(e);
            }
        };
    }
}
