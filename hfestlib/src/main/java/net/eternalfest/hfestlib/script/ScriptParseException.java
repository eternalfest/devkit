package net.eternalfest.hfestlib.script;

import java.text.BreakIterator;

import net.eternalfest.hfestlib.tools.ShowableException;

public class ScriptParseException extends ShowableException {

    private static final long serialVersionUID = -3944328193104802300L;
    private String msg;
    private Type type;
    private ScriptToken.Span pos;

    public ScriptParseException(Type type, String msg, ScriptToken.Span pos, Throwable cause) {
        super(cause);
        this.msg = msg;
        this.type = type;
        this.pos = pos;
    }

    public ScriptParseException(Type type, String msg, ScriptToken.Span pos) {
        this(type, msg, pos, null);
    }

    public ScriptParseException(Type type, String msg, Throwable cause) {
        this(type, msg, null, cause);
    }

    public ScriptToken.Span getPosition() {
        return pos;
    }

    @Override
    public String getMessage() {
        if (pos == null)
            return type + " error: " + msg;

        StringBuilder builder = new StringBuilder(type + " error at line " + pos.getLine() + ", character " + (pos.getStart() + 1) + ": ");
        builder.append(msg);
        builder.append("\n");
        builder.append(pos.getLineString().replaceAll("\t", " "));
        builder.append("\n");

        String line = pos.getLineString();
        if (!line.isEmpty()) {
            BreakIterator charIterator = BreakIterator.getCharacterInstance();
            charIterator.setText(line);

            while (charIterator.next() <= pos.getStart())
                builder.append(" ");

            builder.append("^");
        }

        return builder.toString();
    }

    @Override
    public String getFullMessage() {
        return getFullMessageWithHeader("Erreur dans le script du niveau :");
    }

    public String getFullMessageWithHeader(String headerMsg) {
    	return "<html>" + headerMsg + "<br/><pre>"
                + getMessage().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br/>")
                + "</pre></html>";
    }

    public static enum Type {
        LEXER, SYNTAX, SEMANTIC;

        @Override
        public String toString() {
            String name = this.name();
            String str = name.toLowerCase().replaceFirst("(.)", name.toUpperCase().substring(0, 1));
            return str;
        }
    }
}
