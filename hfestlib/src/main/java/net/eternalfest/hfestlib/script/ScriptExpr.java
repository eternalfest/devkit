package net.eternalfest.hfestlib.script;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public abstract class ScriptExpr extends ScriptNode {
	public abstract boolean visit(Predicate<ScriptExpr> visitor);

    @Override
	public final List<String> toCodeLines() {
    	return Collections.singletonList("(" + this.toCode() + ");");
    }
}
