package net.eternalfest.hfestlib.script;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.Function;

import net.eternalfest.hfestlib.parser.Parser;
import net.eternalfest.hfestlib.parser.ParserCursor;
import net.eternalfest.hfestlib.utils.Result;
import net.eternalfest.hfestlib.utils.TriFunction;

public class PrecedenceTable<E extends Enum<E>, T, R> implements Parser<T, R> {

    private Parser<T, R> atom;
    private Function<T, E> getType;
    private int[] infix;
    private boolean[] prefix;
    private TriFunction<R, T, R, R>[] infixCombiners;
    private BiFunction<T, R, R>[] prefixCombiners;

    @SuppressWarnings("unchecked")
    public PrecedenceTable(
            Class<E> tokTypes,
            Function<T, E> getType,
            Parser<T, R> atom,
            TriFunction<R, T, R, R> infix,
            BiFunction<T, R, R> prefix) {
        this.atom = atom;
        int len = tokTypes.getEnumConstants().length;
        this.getType = getType;
        this.infix = new int[len];
        this.prefix = new boolean[len];
        infixCombiners = (TriFunction<R, T, R, R>[]) new TriFunction<?, ?, ?, ?>[len];
        prefixCombiners = (BiFunction<T, R, R>[]) new BiFunction<?, ?, ?>[len];
        Arrays.fill(this.infix, -1);
        Arrays.fill(infixCombiners, infix);
        Arrays.fill(prefixCombiners, prefix);
    }

    public PrecedenceTable<E, T, R> addInfix(E tok, int precedence, TriFunction<R, T, R, R> combiner) {
        if(precedence < 0)
            throw new IllegalArgumentException("precedence must be >= 0");
        int i = tok.ordinal();
        infix[i] = precedence;
        if(combiner != null)
            infixCombiners[i] = combiner;
        return this;
    }

    public PrecedenceTable<E, T, R> addInfix(E tok, int precedence) {
        return addInfix(tok, precedence, null);
    }

    public PrecedenceTable<E, T, R> addPrefix(E tok, BiFunction<T, R, R> combiner) {
        int i = tok.ordinal();
        prefix[i] = true;
        if(combiner != null)
            prefixCombiners[i] = combiner;
        return this;
    }

    public PrecedenceTable<E, T, R> addPrefix(E tok) {
        return addPrefix(tok, null);
    }

    public boolean isPrefix(E tok) {
        return prefix[tok.ordinal()];
    }

    public boolean isInfix(E tok) {
        return infix[tok.ordinal()] >= 0;
    }

    public int getInfixPrecedence(E tok) {
        return infix[tok.ordinal()];
    }

    @Override
    public Result<R, Error<T>> parse(ParserCursor<T> cursor) {
        return parseExpr(cursor, 0);
    }

    private Result<R, Error<T>> parseExpr(ParserCursor<T> cursor, int minPrec) {
        // precedence climbing
        Result<R, Error<T>> result = parsePrefix(cursor);

        while(result.isSuccess()) {
            int opType = getType.apply(cursor.peek()).ordinal();
            int curPrec = infix[opType];
            if(curPrec < minPrec)
                break;

            R lhs = result.get();
            T op = cursor.next();
            result = parseExpr(cursor, curPrec+1) // left-associativity
                .map(rhs -> infixCombiners[opType].apply(lhs, op, rhs));
        }

        return result;
    }

    private Result<R, Error<T>> parsePrefix(ParserCursor<T> cursor) {
        int opType = getType.apply(cursor.peek()).ordinal();
        if(prefix[opType]) {
            T op = cursor.next();
            return parsePrefix(cursor).map(expr -> prefixCombiners[opType].apply(op, expr));
        }
        return atom.parse(cursor);
    }
}
