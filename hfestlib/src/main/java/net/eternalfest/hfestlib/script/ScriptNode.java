package net.eternalfest.hfestlib.script;

import java.util.List;
import java.util.stream.Collectors;

public abstract class ScriptNode {

    public abstract List<String> toCodeLines();

    public String toCode() {
        return toCodeLines().stream().collect(Collectors.joining("\n"));
    }

}
