package net.eternalfest.hfestlib.script;

import java.util.function.Predicate;

public class ScriptUnaryOp extends ScriptExpr {

    private ScriptToken op;
    private ScriptExpr expr;

    public ScriptUnaryOp(ScriptToken op, ScriptExpr expr) {
        this.op = op;
        this.expr = expr;
    }

    public ScriptExpr getExpr() {
        return expr;
    }

    public ScriptToken getOp() {
        return op;
    }

    @Override
    public boolean visit(Predicate<ScriptExpr> visitor) {
    	return visitor.test(this) && expr.visit(visitor);
    }

    @Override
    public String toCode() {
        if(expr instanceof ScriptValue || expr instanceof ScriptVariable)
            return op.getValue() + expr.toCode();
        return op.getValue() + "(" + expr.toCode() + ")";
    }
}
