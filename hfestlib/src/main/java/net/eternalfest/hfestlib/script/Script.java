package net.eternalfest.hfestlib.script;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Script {

    private static final double VERSION = 2.00;

    private List<ScriptNode> nodes;

    public Script(List<ScriptNode> nodes) {
        this.nodes = nodes;
    }

    public List<ScriptNode> getNodes() {
        return nodes;
    }

    public List<String> toCodeLines() {
        return nodes.stream()
            .map(e -> e.toCodeLines().stream())
            .flatMap(Function.identity())
            .collect(Collectors.toList());
    }

    public String toCode() {
        return nodes.stream()
            .map(e -> e.toCodeLines().stream())
            .flatMap(Function.identity())
            .collect(Collectors.joining("\n"));
    }
}
