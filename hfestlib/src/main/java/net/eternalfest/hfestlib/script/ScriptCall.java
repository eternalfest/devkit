package net.eternalfest.hfestlib.script;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ScriptCall extends ScriptExpr {

    private List<ScriptExpr> args;
    private String fn;


    public ScriptCall(String fn, List<ScriptExpr> args) {
        if(fn == null || fn.isEmpty())
            throw new IllegalArgumentException("'fn' must not be empty!");
        this.fn = fn;
        this.args = args;
    }

    public String getFunction() {
        return fn;
    }

    public List<ScriptExpr> getArguments() {
        return Collections.unmodifiableList(args);
    }

    @Override
    public boolean visit(Predicate<ScriptExpr> visitor) {
    	if(!visitor.test(this))
    		return false;

    	for (ScriptExpr arg: args) {
    		if(!arg.visit(visitor))
    			return false;
    	}

    	return true;
    }

    @Override
    public String toCode() {
        return args.stream()
            .map(ScriptExpr::toCode)
            .collect(Collectors.joining(", ", fn + "(", ")"));
    }
}
