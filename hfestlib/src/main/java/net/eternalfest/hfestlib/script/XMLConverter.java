package net.eternalfest.hfestlib.script;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.stream.Collectors;

import org.jdom2.Attribute;
import org.jdom2.DataConversionException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import net.eternalfest.hfestlib.data.AbstractData;
import net.eternalfest.hfestlib.data.DataBool;
import net.eternalfest.hfestlib.data.DataDouble;
import net.eternalfest.hfestlib.data.DataInt;
import net.eternalfest.hfestlib.data.DataString;
import net.eternalfest.hfestlib.script.ScriptToken.Spanned;
import net.eternalfest.hfestlib.tools.XMLMalformedException;

public class XMLConverter {

    private static char REPLACEMENT_CHAR = 'π';

    public static Script convert(String str) throws XMLMalformedException {
        str = "<root>" + str.replace('$', REPLACEMENT_CHAR) + "</root>";

        try {
            SAXBuilder builder = new SAXBuilder();
            return convert(builder.build(new StringReader(str)));

        } catch (JDOMException e) {
            throw new XMLMalformedException("script", e);
        } catch (IOException e) {
            throw new RuntimeException("This shouldn't happen", e);
        }
    }

    public static Script convert(Document doc) {
        Element root = doc.getRootElement();

        List<ScriptNode> nodes = convertChildren(root);
        for (ScriptNode n : nodes) {
            if(n instanceof ScriptEvent)
             ((ScriptEvent) n).setModifier("detach", null);
        }

        return new Script(nodes);
    }

    private static List<ScriptNode> convertChildren(Element root) {
        return root.getChildren().stream()
            .map(c -> {
                ScriptEvent e = convertNode(c);
                if (!e.getChildren().isEmpty())
                    e.setModifier("detach", new ScriptValue(DataBool.TRUE));
                return e;

            }).collect(Collectors.toList());
    }

    private static ScriptEvent convertNode(Element el) {
        ScriptEvent node = new ScriptEvent(new Spanned<>(convertName(el.getName()), null));

        for (Attribute att : el.getAttributes()) {
            AbstractData value = null;

            try {
                double d = att.getDoubleValue();
                if (d == (int) d)
                    value = DataInt.of((int) d);
                else value = DataDouble.of(d);
            } catch (DataConversionException e) {
                value = DataString.of(att.getValue().replace(REPLACEMENT_CHAR, '$'));
            }

            if (att.getName().equals(REPLACEMENT_CHAR + "repeat")) {
                if (value.asInt().orElse(0).equals(-1))
                    value = DataBool.TRUE;

                node.setModifier(convertName(att.getName()), new ScriptValue(value));
            } else if (att.getName().equals(REPLACEMENT_CHAR + "key")) {
                node.setModifier(convertName(att.getName()), new ScriptValue(value));
            } else {
                node.setParameter(convertName(att.getName()), new ScriptValue(value));
            }
        }

        node.setChildren(convertChildren(el));

        return node;
    }

    private static String convertName(String name) {
        if (name.length() > 0 && name.charAt(0) == REPLACEMENT_CHAR)
            return name.substring(1);
        return name;
    }
}
