package net.eternalfest.hfestlib.script;

import java.util.function.Predicate;

public class ScriptConst extends ScriptExpr {

	private ScriptToken constTok;
	private ScriptExpr expr;

	public ScriptConst(ScriptExpr expr, ScriptToken constTok) {
		this.expr = expr;
		this.constTok = constTok;
	}

    public ScriptExpr getExpr() {
        return expr;
    }
    public ScriptToken getTok() {
    	return constTok;
    }

    @Override
    public boolean visit(Predicate<ScriptExpr> visitor) {
    	return visitor.test(this) && expr.visit(visitor);
    }

	@Override
	public String toCode() {
		if (constTok.getType() == ScriptToken.Type.HASH_L_PAREN) {
			return "#(" + expr.toCode() + ")";
		} else {
			return "#" + expr.toCode();
		}
	}
}
