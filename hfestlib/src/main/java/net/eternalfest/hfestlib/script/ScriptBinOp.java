package net.eternalfest.hfestlib.script;

import java.util.function.Predicate;

public class ScriptBinOp extends ScriptExpr {

    private ScriptExpr left, right;
    private ScriptToken op;

    public ScriptBinOp(ScriptExpr left, ScriptToken op, ScriptExpr right) {
        this.left = left;
        this.right = right;
        this.op = op;
    }

    public ScriptExpr getLeft() {
        return left;
    }

    public ScriptExpr getRight() {
        return right;
    }

    public ScriptToken getOp() {
        return op;
    }

    @Override
    public boolean visit(Predicate<ScriptExpr> visitor) {
    	return visitor.test(this) && left.visit(visitor) && right.visit(visitor);
    }

    @Override
    public String toCode() {
        StringBuilder str = new StringBuilder();
        if(left instanceof ScriptBinOp) {
            str.append('(');
            str.append(left.toCode());
            str.append(')');
        } else str.append(left.toCode());
        str.append(' ');
        str.append(op.getValue());
        str.append(' ');
        if(right instanceof ScriptBinOp) {
            str.append('(');
            str.append(right.toCode());
            str.append(')');
        } else str.append(right.toCode());
        return str.toString();
    }
}
