package net.eternalfest.hfestlib.script;

import java.util.function.Predicate;

import net.eternalfest.hfestlib.data.AbstractData;
import net.eternalfest.hfestlib.data.DataBool;
import net.eternalfest.hfestlib.data.DataDouble;
import net.eternalfest.hfestlib.data.DataInt;
import net.eternalfest.hfestlib.data.DataString;

public class ScriptValue extends ScriptExpr {
    private AbstractData value;

    public ScriptValue(AbstractData value) {
        this.value = value;
    }

    public static ScriptValue of(ScriptToken tok) {
        AbstractData data;

        switch (tok.getType()) {
            case BOOL_LIT_FALSE:
                data = DataBool.FALSE;
                break;
            case BOOL_LIT_TRUE:
                data = DataBool.TRUE;
                break;
            case NUMBER_LIT:
                double d = Double.parseDouble(tok.getValue());
                if (d == (int) d)
                    data = DataInt.of((int) d);
                else data = DataDouble.of(d);
                break;
            case STRING_LIT:
                data = DataString.of(unescape(tok.getValue()));
                break;

            default:
                throw new IllegalArgumentException("Illegal token type " + tok.getType());
        }

        return new ScriptValue(data);
    }

    public AbstractData get() {
    	return this.value;
    }

    private static String unescape(String str) {
        if (str.length() < 2)
            return "";

        StringBuilder builder = new StringBuilder(str.length());
        boolean backslash = false;

        for (int i = 1, l = str.length() - 1; i < l; i++) {
            char c = str.charAt(i);
            if (backslash) {
                switch (c) {
                    case 'b':
                        c = '\b';
                        break;

                    case 't':
                        c = '\t';
                        break;

                    case 'f':
                        c = '\f';
                        break;

                    case 'n':
                        c = '\n';
                        break;

                    case 'r':
                        c = '\r';
                        break;
                }

                builder.append(c);
                backslash = false;

            } else {
                if (c == '\\')
                    backslash = true;
                else builder.append(c);
            }
        }

        return builder.toString();

    }

    @Override
    public boolean visit(Predicate<ScriptExpr> visitor) {
    	return visitor.test(this);
    }

    @Override
    public String toCode() {
        return value.toString();
    }
}
