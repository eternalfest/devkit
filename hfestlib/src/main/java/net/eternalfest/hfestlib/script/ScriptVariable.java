package net.eternalfest.hfestlib.script;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

public class ScriptVariable extends ScriptExpr {

    public static class Access {
        private String ident;
        private ScriptExpr expr;

        public Access(String ident) {
            this.ident = Objects.requireNonNull(ident);
        }

        public Access(ScriptExpr expr) {
            this.expr = Objects.requireNonNull(expr);
        }

        public String getIdent() {
            return ident;
        }

        public ScriptExpr getExpr() {
            return expr;
        }
    }

    private String name;
    private List<Access> parts = new ArrayList<>();

    public ScriptVariable(String name, List<Access> parts) {
        this.name = name;
        this.parts = parts;
    }

    public String getName() {
        return name;
    }

    public int getPartsCount() {
        return parts.size() + 1;
    }

    public Access getPart(int i) {
        if(i == 0) {
            return new Access(name);
        } else {
            return parts.get(i - 1);
        }
    }

    @Override
    public boolean visit(Predicate<ScriptExpr> visitor) {
    	if(!visitor.test(this))
    		return false;

    	for (Access part: parts) {
    		if (part.getExpr() == null || !part.getExpr().visit(visitor))
    			return false;
    	}

    	return true;
    }

    @Override
    public String toCode() {
        StringBuilder str = new StringBuilder(name);
        for(Access part: parts) {
            if(part.expr != null) {
                str.append('[');
                str.append(part.expr.toCode());
                str.append(']');
            } else {
                str.append('.');
                str.append(part.ident);
            }
        }
        return str.toString();
    }
}
