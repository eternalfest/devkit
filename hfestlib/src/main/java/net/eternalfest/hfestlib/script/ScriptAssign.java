package net.eternalfest.hfestlib.script;

import java.util.Collections;
import java.util.List;

public class ScriptAssign extends ScriptNode {

    private ScriptVariable lvalue;
    private ScriptToken assign;
    private ScriptExpr rvalue;

    public ScriptAssign(ScriptVariable lvalue, ScriptToken assign, ScriptExpr rvalue) {
        this.lvalue = lvalue;
        this.assign = assign;
        this.rvalue = rvalue;
    }

    public ScriptVariable getLValue() {
        return lvalue;
    }

    public ScriptToken getAssignOp() {
        return assign;
    }

    public ScriptExpr getRValue() {
        return rvalue;
    }

    @Override
    public List<String> toCodeLines() {
        return Collections.singletonList(lvalue.toCode() + " " + assign.getValue() + " " + rvalue.toCode() + ";");
    }
}
