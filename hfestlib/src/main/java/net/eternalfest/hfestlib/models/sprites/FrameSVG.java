package net.eternalfest.hfestlib.models.sprites;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.function.Supplier;

import com.kitfox.svg.SVGDiagram;
import com.kitfox.svg.SVGException;
import net.eternalfest.hfestlib.tools.ShowableException;
import net.eternalfest.hfestlib.utils.Lazy;

public class FrameSVG implements IAssetFrame {

    private static final Graphics2D FAKE_G2D = (Graphics2D) new BufferedImage(8, 8, BufferedImage.TYPE_INT_ARGB).getGraphics();

    private final String name;
    private final String ID;
    private Supplier<SVGDiagram> image;
    private int width;
    private int height;
    private int offsetX;
    private int offsetY;

    public FrameSVG(String name, String ID, Supplier<SVGDiagram> image, int offsetX, int offsetY) {
        this.name = IAsset.validateString(name);
        this.ID = IAsset.validateString(ID);
        this.image = image;
        this.width = -1;
        this.height = -1;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getID() {
        return ID;
    }

    private void computeDimensions() {
        if(width >= 0)
            return;

        SVGDiagram img = image.get();
        if(img != null) {
            Rectangle2D bbox = img.getViewRect();
            width = (int) bbox.getWidth();
            height = (int) bbox.getHeight();
            // check if the image can render correctly
            if(!tryRender(FAKE_G2D, img, 0, 0)) {
                width = -1;
                height = -1;
            }
        }
        if(width < 0) {
            IAssetFrame def = DefaultFrame.DEFAULT;
            width = def.getWidth();
            height = def.getHeight();
            offsetX = def.getOffsetX();
            offsetY = def.getOffsetY();
        }
    }

    @Override
    public int getWidth() {
        computeDimensions();
        return width;
    }

    @Override
    public int getHeight() {
        computeDimensions();
        return height;
    }

    @Override
    public int getOffsetX() {
        computeDimensions();
        return offsetX;
    }

    @Override
    public int getOffsetY() {
        computeDimensions();
        return offsetY;
    }

    private boolean tryRender(Graphics2D g, SVGDiagram img, int offX, int offY) {
        Shape clip = g.getClip();
        Object aliasHint = g.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
        AffineTransform saveAT = g.getTransform();

        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.translate(-offX, -offY);
        g.setClip(0, 0, width, height);

        try {
            img.render(g);
            return true;
        } catch(SVGException e) {
            image = Lazy.fromValue(null);
            ShowableException.getExceptionWithMessage(
                    "Erreur lors de l'affichage du SVG " + ID + ": " + e.getMessage()).show();
        } finally {
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, aliasHint);
            g.setTransform(saveAT);
            g.setClip(clip);
        }
        return false;
    }

    @Override
    public void draw(Graphics2D g, ImageObserver obs) {
        SVGDiagram img = image.get();
        if(img != null && tryRender(g, img, offsetX, offsetY))
            return;

        DefaultFrame.DEFAULT.draw(g, obs);
    }

    @Override
    public void drawThumbnail(Graphics2D g, int width, int height, ImageObserver obs) {
        SVGDiagram img = image.get();
        if(img == null) {
            DefaultFrame.DEFAULT.drawThumbnail(g, width, height, obs);
            return;
        }

        AffineTransform saveAT = g.getTransform();
        int imgWidth = getWidth(), imgHeight = getHeight();
        double scaleX = width / (double) imgWidth;
        double scaleY = height / (double) imgHeight;
        double scale = Math.min(Math.min(scaleX,  scaleY), 1.0);
        g.scale(scale, scale);
        boolean ok = tryRender(g, img, (width - (int) (imgWidth*scale))/2, (height - (int) (imgHeight*scale))/2);
        g.setTransform(saveAT);
        if(!ok)
            DefaultFrame.DEFAULT.drawThumbnail(g, width, height, obs);
    }
}
