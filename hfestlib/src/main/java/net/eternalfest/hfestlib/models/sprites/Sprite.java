package net.eternalfest.hfestlib.models.sprites;

import java.util.Collections;
import java.util.List;

import net.eternalfest.hfestlib.models.LevelTiles;

public class Sprite implements IAsset {

    public static final int ROTATION_UP = 0b00 << 2;
    public static final int ROTATION_RIGHT = 0b01 << 2;
    public static final int ROTATION_DOWN = 0b10 << 2;
    public static final int ROTATION_LEFT = 0b11 << 2;
    public static final int ROTATION_MASK = 0b11 << 2;
    public static final int ROTATION_SHIFT = 2;

    public static final int CAN_FLIP_H = 1 << 0;
    public static final int CAN_FLIP_V = 1 << 1;
    public static final int CAN_ROTATE = ROTATION_MASK;
    public static final int FLIP_H = CAN_FLIP_H;
    public static final int FLIP_V = CAN_FLIP_V;
    public static final int ORIENTATION_MASK = CAN_ROTATE | CAN_FLIP_H | CAN_FLIP_V;

    public static final int AUTO_ORIENT_NONE = 0b00 << 4;
    public static final int AUTO_ORIENT_ATTACH = 0b01 << 4;
    public static final int AUTO_ORIENT_ATTACH_FLIP = 0b10 << 4;
    public static final int AUTO_ORIENT_ATTACH_NOFLIP = 0b11 << 4;
    public static final int AUTO_ORIENT_MASK = 0b11 << 4;

    public static final int DEFAULT_LAYER_FRONT = 1 << 7;
    public static final int TYPE_FRUIT = 1 << 8;
    public static final int TYPE_FRUIT_ORDER = 1 << 9;

    public static final Sprite UNKNOWN = unknownBase("<unknown-sprite>", false);
    public static final Sprite UNKNOWN_FRUIT = unknownBase("<unknown-fruit>", true);

    private final String name;
    private final String ID;

    private final List<IAssetFrame> frames;
    private final IAssetFrame defaultFrame;

    private final int capabilities;


    public static Sprite unknownWith(String id) {
        return unknownBase(id, false);
    }

    public static Sprite unknownFruitWith(String id) {
        return unknownBase(id, true);
    }

    private static Sprite unknownBase(String id, boolean fruit) {
        int capability = fruit ? TYPE_FRUIT_ORDER : 0;
        String name = fruit ? "Fruit inconnu" : "Sprite inconnu";
        return new Sprite(name, id, Collections.singletonList(DefaultFrame.DEFAULT), capability);
    }

    public Sprite(String name, String ID, List<IAssetFrame> frames, int capabilities) {
        if(frames.isEmpty())
            throw new IllegalArgumentException("Sprite frame list can't be empty");
        this.frames = Collections.unmodifiableList(frames);
        this.defaultFrame = frames.get(0);

        this.name = IAsset.validateString(name);
        this.ID = IAsset.validateString(ID);

        this.capabilities = capabilities;
    }

    public Sprite(String name, String ID, List<IAssetFrame> images) {
        this(name, ID, images, 0);
    }

    public Sprite(String name, String ID, IAssetFrame image) {
        this(name, ID, Collections.singletonList(image));
    }

    public Sprite(String name, String ID, IAssetFrame image, int properties) {
        this(name, ID, Collections.singletonList(image), properties);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getID() {
        return ID;
    }

    public List<IAssetFrame> getFrames() {
        return frames;
    }

    public IAssetFrame getDefaultFrame() {
        return defaultFrame;
    }

    public int getCapabilities() {
        return capabilities;
    }

    public boolean hasCapability(int capability) {
        return (capabilities & capability) != 0;
    }

    public int cycleOrientation(int orientation) {
        int prop = orientation;
        // The modifiable properties
        int mask = this.capabilities & ORIENTATION_MASK;
        // Set the locked properties
        prop |= ~mask;
        // This allow us to cycle through the non-locked ones
        prop += 1;
        // Clear the locked properties
        prop &= mask;
        // Set the locked properties to their original values
        prop |= ~mask & orientation;
        return prop;
    }

    public int calculateOrientation(int orientation, LevelTiles tiles, int x, int y) {
        int auto = capabilities & AUTO_ORIENT_MASK;
        if(auto == 0)
            return orientation;

        if (y == tiles.getHeight() - 1 || tiles.isTileAt(x, y + 1))
            return (orientation & ~CAN_ROTATE) | ROTATION_UP;
        else if (y == 0 || tiles.isTileAt(x, y - 1))
            return (orientation & ~CAN_ROTATE) | ROTATION_DOWN;
        else if (x == 0 || tiles.isTileAt(x - 1, y))
            return (orientation & ~CAN_ROTATE) | ROTATION_RIGHT;
        else if (x == tiles.getWidth() - 1 || tiles.isTileAt(x + 1, y))
            return (orientation & ~CAN_ROTATE) | ROTATION_LEFT;

        return (orientation & ~CAN_ROTATE) | ROTATION_UP;
    }
}
