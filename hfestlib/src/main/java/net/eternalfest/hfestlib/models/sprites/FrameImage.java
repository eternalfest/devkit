package net.eternalfest.hfestlib.models.sprites;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.ImageObserver;
import java.util.function.Supplier;

public class FrameImage implements IAssetFrame {

    private final String name;
    private final String ID;
    private final int offsetX; //en pixels
    private final int offsetY; //en pixels
    private final Supplier<Image> image;

    public FrameImage(String name, String ID, Supplier<Image> image, int offX, int offY) {
        this.name = IAsset.validateString(name);
        this.ID = IAsset.validateString(ID);
        this.offsetX = offX;
        this.offsetY = offY;
        this.image = image;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public int getWidth() {
        Image img = image.get();
        return img == null ? DefaultFrame.DEFAULT.getWidth() : img.getWidth(null);
    }

    @Override
    public int getHeight() {
        Image img = image.get();
        return img == null ? DefaultFrame.DEFAULT.getHeight() : img.getHeight(null);
    }

    @Override
    public int getOffsetX() {
        return image.get() == null ? DefaultFrame.DEFAULT.getOffsetX() : offsetX;
    }

    @Override
    public int getOffsetY() {
        return image.get() == null ? DefaultFrame.DEFAULT.getOffsetY() : offsetY;
    }

    @Override
    public void draw(Graphics2D g, ImageObserver obs) {
        Image img = image.get();
        if(img == null)
            DefaultFrame.DEFAULT.draw(g, obs);
        else g.drawImage(img, -getOffsetX(), -getOffsetY(), obs);
    }

    @Override
    public void drawThumbnail(Graphics2D g, int width, int height, ImageObserver obs) {
        Image img = image.get();
        if(img == null) {
            DefaultFrame.DEFAULT.drawThumbnail(g, width, height, obs);
            return;
        }

        AffineTransform saveAT = g.getTransform();
        int imgWidth = img.getWidth(obs), imgHeight = img.getHeight(obs);
        double scaleX = width / (double) imgWidth;
        double scaleY = height / (double) imgHeight;
        double scale = Math.min(Math.min(scaleX,  scaleY), 1.0);
        g.scale(scale, scale);
        g.drawImage(img, (width - (int) (imgWidth*scale))/2, (height - (int) (imgHeight*scale))/2, obs);
        g.setTransform(saveAT);
    }
}
