package net.eternalfest.hfestlib.models.sprites;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.ImageObserver;

import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.utils.Lazy;

public class FieldType implements IAsset, IThumbnail {

    public static final FieldType DEFAULT = new FieldType(Lazy.fromValue(null), "none", "none");
    public static final FieldType UNKNOWN = unknownWith("<unknown-field>");

    public static FieldType unknownWith(String id) {
        return new FieldType(Lazy.fromValue(null), "Rayon inconnu", id);
    }

    public static final int TILES_SIZE = Level.TILES_SIZE;

    private final String name;
    private final String ID;
    protected final Lazy<Image> image;

    public FieldType(Lazy<Image> image, String name, String ID) {
        this.name = IAsset.validateString(name);
        this.ID = IAsset.validateString(ID);
        this.image = image;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public void drawThumbnail(Graphics2D g, int width, int height, ImageObserver obs) {
        int y = (height - TILES_SIZE) / 2;
        int l = (int) Math.floor(width / (double) TILES_SIZE);
        draw(g, (width % TILES_SIZE) / 2, y, l, false, obs);
    }

    public void draw(Graphics2D g, int posX, int posY, int length, boolean flipped, ImageObserver obs) {
        Image img = image.get();
        if (length < 1)
            length = 1;
        if (img == null) {
            g.setColor(Color.pink);
            g.fillRect(posX, posY + (TILES_SIZE - 5) / 2, TILES_SIZE * length, 5);
        } else {
            for (int i = 0; i < length; i++)
                g.drawImage(img, posX + TILES_SIZE * i, posY, obs);

        }
    }

}
