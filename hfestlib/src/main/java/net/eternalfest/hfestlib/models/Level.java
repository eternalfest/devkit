package net.eternalfest.hfestlib.models;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import net.eternalfest.hfestlib.assets.Assets;
import net.eternalfest.hfestlib.assets.AssetsMap;
import net.eternalfest.hfestlib.models.sprites.BackgroundType;
import net.eternalfest.hfestlib.models.sprites.FieldType;
import net.eternalfest.hfestlib.models.sprites.IAssetFrame;
import net.eternalfest.hfestlib.models.sprites.Sprite;
import net.eternalfest.hfestlib.models.sprites.TileType;
import net.eternalfest.hfestlib.utils.FileUtils;
import net.eternalfest.hfestlib.utils.Utils;
import org.jdom2.CDATA;
import org.jdom2.Document;
import org.jdom2.Element;


public class Level {
    //CONSTANTES
    public static final int TILES_SIZE = 20;
    public static final short DEFAULT_WIDTH = 20;
    public static final short DEFAULT_HEIGHT = 25;
    public static final short DEFAULT_PIXEL_WIDTH = TILES_SIZE * (DEFAULT_WIDTH + 1);
    public static final short DEFAULT_PIXEL_HEIGHT = TILES_SIZE * DEFAULT_HEIGHT;
    public static final short MAX_WIDTH = 1024;
    public static final short MAX_HEIGHT = 1024;

    public static final int SHOW_BORDURES_MASK = 1 << 0;
    public static final int SHOW_FRUITS_MASK = 1 << 1;
    public static final int SHOW_SPRITES_MASK = 1 << 2;
    public static final int SHOW_ORDRE_MASK = 1 << 3;
    public static final int SHOW_SPAWNS_MASK = 1 << 4;
    public static final int DEFAULT_DRAW_OPTIONS = (1 << 5) - 1;

    public static final String XML_VERSION = "version";
    public static final String XML_VERSION_NUMBER = "1.2";
    public static final String XML_LVL_ID = "uid";
    public static final String XML_ROOT = "level";
    public static final String XML_NAME = "name";
    public static final String XML_DESC = "desc";
    public static final String XML_SCRIPT = "script";
    public static final String XML_PREFS = "prefs";
    public static final String XML_PREFS_DRAW = "draw";
    public static final String XML_SKINS = "skins";
    public static final String XML_FOND = "fond";
    public static final String XML_PFS_H = "plateformeH";
    public static final String XML_PFS_V = "plateformeV";
    public static final String XML_RAYONS_NAMES = "rayons";
    public static final String XML_RAYON_NAME = "r";
    public static final String XML_PFS = "plateformes";
    public static final String XML_PFS_WIDTH = "w";
    public static final String XML_PFS_HEIGHT = "h";
    public static final String XML_PLAYER = "player";
    public static final String XML_OBJ_EFFET = "effet";
    public static final String XML_OBJ_POINTS = "points";
    public static final String XML_BADLIST = "badlist";
    public static final String XML_BAD = "bad";
    public static final String XML_SPRITELISTLAYER0 = "spritelist0";
    public static final String XML_SPRITELISTLAYER1 = "spritelist1";
    public static final String XML_SPRITE = "sp";
    public static final String XML_ID = "id";
    public static final String XML_IMAGE_ID = "i";
    public static final String XML_POSX = "x";
    public static final String XML_POSY = "y";
    public static final String XML_FLIP = "f";
    public static final String XML_ROTATION = "r";
    public static final String XML_SCRIPT_TYPE = "type";
    public static final String XML_SCRIPT_TYPE_XML = "xml";
    public static final String XML_SCRIPT_TYPE_NEW = "new";
    public static final Image DEFAULT_BORDERS = FileUtils.loadImageResource("bordures.png");

    private static final Image CHIFFRES_ORDRE = FileUtils.loadImageResource("chiffresOrdre.png");
    private static final String[] DEFAULT_FIELD_IDS = new String[]{
        "blanc", "noir", "bleu", "vert", "rouge", "teleport",
        "vortex", "soccer_violet", "soccer_jaune", "J-O", "nobombs"
    };
    private static DecimalFormat DECIMAL_FORMAT = (DecimalFormat) NumberFormat.getInstance(Locale.US);
    private static LevelLayer[] LAYER_LIST = LevelLayer.values();

    static {
        DECIMAL_FORMAT.applyPattern("0.00");
    }

    //ATTRIBUTS
    private String name = "undefined";
    private String description = "";

    private String script = "";
    private boolean isScriptXML = false;

    private LevelTiles tiles;
    private BackgroundType background;
    private BufferedImage borders;

    private Entity selectedEntity = null;

    private Map<LevelLayer, List<Entity>> entities = new EnumMap<>(LevelLayer.class);
    private Map<Entity, LevelLayer> entitiesRev = new HashMap<>();

    private int drawOptions = Level.DEFAULT_DRAW_OPTIONS;

    //CONSTRUCTEURS
    public Level(int width, int height) {
        tiles = new LevelTiles(width, height);
        // TODO?
        borders = new BufferedImage(40, Level.DEFAULT_PIXEL_HEIGHT, BufferedImage.TYPE_INT_ARGB);
        background = BackgroundType.DEFAULT;

        for(LevelLayer layer: LevelLayer.values())
            entities.put(layer, new ArrayList<>());

        setBorders(DEFAULT_BORDERS);
    }

    public Level(int width, int height, String name, String description) {
        this(width, height);
        if (name != null && !name.isEmpty())
            this.name = name;

        if (description != null && !description.isEmpty())
            this.description = description;
    }

    public Level(Document levelXML, Assets assets) {
    	this(1, 1);
    	loadByXMLDocument(levelXML, assets);
    }

    public LevelTiles getTiles() {
        return tiles;
    }

    public void flush() {
        tiles.flush();
    }

    public static String getUidOfXML(Document lvl) {
        return lvl.getRootElement().getAttributeValue(XML_LVL_ID);
    }

    public Stream<Entity> allEntities() {
        return entities.values().stream().flatMap(List::stream);
    }

    public void addEntity(Entity entity, LevelLayer layer) {
        Objects.requireNonNull(entity);
        addEntityAt(entity, layer, entities.get(layer).size());
    }

    public LevelLayer.Pos removeEntity(Entity entity) {
        Objects.requireNonNull(entity);
        LevelLayer layer = entitiesRev.remove(entity);
        if(layer != null) {
            List<Entity> list = entities.get(layer);
            int idx = list.indexOf(entity);
            list.remove(idx);
            entity.attachToLevel(null);

            if(selectedEntity == entity)
                selectedEntity = null;

            return layer.atIndex(idx);
        }
        return null;
    }

    public void clearLayer(LevelLayer layer) {
        List<Entity> list = entities.get(layer);
        for(Entity e: list) {
            if(selectedEntity == e)
                selectedEntity = null;
            entitiesRev.remove(e);
            e.attachToLevel(null);
        }
        list.clear();
    }

    public void clearAllLayers() {
        selectedEntity = null;
        for(List<Entity> list: entities.values()) {
            for(Entity e: list)
                e.attachToLevel(null);
            list.clear();
        }
        entitiesRev.clear();
    }

    public int getLayerSize(LevelLayer layer) {
        return entities.get(layer).size();
    }

    public List<Entity> getLayer(LevelLayer layer) {
        return Collections.unmodifiableList(entities.get(layer));
    }

    public LevelLayer getEntityLayer(Entity entity) {
        Objects.requireNonNull(entity);
        return entitiesRev.get(entity);
    }

    public LevelLayer.Pos getEntityInfo(Entity entity) {
        LevelLayer layer = getEntityLayer(entity);
        if(layer == null)
            return null;
        return layer.atIndex(entities.get(layer).indexOf(entity));
    }

    public Entity getEntityAt(LevelLayer layer, int index) {
        return entities.get(layer).get(index);
    }

    public Entity setEntityAt(Entity entity, LevelLayer layer, int index) {
        if(entitiesRev.containsKey(entity))
            throw new IllegalStateException("Entity already exists in the level!");
        Entity removed = entities.get(layer).set(index, entity);
        entity.attachToLevel(this);
        if(selectedEntity == removed)
            selectedEntity = null;
        return removed;
    }

    public void addEntityAt(Entity entity, LevelLayer layer, int index) {
        List<Entity> list = entities.get(layer);
        if(layer == LevelLayer.PLAYER_SPAWN && !list.isEmpty())
            throw new IllegalStateException("Can't have more than one player in the level!");

        LevelLayer tmp = entitiesRev.put(entity, layer);
        if(tmp != null) {
            entitiesRev.put(entity, tmp);
            throw new IllegalStateException("Entity already exists in the level!");
        }
        list.add(index, entity);
        entity.attachToLevel(this);
    }

    public void swapEntities(LevelLayer layer, int i, int j) {
        if(i == j)
            return;
        List<Entity> list = entities.get(layer);
        Entity tmp = list.get(i);
        list.set(i, list.get(j));
        list.set(j, tmp);
    }

    public Entity getSelected() {
        return selectedEntity;
    }

    public void setSelected(Entity entity) {
        selectedEntity = entity;
    }

    //METHODES DU NIVEAU
    @Override
    public String toString() {
        return getName();
    }

    public String getName() {
        return name;
    }

    public void setName(String str) {
        if (str == null)
            name = "undefined";
        else name = str;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String str) {
        if (str == null)
            description = "";
        else description = str;

    }

    public String getScript() {
        return script;
    }

    public void setScript(String str) {
        if (str != null)
            script = str.trim();
        else script = "";
    }

    public boolean isScriptXML() {
        return isScriptXML;
    }

    public void setScriptType(boolean isXML) {
        isScriptXML = isXML;
    }

    public short getHeight() {
        return tiles.getHeight();
    }

    public short getWidth() {
        return tiles.getWidth();
    }

    public int getPixelHeight() {
    	return tiles.getHeight() * TILES_SIZE;
    }

    public int getPixelWidth() {
    	return (tiles.getWidth() + 1) * TILES_SIZE;
    }

    public void addDrawOptions(int options, boolean value) {
        if (value)
            drawOptions |= options;
        else drawOptions -= drawOptions & options;
    }

    public int getDrawOptions() {
        return drawOptions;
    }

    public void setDrawOptions(int options) {
        drawOptions = options;
    }

    public boolean isDrawOption(int option) {
        return (option & drawOptions) != 0;
    }

    public void setBorders(Image img) {
        Graphics2D g = borders.createGraphics();
        g.setBackground(new Color(0, 0, 0, 0));
        g.clearRect(0, 0, borders.getWidth(null), borders.getHeight(null));
        g.drawImage(img, 0, 0, null);
        g.dispose();
    }

    public BackgroundType getBackground() {
        return background;
    }

    public void setBackground(BackgroundType fond) {
        if (fond != null)
            this.background = fond;
        else this.background = BackgroundType.DEFAULT;
    }

    public Entity getEntityAt(double x, double y) {
        // On regarde DANS L'ORDRE INVERSE de l'affichage, pour
        // sélectionner en priorité les sprites AU-DESSUS des autres
        for(int i = LAYER_LIST.length - 1; i >= 0; i--) {
            if(!isLayerVisible(null, null, LAYER_LIST[i]))
                continue;
            List<Entity> list = entities.get(LAYER_LIST[i]);
            for(int j = list.size() - 1; j >= 0; j--) {
                Entity entity = list.get(j);
                if (entity != null && hitTest(entity, x, y))
                    return entity;
            }
        }

        return null;
    }

    private boolean hitTest(Entity entity, double x, double y) {
        return entity.getBoundingBox().contains(TILES_SIZE*x, TILES_SIZE*y);
    }

    public void loadByXMLDocument(Document levelXML, Assets assets) {
        Element root = levelXML.getRootElement();
        Element name = root.getChild(XML_NAME),
            desc = root.getChild(XML_DESC),
            script = root.getChild(XML_SCRIPT),
            prefs = root.getChild(XML_PREFS),
            skins = root.getChild(XML_SKINS),
            rayons = root.getChild(XML_RAYONS_NAMES),
            plateformes = root.getChild(XML_PFS),
            bads = root.getChild(XML_BADLIST),
            spritesLayer0 = root.getChild(XML_SPRITELISTLAYER0),
            spritesLayer1 = root.getChild(XML_SPRITELISTLAYER1),
            objPlayer = root.getChild(XML_PLAYER);

        List<Element> objsEffet = root.getChildren(XML_OBJ_EFFET), objsPoints = root.getChildren(XML_OBJ_POINTS);

        this.name = name == null ? "" : name.getTextTrim();
        if(this.name.isEmpty())
            this.name = "undefined";

        description = desc == null ? "" : desc.getTextTrim();
        if(description.isEmpty())
            description = "Pas de description";

        this.script = script == null ? "" : script.getTextTrim();
        if (!this.script.isEmpty()) {
            //Si pas d'attribut, alors c'est du XML
            this.isScriptXML = !XML_SCRIPT_TYPE_NEW.equals(script.getAttributeValue(XML_SCRIPT_TYPE));
        }

        if (prefs != null)
            drawOptions = Utils.parseInt(prefs.getAttributeValue(XML_PREFS_DRAW));
        else drawOptions = Level.DEFAULT_DRAW_OPTIONS;

        Element skinFond = null, skinPfsH = null, skinPfsV = null;
        if (skins != null) {
            skinFond = skins.getChild(XML_FOND);
            skinPfsH = skins.getChild(XML_PFS_H);
            skinPfsV = skins.getChild(XML_PFS_V);
        }

        if (skinFond != null) {
            BackgroundType fond = assets.backgrounds().byID(skinFond.getAttributeValue(XML_ID));
            setBackground(fond);
        } else setBackground(null);

        if (skinPfsH != null) {
            TileType pfsH = assets.tiles().byID(skinPfsH.getAttributeValue(XML_ID));
            tiles.setTileTypeH(pfsH);
        } else tiles.setTileTypeH(null);

        if (skinPfsV != null) {
            TileType pfsV = assets.tiles().byID(skinPfsV.getAttributeValue(XML_ID));
            tiles.setTileTypeV(pfsV);
        } else tiles.setTileTypeV(null);

        if (plateformes != null) {
        	int width = Utils.parseInt(plateformes.getAttributeValue(XML_PFS_WIDTH), DEFAULT_WIDTH);
        	int height = Utils.parseInt(plateformes.getAttributeValue(XML_PFS_HEIGHT), DEFAULT_HEIGHT);
            List<FieldType> rayonList;

            if (rayons != null) {
                rayonList = new ArrayList<>();
                for (Element child : rayons.getChildren(XML_RAYON_NAME)) {
                    String id = child.getAttributeValue(XML_NAME);
                    if ("none".equals(id) && rayonList.isEmpty()) {
                        // Workaround for `.lvl` files created with a version of editor storing the `none` field.
                        continue;
                    }
                    rayonList.add(assets.fields().byID(id));
                }
            } else {
                // Older `.lvl` files don't have a field list
                rayonList = Arrays.stream(DEFAULT_FIELD_IDS)
                    .map(assets.fields()::byID)
                    .collect(Collectors.toList());
            }

            tiles.loadFromString(width, height, plateformes.getText(), rayonList);
        } else {
        	tiles = new LevelTiles(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        }

        clearAllLayers();

        // SPAWNS
        if (objPlayer != null)
            addEntity(getSpecialEntityFromXML(assets, objPlayer, "spawnIgor"), LevelLayer.PLAYER_SPAWN);

        for (Element objEffet : objsEffet)
            addEntity(getSpecialEntityFromXML(assets, objEffet, "spawnEffet"), LevelLayer.SPEC_ITEM_SPAWN);

        for (Element objPoints : objsPoints)
            addEntity(getSpecialEntityFromXML(assets, objPoints, "spawnPoints"), LevelLayer.SCORE_ITEM_SPAWN);


        //FRUITS
        if (bads != null) {
            List<Element> listBad = bads.getChildren();

            for (Element el : listBad) {
                Entity bad = getEntityFromXML(assets, assets.bads(), el, el.getAttributeValue(XML_ID));
                addEntity(bad, LevelLayer.preferredFruitLayerFor(bad.getSprite()));
            }
        }

        //SPRITES (COUCHE 0)
        if (spritesLayer0 != null) {
            for (Element el : spritesLayer0.getChildren()) {
                Entity entity = getEntityFromXML(assets, assets.sprites(), el, el.getAttributeValue(XML_ID));
                addEntity(entity, LevelLayer.BACKGROUND);
            }
        }

        //SPRITES (COUCHE 1)
        if (spritesLayer1 != null) {
            for (Element el : spritesLayer1.getChildren()) {
                Entity entity = getEntityFromXML(assets, assets.sprites(), el, el.getAttributeValue(XML_ID));
                addEntity(entity, LevelLayer.FOREGROUND);
            }
        }

        selectedEntity = null;
    }

    public Document getXMLDocument() {
        Document levelXML = new Document();
        Element root = new Element(XML_ROOT);
        levelXML.setRootElement(root);
        root.setAttribute(XML_VERSION, XML_VERSION_NUMBER);
        root.setAttribute(XML_LVL_ID, (System.currentTimeMillis() / 1000) + "" + (int) (Math.random() * 10000));

        Element name = new Element(XML_NAME);
        name.addContent(this.name);
        root.addContent(name);

        Element desc = new Element(XML_DESC);
        desc.addContent(description);
        root.addContent(desc);

        if(!this.script.isEmpty()) {
            Element script = new Element(XML_SCRIPT);
            script.addContent(new CDATA(this.script));
            script.setAttribute(XML_SCRIPT_TYPE, isScriptXML ? XML_SCRIPT_TYPE_XML : XML_SCRIPT_TYPE_NEW);
            root.addContent(script);
        }

        Element prefs = new Element(XML_PREFS);
        prefs.setAttribute(XML_PREFS_DRAW, Integer.toString(drawOptions));
        root.addContent(prefs);

        Element skins = new Element(XML_SKINS), fond = new Element(XML_FOND), plateformeH = new Element(XML_PFS_H), plateformeV = new Element(XML_PFS_V);
        fond.setAttribute(XML_ID, this.background.getID());
        plateformeH.setAttribute(XML_ID, tiles.getTileTypeH().getID());
        plateformeV.setAttribute(XML_ID, tiles.getTileTypeV().getID());

        skins.addContent(fond);
        skins.addContent(plateformeH);
        skins.addContent(plateformeV);
        root.addContent(skins);

        List<FieldType> fieldsList = new ArrayList<>();
        String tilesString = tiles.emitToString(fieldsList);

        Element rayons = new Element(XML_RAYONS_NAMES);
        for (FieldType r : fieldsList) {
            Element e = new Element(XML_RAYON_NAME);
            e.setAttribute(XML_NAME, r.getID());
            rayons.addContent(e);
        }
        root.addContent(rayons);

        Element plateformes = new Element(XML_PFS);
        if (getWidth() != DEFAULT_WIDTH || getHeight() != DEFAULT_HEIGHT) {
        	plateformes.setAttribute(XML_PFS_WIDTH, Short.toString(getWidth()));
        	plateformes.setAttribute(XML_PFS_HEIGHT, Short.toString(getHeight()));
        }
        plateformes.addContent(tilesString);
        root.addContent(plateformes);

        //SPAWNS
        for (Entity player : entities.get(LevelLayer.PLAYER_SPAWN))
            root.addContent(setXMLFromEntity(new Element(XML_PLAYER), player));

        for (Entity specItem : entities.get(LevelLayer.SPEC_ITEM_SPAWN))
            root.addContent(setXMLFromEntity(new Element(XML_OBJ_EFFET), specItem));

        for (Entity scoreItem : entities.get(LevelLayer.SCORE_ITEM_SPAWN))
            root.addContent(setXMLFromEntity(new Element(XML_OBJ_POINTS), scoreItem));

        //FRUITS
        Element bads = new Element(XML_BADLIST);
        for(Entity bad: entities.get(LevelLayer.FRUITS_PIC))
            bads.addContent(setXMLFromEntity(new Element(XML_BAD), bad));

        for(Entity bad: entities.get(LevelLayer.FRUITS_ORDER))
            bads.addContent(setXMLFromEntity(new Element(XML_BAD), bad));

        for(Entity bad: entities.get(LevelLayer.FRUITS_OTHER))
            bads.addContent(setXMLFromEntity(new Element(XML_BAD), bad));

        if(!bads.getChildren().isEmpty())
            root.addContent(bads);

        //SPRITES
        Element sprites0 = new Element(XML_SPRITELISTLAYER0);
        for (Entity sprite: entities.get(LevelLayer.BACKGROUND))
            sprites0.addContent(setXMLFromEntity(new Element(XML_SPRITE), sprite));

        if(!sprites0.getChildren().isEmpty())
            root.addContent(sprites0);

        Element sprites1 = new Element(XML_SPRITELISTLAYER1);
        for (Entity sprite: entities.get(LevelLayer.FOREGROUND))
            sprites1.addContent(setXMLFromEntity(new Element(XML_SPRITE), sprite));

        if(!sprites1.getChildren().isEmpty())
            root.addContent(sprites1);

        return levelXML;
    }

    private static Element setXMLFromEntity(Element element, Entity entity) {
        Sprite sprite = entity.getSprite();
        element.setAttribute(XML_ID, sprite.getID());
        element.setAttribute(XML_IMAGE_ID, entity.getFrame().getID());
        element.setAttribute(XML_POSX, DECIMAL_FORMAT.format(entity.getX()));
        element.setAttribute(XML_POSY, DECIMAL_FORMAT.format(entity.getY()));
        int flip = 0;
        if((entity.getOrientation() & Sprite.FLIP_H) != 0)
            flip += 1;
        if((entity.getOrientation() & Sprite.FLIP_V) != 0)
            flip += 2;
        element.setAttribute(XML_FLIP, Integer.toString(flip));
        element.setAttribute(XML_ROTATION, Integer.toString((entity.getOrientation() & Sprite.ROTATION_MASK) >> Sprite.ROTATION_SHIFT));
        return element;
    }

    private static Entity getEntityFromXML(Assets assets, AssetsMap<Sprite> sprites, Element el, String spriteID) {
        return makeEntityFromXML(assets.frames(), el, sprites.byID(spriteID), spriteID);
    }

    private static Entity getSpecialEntityFromXML(Assets assets, Element el, String specialName) {
        Sprite sprite = assets.getSpecial(specialName);
        return makeEntityFromXML(assets.frames(), el, sprite, sprite.getID());
    }

    private static Entity makeEntityFromXML(AssetsMap<IAssetFrame> frames, Element el, Sprite sprite, String spriteID) {
        Entity entity = new Entity(sprite);
        String frameID = el.getAttributeValue(XML_IMAGE_ID);
        if(frameID == null) {
            frameID = sprite.getDefaultFrame().getID();
        } else if(!frames.has(frameID) && !frameID.contains(":")) {
            frameID = guessFrameID(frames, spriteID, frameID);
        }

        entity.setFrame(frames.byID(frameID));

        entity.setPos(Utils.parseDouble(el.getAttributeValue(XML_POSX)), Utils.parseDouble(el.getAttributeValue(XML_POSY)));
        int orientation = Utils.parseInt(el.getAttributeValue(XML_ROTATION));
        if(orientation >= 4) // some versions shifted the orientation in the wrong direction
            orientation >>= Sprite.ROTATION_SHIFT;
        else orientation <<= Sprite.ROTATION_SHIFT;
        int flip = Utils.parseInt(el.getAttributeValue(XML_FLIP));
        if (flip % 2 == 1)
            orientation |= Sprite.FLIP_H;
        if (flip > 1)
            orientation |= Sprite.FLIP_V;
        entity.setOrientation(orientation);
        return entity;
    }

    private static String guessFrameID(AssetsMap<IAssetFrame> frames, String spriteID, String frameID) {
        // Transform old-style id: 'sprite_id_frame_id' or 'frame_id' into
        // new-style id: 'sprite_id:frame_id'
        if (frameID.startsWith(spriteID + "_"))
            return spriteID + ":" + frameID.substring(spriteID.length() + 1);

        // Try to find a corresponding frame
        int underscore = frameID.indexOf('_');
        while(underscore >= 0) {
            String id = spriteID + ":" + frameID.substring(underscore+1);
            if(frames.has(id))
                return id;

            underscore = frameID.indexOf('_', underscore+1);
        }

        return spriteID + ":" + frameID;
    }

    private boolean isLayerVisible(Graphics2D g, ImageObserver obs, LevelLayer layer) {
        switch(layer) {
        case FOREGROUND:
            // Hack, draw borders before drawing foreground sprites
            if (g != null && (drawOptions & Level.SHOW_BORDURES_MASK) != 0) {
            	drawBorders(g, obs);
            }
        case BACKGROUND:
            return (drawOptions & Level.SHOW_SPRITES_MASK) != 0;

        case PLAYER_SPAWN:
        case SPEC_ITEM_SPAWN:
        case SCORE_ITEM_SPAWN:
            return (drawOptions & Level.SHOW_SPAWNS_MASK) != 0;

        case FRUITS_PIC:
        case FRUITS_ORDER:
        case FRUITS_OTHER:
            return (drawOptions & Level.SHOW_FRUITS_MASK) != 0;
        }
        return false;
    }

    private void drawBorders(Graphics2D g, ImageObserver obs) {
        Rectangle clip = g.getClipBounds();
        Objects.requireNonNull(clip, "Cannot draw border without explicit clipping area");

    	int height = borders.getHeight(obs);
    	int ymin = Math.floorDiv(clip.y, height) * height;
    	int ymax = clip.y + clip.height;
    	for (int y = ymin; y < ymax; y += height) {
            g.drawImage(borders, -Level.TILES_SIZE, y, obs);
            g.drawImage(borders, Level.TILES_SIZE * getWidth() - Level.TILES_SIZE / 2, y, obs);
    	}
    }

    public void draw(Graphics2D g, ImageObserver obs) {
        Shape clip = g.getClip();
        g.clipRect(0, 0, getPixelWidth(), getPixelHeight());

        AffineTransform saveAT = g.getTransform();
        g.translate(10, 0);

        // Tiles and backgrounds
        g.setColor(Color.gray);
        background.draw(g, -10, 0, obs);
        tiles.draw(g, obs);

        // Entities (this also draw the borders)
        for(Map.Entry<LevelLayer, List<Entity>> layer: entities.entrySet()) {
            if(isLayerVisible(g, obs, layer.getKey())) {
                for(Entity e: layer.getValue())
                    e.draw(g, obs);
            }
        }

        // Order
        if ((drawOptions & Level.SHOW_ORDRE_MASK) != 0)
            drawOrder(g, obs);


        // Selection (if it exists)
        if (selectedEntity != null) {
            g.setColor(new Color(255, 255, 255, 128));
            Rectangle bbox = selectedEntity.getBoundingBox();
            g.fillRect(bbox.x, bbox.y, bbox.width, bbox.height);
        }


        g.setTransform(saveAT);
        g.setClip(clip);
    }

    private void drawOrder(Graphics2D g, ImageObserver obs) {
        List<Entity> bads = entities.get(LevelLayer.FRUITS_ORDER);
        if (bads.size() <= 1)
            return;

        int offX = -Level.TILES_SIZE/2;
        int offY = CHIFFRES_ORDRE.getHeight(null) + 4;
        int num = 1;

        for (Entity bad: bads) {
            Rectangle bbox = bad.getBoundingBox();

            int x = (2*bbox.x + bbox.width)/2 + offX;
            int y = bbox.y - offY;

            if (y < 0)
                y += bad.getFrame().getHeight() + Level.TILES_SIZE;

            if (num < 10)
                drawOrderDigit(num, x, y, g, obs);
            else {
                drawOrderDigit(num % 10, x + 5, y, g, obs);
                drawOrderDigit(num / 10, x - 4, y, g, obs);
            }
            num++;
        }
    }

    private void drawOrderDigit(int digit, int x, int y, Graphics2D g, ImageObserver obs) {
        if (digit < 0)
            digit = 0;
        if (digit > 9)
            digit = 9;

        g.drawImage(CHIFFRES_ORDRE, x, y, x + Level.TILES_SIZE, y + Level.TILES_SIZE,
            digit * Level.TILES_SIZE, 0, (digit + 1) * Level.TILES_SIZE, Level.TILES_SIZE, obs);
    }

	public boolean isInBounds(int x, int y) {
		return x >= 0 && x < getWidth() && y >= 0 && y < getHeight();
	}
}

