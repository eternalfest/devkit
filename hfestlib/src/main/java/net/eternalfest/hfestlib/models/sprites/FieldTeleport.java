package net.eternalfest.hfestlib.models.sprites;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.ImageObserver;

import net.eternalfest.hfestlib.utils.Lazy;

public class FieldTeleport extends FieldType {

    private static int SIZE_END = 10;

    public FieldTeleport(Lazy<Image> image, String name, String ID) {
        super(image, name, ID);
    }

    @Override
    public void draw(Graphics2D g, int posX, int posY, int length, boolean flipped, ImageObserver obs) {
        if (length < 1)
            length = 1;
        Image img = image.get();

        if (img == null) {
            super.draw(g, posX, posY, length, flipped, obs);
        } else {
            int w = img.getWidth(null), h = img.getHeight(null);
            g.drawImage(img, posX, posY, posX + length * TILES_SIZE, posY + h, 2 * SIZE_END, 0, w, h, obs);
            // Always draw the extremities, even for 1 tile teleporters. This is not what the game does,
            // but teleporters without their extremities are hard to see and easily missed.
            g.drawImage(img, posX, posY, posX + SIZE_END, posY + h, 0, 0, SIZE_END, h, obs);
            g.drawImage(img, posX + length * TILES_SIZE, posY, posX + length * TILES_SIZE - SIZE_END, posY + h, 2 * SIZE_END, 0, SIZE_END, h, obs);
        }
    }
}
