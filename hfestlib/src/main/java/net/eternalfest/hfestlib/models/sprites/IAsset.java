package net.eternalfest.hfestlib.models.sprites;

public interface IAsset {
    public String getName();

    public String getID();

    public static String validateString(String name) {
        if (name == null || name.equals(""))
            return "<undefined>";
        return name;
    }
}
