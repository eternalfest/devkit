package net.eternalfest.hfestlib.models;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.ImageObserver;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import javax.swing.event.EventListenerList;

import net.eternalfest.hfestlib.models.sprites.FieldType;
import net.eternalfest.hfestlib.models.sprites.TileType;

public class LevelTiles {

    public static interface Listener extends EventListener {
    	// TODO: do we need a tilesResized event?
        public void tilesChanged(LevelTiles tiles);
        public void skinChanged(LevelTiles tiles);
    }

    public enum Shadows {
        NONE, SEMI, FULL;
    }

    // Higher values wouldn't be encodable as a printable ASCII char.
    public static final int MAX_RAW_FIELD_VALUE = 62;

    private static final int TILES_SIZE = Level.TILES_SIZE;

    /**
     * Width of the level in tiles.
     */
    private short width;

    /**
     * Height of the level in tiles.
     */
    private short height;

    /**
     * Listeners, triggered when is changed.
     */
    EventListenerList listeners = new EventListenerList();

    /**
     * Style used for the shadows.
     */
    private Shadows shadows = Shadows.SEMI;

    /**
     * The level's tiles; -1 for ground, >0 for fields.
     */
    private byte[] tiles;

    /**
     * Tile type used for horizontal tiles.
     */
    private TileType horizTiles = TileType.DEFAULT;

    /**
     * Tile type used for vertical tiles.
     */
    private TileType vertTiles = TileType.DEFAULT;

    /**
     * Current mapping between internal field ids and field types.
     */
    private FieldMap fieldMap;

    /**
     * Boolean indicating if the segment list needs to be recomputed.
     */
    private boolean recalculate = true;

    private List<Segment> segments;

    private static void validateDimensions(int width, int height) {
    	if (width <= 0 || width > Level.MAX_WIDTH || height <= 0 || height > Level.MAX_HEIGHT)
    		throw new IllegalArgumentException("Invalid level dimensions: (" + width + ", " + height + ")");
    }

    public LevelTiles(int width, int height) {
    	validateDimensions(width, height);
        this.width = (short) width;
        this.height = (short) height;
        fieldMap = new FieldMap();
        tiles = new byte[width * height];
        segments = new ArrayList<>();
    }

    public short getWidth() {
        return width;
    }

    public short getHeight() {
        return height;
    }

    private byte get(int x, int y) {
    	return tiles[height * x + y];
    }

    private void set(int x, int y, int value) {
        if (isInRange(x, y)) {
            tiles[height * x + y] = (byte) value;
        }
    }

    public byte getRawAt(int x, int y) {
        return isInRange(x, y) ? get(x, y) : 0;
    }

    public void setRawAt(int x, int y, int value) {
        if (isInRange(x, y)) {
        	int idx = height * x + y;
        	if (tiles[idx] != value) {
        		tiles[idx] = (byte) value;
        		tilesChanged();
        	}
        }
    }

    private boolean isInRange(int x, int y) {
        return 0 <= x && x < width && 0 <= y && y < height;
    }

    public void setShadows(Shadows shadows) {
        this.shadows = shadows;
        tilesChanged();
    }

    public TileType getTileTypeH() {
        return horizTiles;
    }

    public void setTileTypeH(TileType tileType) {
        if (tileType == null) {
            tileType = TileType.DEFAULT;
        }
        horizTiles = tileType;
        skinChanged();
    }

    public TileType getTileTypeV() {
        return vertTiles;
    }

    public void setTileTypeV(TileType tileType) {
        if (tileType == null) {
            tileType = TileType.DEFAULT;
        }
        vertTiles = tileType;
        skinChanged();
    }

    public boolean isTileAt(int x, int y) {
        return getRawAt(x, y) == -1;
    }

    public boolean isFieldAt(int x, int y) {
        return getRawAt(x, y) > 0;
    }

    public FieldType getFieldAt(int x, int y) {
        byte raw = getRawAt(x, y);
        return raw > 0 ? fieldMap.getField(raw) : null;
    }

    public void addTileAt(int x, int y) {
    	setRawAt(x, y, -1);
    }

    public void addFieldAt(int x, int y, FieldType type) {
    	setRawAt(x, y, fieldMap.getOrCreateId(type));
    }

    public void removeAt(int x, int y) {
    	setRawAt(x, y, 0);
    }

    public void shift(int dx, int dy) {
        // Shift without copy: the conditionals ensure that the grid is traversed in the right order.
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int x = dx >= 0 ? width - 1 - i : i;
                int y = dy >= 0 ? height - 1 - j : j;
                set(x, y, getRawAt(x - dx, y - dy));
            }
        }

        tilesChanged();
    }

    /**
     * Flip the level horizontally.
     */
    public void reverseH() {
        for (int i = 0; i < width / 2; i++) {
            int ii = width - 1 - i;
            for (int j = 0; j < height; j++) {
            	int tmp = get(i, j);
            	set(i, j, get(ii, j));
            	set(ii, j, tmp);
            }
        }

        tilesChanged();
    }

    /**
     * Flip the level vertically.
     */
    public void reverseV() {
        for (int j = 0; j < height / 2; j++) {
            int jj = height - 1 - j;
            for (int i = 0; i < width; i++) {
            	int tmp = get(i, j);
                set(i, j, get(i, jj));
                set(i, jj, tmp);
            }
        }

        tilesChanged();
    }

    public void clear() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                set(i, j, 0);
            }
        }
        tilesChanged();
    }

    public void resize(int width, int height) {
    	resize(width, height, true);
    }

    private void resize(int width, int height, boolean preserveTiles) {
    	if (width == this.width && height == this.height)
    		return;
    	validateDimensions(width, height);
    	byte[] tiles = new byte[width * height];

    	if (preserveTiles) {
    		int w = Math.min(width, this.width);
    		int h = Math.min(height, this.height);
    		for (int i = 0; i < w; i++) {
    			for (int j = 0; j < h; j++) {
    				tiles[height * i + j] = get(i, j);
    			}
    		}
    	}

    	this.width = (short) width;
    	this.height = (short) height;
    	this.tiles = tiles;
    	recalculate = true;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder(width * height);

        for (int i = 0; i < tiles.length; i++) {
        	str.append(FieldMap.encodeTile(tiles[i]));
        }

        return str.toString();
    }

    public void loadFromString(int width, int height, String string, List<FieldType> fieldMap) {
    	resize(width, height, false);
        this.fieldMap = new FieldMap(fieldMap);
        int length = string.length();

        StringBuilder str = new StringBuilder(string);
        for (int i = height * width; i > length; i--)
            str.append('0');

        byte maxId = this.fieldMap.getMaxId();
        for (int i = 0; i < tiles.length; i++) {
        	tiles[i] = FieldMap.decodeTile(str.charAt(i), maxId);
        }

        tilesChanged();
    }
    public String emitToString(List<FieldType> outFields) {
    	if (!outFields.isEmpty())
    		throw new IllegalArgumentException("'outFields' parameter should be empty");

    	StringBuilder str = new StringBuilder(width * height);
    	byte[] compactIds = new byte[128];

    	// 'Compact' field ids to not save unused fields in the field list.
    	for (int i = 0; i < tiles.length; i++) {
    		byte tile = tiles[i];
    		if (tile > 0) {
    			if (compactIds[tile] == 0) {
    				outFields.add(this.fieldMap.getField(tile));
    				compactIds[tile] = (byte) outFields.size();
    			}
    			tile = compactIds[tile];
    		}

    		str.append(FieldMap.encodeTile(tile));
    	}

    	return str.toString();
    }

    private List<Segment> getSegments() {
    	if (!recalculate)
    		return segments;
    	recalculate = false;
    	segments.clear();

    	Segment[] verts = new Segment[width];

    	for (short j = 0; j < height; j++) {
    		Segment horiz = null;
    		for (short i = 0; i < width; i++) {
    			byte raw = get(i, j);
    			if (raw == 0) {
    				// Skip empty tiles.
    				verts[i] = horiz = null;
    				continue;
    			}

    			Segment vert = verts[i];
    			boolean isField = raw != -1;
    			FieldType type = isField ? fieldMap.getField(raw) : null;

    			boolean hasLeft = horiz != null && horiz.type == type;
    			boolean hasUp = vert != null && vert.type == type;
    			boolean hasRight = i + 1 < width && get(i + 1, j) == raw;

    			if (hasLeft) {
    				// Continue existing horizontal segment.
    				horiz.len += 1;
    			} else if (!hasRight && !hasUp) {
					// Either a 1x1 or the top of a vertical segment.
					horiz = new Segment(i, j, true, (short) 1, type);
					verts[i] = horiz;
					segments.add(horiz);
					continue;
    			} else if (hasRight && !(hasUp && isField)) {
    				// Create a new horizontal segment (unless there's already a vertical field).
    				horiz = new Segment(i, j, true, (short) 1, type);
    				verts[i] = null;
    				segments.add(horiz);
    				continue;
    			} else {
        			horiz = null;
    			}

    			if (hasUp && (isField || (!hasLeft && !hasRight))) {
    				// Continue existing vertical segment (note special case for fields).
    				vert.horiz = false;
    				vert.len += 1;
    			} else {
    				verts[i] = null;
    			}
    		}
    	}

    	// Fields should always show above tiles.
    	segments.sort(Comparator.comparingInt(s -> s.type == null ? 0 : 1));

    	return segments;
    }

    public void draw(Graphics2D g, ImageObserver obs) {
		Graphics2D gg = (Graphics2D) g.create();

        List<Segment> segments = getSegments();

        if (shadows == Shadows.FULL) {
        	drawFullShadows(gg, segments);
        } else if (shadows == Shadows.SEMI) {
        	drawSemiShadows(gg, segments);
        }

        drawSegments(gg, segments, obs);
    }

    private static void drawFullShadows(Graphics2D g, List<Segment> segments) {
    	for (Segment s: segments) {
    		// Tiles only.
    		if (s.type != null) continue;

    		int w = s.horiz ? s.len : 1;
    		int h = s.horiz ? 1 : s.len;
    		g.fillRect(s.x*TILES_SIZE + 4, s.y*TILES_SIZE + 4, w, h);
    	}
    }

    private static void drawSemiShadows(Graphics2D g, List<Segment> segments) {
        Color ombre0 = new Color(0, 0, 0, 75);
        Color ombre1 = new Color(0, 0, 0, 35);
        Color ombre2 = new Color(0, 0, 0, 20);

    	for (Segment s: segments) {
    		// Tiles only.
    		if (s.type != null) continue;

    		int x1 = s.x, y1 = s.y;

    		if (s.horiz) {
    			int x2 = s.x + s.len - 1;
                g.setColor(ombre0);
                g.fillRect(x2*TILES_SIZE + 6, y1*TILES_SIZE + 5, TILES_SIZE - 3, TILES_SIZE - 2);
                if (s.len > 1)
    				g.fillRect(x1*TILES_SIZE + 6, y1*TILES_SIZE + 5, (s.len-1)*TILES_SIZE, TILES_SIZE - 2);
                g.setColor(ombre1);
                g.fillRect(x2*TILES_SIZE + 6, (y1 + 1)*TILES_SIZE + 3, TILES_SIZE - 8, 1);
                if (s.len > 1)
    				g.fillRect(x1*TILES_SIZE + 6, (y1 + 1)*TILES_SIZE + 3, (s.len-1)*TILES_SIZE, 1);
                g.setColor(ombre2);
                g.fillRect((x2 + 1)*TILES_SIZE - 2, (y1 + 1)*TILES_SIZE + 3, 5, 1);
            } else {
            	int y2 = s.y + s.len - 1;
                g.setColor(ombre0);
                g.fillRect(x1*TILES_SIZE + 5, y2*TILES_SIZE + 6, TILES_SIZE - 2, TILES_SIZE - 2);
                if (s.len > 1)
                	g.fillRect(x1*TILES_SIZE + 5, y1*TILES_SIZE + 6, TILES_SIZE - 2, (s.len - 1)*TILES_SIZE);
                g.setColor(ombre1);
                g.fillRect((x1 + 1)*TILES_SIZE + 3, y2*TILES_SIZE + 6, 1, TILES_SIZE - 8);
                if (s.len > 1)
                	g.fillRect((x1 + 1)*TILES_SIZE + 3, y1*TILES_SIZE + 6, 1, (s.len - 1)*TILES_SIZE);
                g.setColor(ombre2);
                g.fillRect((x1 + 1)*TILES_SIZE + 3, (y2 + 1)*TILES_SIZE - 2, 1, 6);
    		}
    	}
    }

    private void drawSegments(Graphics2D g, List<Segment> segments, ImageObserver obs) {
    	AffineTransform horizAT = g.getTransform();
    	AffineTransform vertAT = new AffineTransform(horizAT);
    	vertAT.rotate(Math.PI/2, 0, 0);
    	vertAT.scale(1.0, -1.0);

    	for (Segment s: segments) {
    		g.setTransform(s.horiz ? horizAT : vertAT);
    		int x = (s.horiz ? s.x : s.y)*TILES_SIZE;
    		int y = (s.horiz ? s.y : s.x)*TILES_SIZE;

    		if (s.type == null) {
    			(s.horiz ? horizTiles : vertTiles).draw(g, x, y, s.len, obs);
    		} else {
    			boolean flipped = !s.horiz && (s.x == 0 || get(s.x - 1, s.y) == -1);
    			s.type.draw(g, x, y, s.len, flipped, obs);
    		}
    	}
    }

    public void flush() {
        tiles = null;
        segments = null;
    }

    public void addListener(Listener l) {
        listeners.add(Listener.class, l);
    }

    public void removeListener(Listener l) {
        listeners.remove(Listener.class, l);
    }

    private void tilesChanged() {
        recalculate = true;
        for (Listener l : listeners.getListeners(Listener.class)) {
            l.tilesChanged(this);
        }
    }

    private void skinChanged() {
        for (Listener l : listeners.getListeners(Listener.class)) {
            l.skinChanged(this);
        }
    }

    private static class Segment {
    	short x, y, len;
    	boolean horiz;
    	FieldType type; // null for tiles

    	public Segment(short x, short y, boolean horiz, short len, FieldType type) {
    		this.x = x;
    		this.y = y;
    		this.horiz = horiz;
    		this.len = len;
    		this.type = type;
    	}
    }

    /**
     * This class maintains a mapping between internal field ids and field types.
     * The exposed id is in the inclusive range `[1, 126]`.
     */
    private static class FieldMap {
        private final ArrayList<FieldType> idToField;
        private final HashMap<FieldType, Byte> fieldToId;

        FieldMap(Iterable<FieldType> fieldMap) {
            idToField = new ArrayList<>();
            fieldToId = new HashMap<>();
            for (FieldType fieldType : fieldMap) {
                Objects.requireNonNull(fieldType);
                byte id = (byte) idToField.size();
                idToField.add(fieldType);
                fieldToId.put(fieldType, id);
            }
        }

        FieldMap() {
            this(new ArrayList<>());
        }

        FieldType getField(byte id) {
            if (id == 0) {
                return FieldType.DEFAULT;
            }
            return this.idToField.get(id - 1);
        }

        byte getOrCreateId(FieldType fieldType) {
            Objects.requireNonNull(fieldType);
            Byte id = fieldToId.get(fieldType);
            if (id == null) {
                id = (byte) idToField.size();
                idToField.add(fieldType);
                fieldToId.put(fieldType, id);
            }
            if (id + 1 >= MAX_RAW_FIELD_VALUE) {
                throw new IllegalStateException("max number of fields reached!");
            }
            return (byte) (id + 1);
        }

        byte getMaxId() {
            return (byte) this.idToField.size();
        }

        static byte decodeTile(char chr, byte maxId) {
        	switch (chr) {
        	case '0':
        		return 0;
        	case '1':
        		return -1;
        	default:
        		int id = chr - ('A' - 1);
        		if (id >= 1 &&id <= maxId) {
        			return (byte) id;
        		} else {
                    System.err.println("Invalid field id: " + id);
                    return 0;
        		}
        	}
        }

        static char encodeTile(byte tile) {
        	switch (tile) {
        	case 0:
        		return '0';
        	case -1:
        		return '1';
        	default:
        		return (char) ('A' - 1 + tile);
        	}
        }
    }
}
