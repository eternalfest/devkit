package net.eternalfest.hfestlib.models.sprites;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.ImageObserver;

import net.eternalfest.hfestlib.models.Level;
import net.eternalfest.hfestlib.utils.Lazy;


public class TileType implements IAsset, IThumbnail {

    public static final TileType DEFAULT = unknownWith("<default-tile>");
    public static final TileType UNKNOWN = unknownWith("<unknown-tile>");

    public static TileType unknownWith(String id) {
        return new TileType(Lazy.fromValue(null), Lazy.fromValue(null), "Plateformes inconnues", id);
    }

    public static final int TILES_SIZE = Level.TILES_SIZE;

    private final String name;
    private final String ID;
    private final Lazy<Image> bodyImage;
    private final Lazy<Image> endImage;

    public TileType(Lazy<Image> bodyImage, Lazy<Image> endImage, String name, String ID) {
        this.name = IAsset.validateString(name);
        this.ID = IAsset.validateString(ID);
        this.bodyImage = bodyImage;
        this.endImage = endImage;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public void drawThumbnail(Graphics2D g, int width, int height, ImageObserver obs) {
        int y = (height - TILES_SIZE) / 2;
        int l = (int) Math.floor(width / (double) TILES_SIZE);
        draw(g, (width % TILES_SIZE) / 2, y, l, obs);
    }

    public void draw(Graphics2D g, int posX, int posY, int length, ImageObserver obs) {
        Image body = bodyImage.get();
        Image end = endImage.get();
        int endWidth = end == null ? 0 : end.getWidth(obs);
        int bodyWidth = TILES_SIZE * length - endWidth;
        int bodyHeight = body == null ? 0 : body.getHeight(obs);

        if (body == null) {
            g.setColor(new Color(178, 178, 178));
            g.fillRect(posX, posY, bodyWidth, TILES_SIZE);
        } else {
            g.drawImage(body, posX, posY, posX + bodyWidth, posY + bodyHeight, 0, 0, bodyWidth, bodyHeight, obs);
        }

        if (end != null) {
            g.drawImage(end, posX + bodyWidth, posY, obs);
        }
    }
}
