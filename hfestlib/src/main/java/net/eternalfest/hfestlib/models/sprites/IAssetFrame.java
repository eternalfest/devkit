package net.eternalfest.hfestlib.models.sprites;

import java.awt.Graphics2D;
import java.awt.image.ImageObserver;

public interface IAssetFrame extends IAsset, IThumbnail {
    public int getWidth();

    public int getHeight();

    public int getOffsetX();

    public int getOffsetY();

    public void draw(Graphics2D g, ImageObserver obs);
}
