package net.eternalfest.hfestlib.models.sprites;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.ImageObserver;

import net.eternalfest.hfestlib.utils.Lazy;

public class FieldVortex extends FieldType {

    public FieldVortex(Lazy<Image> image, String name, String ID) {
        super(image, name, ID);
    }

    @Override
    public void draw(Graphics2D g, int posX, int posY, int length, boolean flipped, ImageObserver obs) {
        if (length < 1)
            length = 1;
        Image img = image.get();

        if (img == null) {
            super.draw(g, posX, posY, length, flipped, obs);
        } else {
            int w = img.getWidth(null), h = img.getHeight(null);
            if (flipped) {
                g.drawImage(img, posX, posY - h + TILES_SIZE, posX + length * TILES_SIZE, posY + TILES_SIZE, w, h, 0, 0, obs);

            } else {
                g.drawImage(img, posX, posY, posX + length * TILES_SIZE, posY + h, 0, 0, w, h, obs);
            }
        }
    }

}
