package net.eternalfest.hfestlib.models.sprites;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.image.ImageObserver;
import java.util.Objects;

import net.eternalfest.hfestlib.utils.Lazy;


public class BackgroundType implements IAsset, IThumbnail {

    public static final BackgroundType DEFAULT = unknownWith("<default-bg>");
    public final static BackgroundType UNKNOWN = unknownWith("<unknown-bg>");

    public static BackgroundType unknownWith(String id) {
        return new BackgroundType(Lazy.fromValue(null), "Fond inconnu", id);
    }

    private final String name;
    private final String ID;
    private final Lazy<Image> image;

    public BackgroundType(Lazy<Image> image, String name, String ID) {
        this.name = IAsset.validateString(name);
        this.ID = IAsset.validateString(ID);
        this.image = image;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public void drawThumbnail(Graphics2D g, int width, int height, ImageObserver obs) {
        AffineTransform saveAT = g.getTransform();
        Shape saveClip = g.getClip();
        g.clip(new Rectangle(0, 0, width, height));
        g.scale(width / (double) 128, height / (double) 128);
        draw(g, 0, 0, obs);
        g.setTransform(saveAT);
        g.setClip(saveClip);
    }

    public void draw(Graphics2D g, int offX, int offY, ImageObserver obs) {
        Image img = image.get();
        Rectangle clip = g.getClipBounds();
        Objects.requireNonNull(clip, "Cannot draw background without explicit clipping area");

        if (img == null) {
            g.setColor(new Color(192, 192, 192));
            g.fill(clip);
        } else {
        	int width = img.getWidth(obs), height = img.getHeight(obs);
        	int xmin = offX + Math.floorDiv(clip.x - offX, width) * width;
        	int ymin = offY + Math.floorDiv(clip.y - offY, height) * height;
        	int xmax = clip.x + clip.width, ymax = clip.y + clip.height;

            for (int i = xmin; i < xmax; i += width) {
                for (int j = ymin; j < ymax; j += height)
                    g.drawImage(img, i, j, obs);
            }
        }
    }
}
