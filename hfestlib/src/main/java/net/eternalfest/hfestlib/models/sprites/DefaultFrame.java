package net.eternalfest.hfestlib.models.sprites;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.image.ImageObserver;

import net.eternalfest.hfestlib.models.Level;

public class DefaultFrame implements IAssetFrame {
    public static final DefaultFrame DEFAULT = with("<unknown-frame>");

    public static DefaultFrame with(String id) {
        return new DefaultFrame("Image inconnue", id);
    }

    private static final Stroke STROKE = new BasicStroke(2);

    private final String name;
    private final String ID;

    public DefaultFrame(String name, String ID) {
        this.name = name;
        this.ID = ID;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public int getWidth() {
        return Level.TILES_SIZE;
    }

    @Override
    public int getHeight() {
        return Level.TILES_SIZE;
    }

    @Override
    public int getOffsetX() {
        return 0;
    }

    @Override
    public int getOffsetY() {
        return 0;
    }

    @Override
    public void draw(Graphics2D g, ImageObserver obs) {
        g.setColor(Color.RED);
        g.setStroke(STROKE);
        g.drawRect(-getOffsetX(), -getOffsetY(), getWidth(), getHeight());
    }

    @Override
    public void drawThumbnail(Graphics2D g, int width, int height, ImageObserver obs) {
        g.setColor(Color.RED);
        g.setStroke(STROKE);
        g.drawLine(2, 2, width - 2, height - 2);
        g.drawLine(2, height - 2, width - 2, 2);
    }
}