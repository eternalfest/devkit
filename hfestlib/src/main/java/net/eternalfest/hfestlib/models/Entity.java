package net.eternalfest.hfestlib.models;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.image.ImageObserver;
import java.util.Objects;

import net.eternalfest.hfestlib.data.DataObject;
import net.eternalfest.hfestlib.models.sprites.IAssetFrame;
import net.eternalfest.hfestlib.models.sprites.Sprite;

public class Entity {

    private final Sprite sprite;
    private LevelTiles tiles;
    private TileListener listener;

    private int orientation = 0;
    private int calculatedOrientation = 0;
    private double x = 0;
    private double y = 0;
    private IAssetFrame frame;
    private AffineTransform cachedTransform;
    private Rectangle cachedBoundingBox;

    public Entity(Sprite sprite) {
        this.sprite = sprite;
        this.frame = sprite.getDefaultFrame();
    }

    private Entity(Entity entity) {
        this.sprite = entity.sprite;
        this.frame = entity.frame;
        this.orientation = entity.orientation;
        this.x = entity.x;
        this.y = entity.y;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getPixelX() {
        return (int) (x * Level.TILES_SIZE);
    }

    public int getPixelY() {
        return (int) (y * Level.TILES_SIZE);
    }

    public void setPos(double posX, double posY) {
        boolean newTile = (int) posX != (int) x || (int) posY != (int) y;
        x = posX;
        y = posY;
        if(newTile)
            calculate();
        invalidate();
    }

    public void setPixelPos(int posX, int posY) {
        setPos(posX / (double) Level.TILES_SIZE, posY / (double) Level.TILES_SIZE);
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation & Sprite.ORIENTATION_MASK;
        invalidate();
    }

    public void cycleOrientation() {
        this.orientation = sprite.cycleOrientation(orientation);
        invalidate();
    }

    public IAssetFrame getFrame() {
        return frame;
    }

    public void setFrame(IAssetFrame frame) {
        Objects.requireNonNull(frame);
        if(this.frame != frame) {
            this.frame = frame;
            invalidate();
        }
    }

    public DataObject getDataObject() {
        DataObject obj = new DataObject();

        obj.put("x", getPixelX() + Level.TILES_SIZE / 2);
        obj.put("y", getPixelY() + Level.TILES_SIZE);
        obj.put("name", sprite.getID());
        return obj;
    }

    public void attachToLevel(Level lvl) {
        if((sprite.getCapabilities() & Sprite.AUTO_ORIENT_MASK) != 0) {
            if(listener == null)
                listener = new TileListener();

            if(lvl == null) {
                if(tiles != null)
                    tiles.removeListener(listener);
            } else if(lvl.getTiles() != tiles) {
                if(tiles != null)
                    tiles.removeListener(listener);
                tiles = lvl.getTiles();
                tiles.addListener(listener);
                calculate();
            }
        }
    }

    private void calculate() {
        if(tiles != null) {
            boolean orientationModified = this.calculatedOrientation != this.orientation;
            this.calculatedOrientation = sprite.calculateOrientation(orientation, tiles, (int) x, (int) y);
            if(!orientationModified) {
                this.orientation = this.calculatedOrientation;
                invalidate();
            }
        }
    }

    private AffineTransform getTransform() {
        if(cachedTransform != null)
            return cachedTransform;

        int rotation = orientation & Sprite.ROTATION_MASK;
        boolean flipH = (orientation & Sprite.FLIP_H) != 0;
        boolean flipV = (orientation & Sprite.FLIP_V) != 0;

        IAssetFrame frame = getFrame();
        AffineTransform transform = new AffineTransform();

        transform.translate(x * Level.TILES_SIZE, y * Level.TILES_SIZE);

        int autoOrient = sprite.getCapabilities() & Sprite.AUTO_ORIENT_MASK;
        if(autoOrient == Sprite.AUTO_ORIENT_ATTACH_FLIP) {
            if(rotation == Sprite.ROTATION_LEFT) {
                rotation = Sprite.ROTATION_RIGHT;
                flipV = !flipV;
            } else if(rotation == Sprite.ROTATION_DOWN) {
                rotation = Sprite.ROTATION_UP;
                flipV = !flipV;
            }
        } else if(autoOrient == Sprite.AUTO_ORIENT_ATTACH_NOFLIP) {
            int offset = Level.TILES_SIZE - frame.getHeight() + 2 * frame.getOffsetY();
            if(rotation == Sprite.ROTATION_LEFT) {
                rotation = Sprite.ROTATION_RIGHT;
                transform.translate(-offset, 0);
            } else if(rotation == Sprite.ROTATION_DOWN) {
                rotation = Sprite.ROTATION_UP;
                transform.translate(0, offset);
            }
        }

        switch (rotation) {
            case Sprite.ROTATION_RIGHT:
                transform.translate(Level.TILES_SIZE, 0);
                transform.rotate(Math.PI / 2);
                break;

            case Sprite.ROTATION_DOWN:
                transform.rotate(Math.PI, Level.TILES_SIZE / 2, Level.TILES_SIZE / 2);
                break;

            case Sprite.ROTATION_LEFT:
                transform.translate(0, Level.TILES_SIZE);
                transform.rotate(3 * Math.PI / 2);
                break;
        }

        if (flipH) {
            transform.translate(frame.getWidth() - 2 * frame.getOffsetX(), 0);
            transform.scale(-1.0, 1.0);
        }
        if (flipV) {
            transform.translate(0, frame.getHeight() + 2 * frame.getOffsetY());
            transform.scale(1.0, -1.0);
        }

        cachedTransform = transform;
        return transform;
    }

    private void invalidate() {
        cachedTransform = null;
        cachedBoundingBox = null;
    }

    public void draw(Graphics2D g, ImageObserver obs) {
        AffineTransform saveAT = g.getTransform();
        g.transform(getTransform());
        getFrame().draw(g, obs);
        g.setTransform(saveAT);
    }

    public Rectangle getBoundingBox() {
        if(cachedBoundingBox != null)
            return cachedBoundingBox;

        IAssetFrame frame = getFrame();
        AffineTransform transform = getTransform();
        Point p1 = new Point(-frame.getOffsetX(), -frame.getOffsetY());
        Point p2 = new Point(p1.x + frame.getWidth(), p1.y + frame.getHeight());
        transform.transform(p1, p1);
        transform.transform(p2, p2);

        cachedBoundingBox = new Rectangle(
                Math.min(p1.x, p2.x), Math.min(p1.y, p2.y),
                Math.abs(p1.x - p2.x), Math.abs(p1.y - p2.y)
            );
        return cachedBoundingBox;
    }

    private class TileListener implements LevelTiles.Listener {
        @Override
        public void tilesChanged(LevelTiles tiles) {
            calculate();
        }

        @Override
        public void skinChanged(LevelTiles tiles) {
            // Do nothing
        }
    }

    public Entity copy() {
        return new Entity(this);
    }

}
