package net.eternalfest.hfestlib.models.sprites;

import java.awt.Graphics2D;
import java.awt.image.ImageObserver;

@FunctionalInterface
public interface IThumbnail {
    public void drawThumbnail(Graphics2D g, int width, int height, ImageObserver obs);
}
