package net.eternalfest.hfestlib.models;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import net.eternalfest.hfestlib.models.sprites.Sprite;

public enum LevelLayer {
    BACKGROUND,

    PLAYER_SPAWN, // Cannot contain more than one entity
    SPEC_ITEM_SPAWN,
    SCORE_ITEM_SPAWN,

    FRUITS_PIC,
    FRUITS_ORDER,
    FRUITS_OTHER,

    FOREGROUND;

    public static final List<LevelLayer> FRUITS_LAYERS = Collections.unmodifiableList(Arrays.asList(
            FRUITS_PIC, FRUITS_ORDER, FRUITS_OTHER
        ));

    public static final List<LevelLayer> SPRITES_LAYERS = Collections.unmodifiableList(Arrays.asList(
            BACKGROUND, FOREGROUND
        ));

    public static class Pos {
        public final LevelLayer layer;
        public final int index;

        public Pos(LevelLayer layer, int index) {
            Objects.requireNonNull(layer);
            if(index < 0)
                throw new IllegalArgumentException("index must be positive or null");
            this.layer = layer;
            this.index = index;
        }
    }

    private static final String PIC_NAME = "fruit_pic";

    public static LevelLayer preferredFruitLayerFor(Sprite sprite) {
        if (PIC_NAME.equals(sprite.getID())) {
            return LevelLayer.FRUITS_PIC;
        } else if (sprite.hasCapability(Sprite.TYPE_FRUIT_ORDER)) {
            return LevelLayer.FRUITS_ORDER;
        }
        return LevelLayer.FRUITS_OTHER;
    }

    public static LevelLayer preferredSpriteLayerFor(Sprite sprite) {
        if(sprite.hasCapability(Sprite.DEFAULT_LAYER_FRONT))
            return FOREGROUND;
        return BACKGROUND;
    }

    public Pos atIndex(int index) {
        return new Pos(this, index);
    }
}
