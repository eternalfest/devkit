package net.eternalfest.hfestlib.utils;

import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ArgumentsParser {

    public static class ActionArgs {
        public final List<String> positional;
        public final Map<String, String> named;

        private ActionArgs(List<String> pos, Map<String, String> name) {
            this.positional = pos;
            this.named = name;
        }

        public String get(int idx) {
            return positional.get(idx);
        }

        public String get(String name) {
            return named.get(name);
        }
    }

    public static interface Action {
        boolean doAction(ActionArgs args);
    }

    private final String name;
    private final Map<String, ActionInfo> actions = new HashMap<>();
    private ActionInfo defaultAction = null;
    private ActionInfo parseErrorAction = null;
    private final List<ActionInfo> actionsList = new ArrayList<>();
    private final Map<String, String> defaultNamedArgs = new HashMap<>();
    private String argPrefix = "--";
    private boolean fallbackToDefaultActionIfUnknown = false;

    public ArgumentsParser(Class<?> entryPoint) {
        String name;
        try {
            URI uri = entryPoint.getProtectionDomain().getCodeSource().getLocation().toURI();
            Path path = Paths.get(uri);
            name = path.getName(path.getNameCount()-1).toString();
        } catch(URISyntaxException e) {
            e.printStackTrace();
            name = "<unknown>";
        }
        this.name = name;
    }

    public ArgumentsParser(String name) {
        this.name = name;
    }

    public void setArgumentsPrefix(String argPrefix) {
        this.argPrefix = argPrefix;
    }

    public void setFallbackToDefaultActionIfUnkwown(boolean value) {
        this.fallbackToDefaultActionIfUnknown = value;
    }

    public void setDefaultNamedArg(String name, String value) {
        defaultNamedArgs.put(name, value);
    }

    public void addAction(String name, Action action, String... help) {
        addAction(name, new String[]{}, action, help);
    }

    public void addAction(String name, String arg, Action action, String... help) {
        addAction(name, new String[]{ arg }, action, help);
    }

    public void addAction(String name, String arg1, String arg2, Action action, String... help) {
        addAction(name, new String[]{ arg1, arg2 }, action, help);
    }

    public void addNamedArg(String actionName, String argName, boolean acceptValue) {
        Objects.requireNonNull(argName);
        ActionInfo action = actionName == null ? defaultAction : actions.get(actionName);
        if(action == null)
            throw new IllegalArgumentException("the action " + actionName + " doesn't exist!");
        action.namedArgs.put(argName, acceptValue);
    }

    public void addAction(String name,  String[] args, Action action, String[] help) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(action);
        if(actions.containsKey(name))
            throw new IllegalArgumentException("the action " + name + " already exists!");
        ActionInfo info = new ActionInfo(name, args, action, help);
        actions.put(name, info);
        actionsList.add(info);
    }

    public void setDefaultAction(Action action, String... help) {
        setDefaultAction(new String[]{}, action, help);
    }

    public void setDefaultAction(String[] args, Action action, String... help) {
        Objects.requireNonNull(action);
        if(defaultAction != null)
            throw new IllegalArgumentException("the default action is already set!");
        defaultAction = new ActionInfo(null, args, action, help);
        actionsList.add(0, defaultAction);
    }

    public void setParseErrorAction(Action action) {
        Objects.requireNonNull(action);
        if(parseErrorAction != null)
            throw new IllegalArgumentException("the error action is already set!");
        parseErrorAction = new ActionInfo(null, new String[]{}, action, new String[]{});
    }

    public void addHelpAndVersionActions(String version) {
        addAction("help", args -> {
            printUsage(System.out);
            return true;
        });

        addAction("version", args -> {
            System.out.println("Current version : " + version);
            return true;
        });

        if(parseErrorAction == null) {
            setParseErrorAction(args -> {
                System.err.println("Invalid arguments.");
                printUsage(System.err);
                return false;
            });
        }
    }

    public void printUsage(PrintStream stream) {
        stream.println("Utilisation :");
        for(ActionInfo action: actionsList) {
            stream.print("java -jar " + name);
            if(action.name != null)
                stream.print(" " + argPrefix + action.name);
            for(String arg: action.args)
                stream.print(" [" + arg + "]");
            for(Map.Entry<String, Boolean> arg: action.namedArgs.entrySet())
                stream.print(" [" + argPrefix + arg.getKey() + (arg.getValue() ? " <" + arg.getKey() + ">]" : "]"));
            stream.println();
            for(String help: action.help) {
                stream.print("\t");
                stream.println(help);
            }
        }
    }

    public boolean parse(String[] arguments) {
        int offset = 0;
        ActionInfo action = null;
        List<String> args = Arrays.asList(arguments);

        if(args.size() == 0 || !args.get(0).startsWith(argPrefix)) {
            action = defaultAction;
        } else {
            action = actions.get(args.get(offset++).substring(argPrefix.length()));
            if(action == null && fallbackToDefaultActionIfUnknown) {
                action = defaultAction;
                offset--;
            }
        }

        Map<String, String> namedArgs = new HashMap<>(defaultNamedArgs);

        if(action != null) {
            args = new ArrayList<>(args.subList(offset, args.size()));

            int i = 0;
            while (i < args.size()) {
                if(args.get(i).startsWith(argPrefix)) { // named arg
                    String name = args.remove(i).substring(argPrefix.length());
                    Boolean acceptValue = action.namedArgs.get(name);
                    if(acceptValue == null) // the named argument doesn't exist
                        return onParseError(arguments, namedArgs);

                    if(acceptValue && i < args.size() && !args.get(i).startsWith(argPrefix))
                        namedArgs.put(name, args.remove(i));
                    else namedArgs.put(name, "");

                } else {
                    // positional arg
                    i++;
                }
            }

            if(args.size() == action.args.length)
                return action.action.doAction(new ActionArgs(args, new HashMap<>(namedArgs)));
        }

        return onParseError(arguments, namedArgs);
    }

    private boolean onParseError(String[] positional, Map<String, String> named) {
        if(parseErrorAction != null)
            return parseErrorAction.action.doAction(new ActionArgs(Arrays.asList(positional), named));
        return false;
    }

    private static class ActionInfo {
        public final String name;
        public final String[] help;
        public final String[] args;
        public final Map<String, Boolean> namedArgs;
        public final Action action;

        public ActionInfo(String name,  String[] args, Action action, String[] help) {
            this.name = name;
            this.help = help;
            this.args = args;
            this.namedArgs = new HashMap<>();
            this.action = action;
        }

    }
}
