package net.eternalfest.hfestlib.utils;

import java.util.Objects;
import java.util.function.Supplier;

public class Lazy<T> implements Supplier<T> {
    private T value = null;
    private Supplier<T> supplier = null;

    private Lazy() {
    }

    public static <T> Lazy<T> from(Supplier<T> supplier) {
        Lazy<T> lazy = new Lazy<>();
        Objects.requireNonNull(supplier);
        lazy.supplier = supplier;
        return lazy;
    }

    public static <T> Lazy<T> fromValue(T value) {
        Lazy<T> lazy = new Lazy<>();
        lazy.value = value;
        return lazy;
    }

    @Override
    public T get() {
        if (supplier != null) {
            value = supplier.get();
            supplier = null;
        }
        return value;
    }
}
