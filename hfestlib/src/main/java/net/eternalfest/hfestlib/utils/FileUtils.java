package net.eternalfest.hfestlib.utils;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import net.eternalfest.hfestlib.tools.FileNotExistingException;
import net.eternalfest.hfestlib.tools.FileNotWritableException;
import net.eternalfest.hfestlib.tools.FileUnknownException;
import net.eternalfest.hfestlib.tools.ShowableException;
import net.eternalfest.hfestlib.tools.XMLMalformedException;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public enum FileUtils {
    ;

    // Look for a file named 'name' by going back up the file hierarchy.
    // This will look for /foo/bar/<name>, then /foo/<name>, then /<name>.
    // If no file was found, or if the file found was not readable, return nothing.
    public static Optional<Path> findInAncestors(Path folder, String... names) {
        do {
        	for (String name: names) {
	            Path current = folder.resolve(name);
	            if (Files.exists(current)) {
	                return Optional.of(current).filter(Files::isReadable);
	            }
        	}

            folder = folder.getParent();
        } while (folder != null);

        return Optional.empty();
    }

    public static Image loadImage(Path path) throws ShowableException {
        if (!Files.isReadable(path))
            throw new FileNotExistingException(path);

        Image img;
        try {
            img = ImageIO.read(path.toFile());

            if (img.getHeight(null) < 0) {
                throw new FileUnknownException(path);
            }

            return img;
        } catch (IOException e) {
            throw new FileUnknownException(path);
        }
    }

    public static Image loadImageResource(String path) {
        URL u = FileUtils.class.getResource(normalizeResourcePath(path));
        if (u == null)
            throw new RuntimeException("Couldn't load resource " + path);
        return new ImageIcon(u).getImage();
    }

    public static Document loadXMLResource(String path) {
    	InputStream in = FileUtils.class.getResourceAsStream(normalizeResourcePath(path));
        SAXBuilder builder = new SAXBuilder();
        try {
			return builder.build(in);
		} catch (JDOMException | IOException e) {
            throw new RuntimeException("Couldn't load resource " + path, e);
		}
    }

    private static String normalizeResourcePath(String path) {
        if (!path.startsWith("/"))
            path = "/" + path;
        return "/net/eternalfest/hfestlib" + path;
    }

    public static void saveImage(Path path, BufferedImage image) {
        try {
            ImageIO.write(image, "png", path.toFile());
        } catch (IOException e) {
        }
    }

    public static Document loadXML(Path path) throws ShowableException {
        try {
            SAXBuilder builder = new SAXBuilder();
            return builder.build(path.toFile());
        } catch (IOException e) {
            throw new FileNotExistingException(path);
        } catch (JDOMException e) {
            throw new XMLMalformedException(path.toString(), e);
        }
    }

    public static Document loadXML(InputStream in) throws ShowableException {
        SAXBuilder builder = new SAXBuilder();
        try {
            return builder.build(in);
        } catch (JDOMException e) {
            throw new XMLMalformedException("<stream>", e);
        } catch (IOException e) {
            throw new ShowableException(e);
        }
    }

    public static void saveXML(Path path, Document doc) throws ShowableException {
        FileUtils.saveXML(path, doc, Format.getPrettyFormat());
    }

    public static void saveXML(Path path, Document doc, Format format) throws ShowableException {
        XMLOutputter output = new XMLOutputter(format);
        try {
            FileOutputStream outStream = new FileOutputStream(path.toFile());
            output.output(doc, outStream);
        } catch (IOException e) {
            throw new FileNotWritableException(path);
        }
    }

    public static void copyFolder(Path path, Path dest) throws ShowableException {
        copyFolder(path.toFile(), dest.toFile());
    }

    private static void copyFolder(File path, File dest) throws ShowableException {
        if (path.equals(dest))
            return;

        if (!path.exists())
            throw new FileNotExistingException(path.toPath());

        File[] listDest = dest.listFiles(), listSource = path.listFiles();
        List<File> listOriginalFiles = new ArrayList<File>(), listSourceFiles = new ArrayList<File>();
        if (listDest != null)
            listOriginalFiles = Arrays.asList(listDest);

        if (listSource != null)
            listSourceFiles = Arrays.asList(listSource);

        if (!dest.exists())
            dest.mkdir();

        for (File file : listSourceFiles) {
            File destFile = new File(dest, file.getName());
            if (file.isFile())
                FileUtils.copyFile(file.toPath(), destFile.toPath());
            else if (file.isDirectory())
                copyFolder(file, destFile);

            listOriginalFiles.remove(destFile);
        }

        for (File file : listOriginalFiles)
            file.delete();
    }

    public static void copyFile(Path source, Path dest) throws ShowableException {
        try {
            Files.copy(source, dest);
        } catch (FileNotFoundException e) {
            throw new FileNotExistingException(source);
        } catch (IOException e) {
            throw new FileNotWritableException(dest);
        }
    }

    public static List<String> loadFileLines(Path file) throws ShowableException {
        try {
            return Files.readAllLines(file);
        } catch (FileNotFoundException e) {
            throw new FileNotExistingException(file);
        } catch (IOException e) {
            throw new FileUnknownException(file);
        }
    }

    public static String loadFile(Path file) throws ShowableException {
        StringBuffer str = new StringBuffer();
        try (BufferedReader reader = Files.newBufferedReader(file)) {

            String line = reader.readLine();
            while (line != null) {
                str.append(line);
                str.append('\n');
                line = reader.readLine();
            }


        } catch (FileNotFoundException e) {
            throw new FileNotExistingException(file);
        } catch (IOException e) {
            throw new FileUnknownException(file);
        }

        return str.toString();
    }

    public static Path getFileWithExtension(Path file, String extension) {
        String name = file.getFileName().toString();
        if (name.endsWith("." + extension))
            return file;
        else if (name.endsWith("."))
            return file.resolveSibling(name + extension);
        else return file.resolveSibling(name + "." + extension);
    }
}
