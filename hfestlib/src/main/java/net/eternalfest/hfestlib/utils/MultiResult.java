package net.eternalfest.hfestlib.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;

public class MultiResult<T, E> {
    private final T value;
    private final ArrayList<E> errors;

    private MultiResult(T value, ArrayList<E> errors) {
        this.value = value;
        this.errors = errors;
    }

    public static <T, E> MultiResult<T, E> of(T value) {
        return new MultiResult<>(value, new ArrayList<>());
    }

    public static <T, E> MultiResult<T, E> withError(T value, E error) {
        return MultiResult.<T, E>of(value).addError(error);
    }

    public static <T, E> MultiResult<T, E> withErrors(T value, ArrayList<E> errors) {
        return new MultiResult<>(value, errors);
    }

    public T value() {
        return value;
    }

    public List<E> errors() {
        return Collections.unmodifiableList(errors);
    }

    public MultiResult<T, E> addError(E error) {
        errors.add(error);
        return this;
    }

    public MultiResult<T, E> addErrors(Collection<E> errors) {
        this.errors.addAll(errors);
        return this;
    }

    public <U, R> MultiResult<R, E> merge(MultiResult<U, E> other, BiFunction<T, U, R> merger) {
        return MultiResult.<R, E>of(merger.apply(value, other.value))
            .addErrors(errors)
            .addErrors(other.errors);
    }

    public <U, V, R> MultiResult<R, E> merge(MultiResult<U, E> other1, MultiResult<V, E> other2, TriFunction<T, U, V, R> merger) {
        return MultiResult.<R, E>of(merger.apply(value, other1.value, other2.value))
            .addErrors(errors)
            .addErrors(other2.errors)
            .addErrors(other1.errors);
    }


}
