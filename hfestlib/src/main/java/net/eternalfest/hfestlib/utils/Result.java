package net.eternalfest.hfestlib.utils;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public abstract class Result<T, E> {

    @FunctionalInterface
    public static interface Faillible<T, E extends Exception> {
        public T get() throws E;
    }

    private Result() {
    }

    public static <T, E> Result<T, E> ok(T value) {
        return new Success<>(value);
    }

    public static <T, E> Result<T, E> fail(E error) {
        return new Failure<>(error);
    }

    @SuppressWarnings("unchecked")
    public static <T, E extends Exception> Result<T, E> catchError(Class<E> exception, Faillible<? extends T, ? extends E> op) {
        try {
            return Result.ok(op.get());
        } catch(Exception e) {
            if(exception.isInstance(e))
                return Result.fail((E) e);
            Utils.sneakyThrow(e);
            return null; // unreachable
        }
    }

    public abstract boolean isSuccess();

    public abstract boolean isFailure();

    public abstract boolean ifSuccess(Consumer<T> consumer);

    public abstract boolean ifFailure(Consumer<E> consumer);

    public abstract T get();


    public abstract T getOrElse(Function<? super E, ? extends T> mapper);

    public abstract <E1 extends Exception> T getOrThrow(Function<? super E, ? extends E1> mapper) throws E1;

    public abstract E getError();

    public abstract Optional<E> consume(Consumer<? super T> consumer);

    public abstract Optional<T> consumeError(Consumer<? super E> errorConsumer);

    public abstract <U> Result<U, E> map(Function<? super T, ? extends U> mapper);

    public abstract <U> Result<U, E> flatMap(Function<? super T, Result<U, E>> mapper);

    public abstract <F> Result<T, F> mapError(Function<? super E, ? extends F> mapper);

    public abstract <F> Result<T, F> flatMapError(Function<? super E, Result<T, F>> mapper);

    public abstract <U, F> Result<U, F> biMap(Function<? super T, ? extends U> mapper,
                                    Function<? super E, ? extends F> errorMapper);

    public abstract <U> U biFlatMap(Function<? super T, ? extends U> mapper,
                                    Function<? super E, ? extends U> errorMapper);

    private static final class Success<T, E> extends Result<T, E> {
        T value;

        Success(T value) {
            this.value = Objects.requireNonNull(value);
        }

        @Override
        public boolean isSuccess() {
            return true;
        }

        @Override
        public boolean isFailure() {
            return false;
        }

        @Override
        public boolean ifSuccess(Consumer<T> consumer) {
            consumer.accept(value);
            return true;
        }

        @Override
        public boolean ifFailure(Consumer<E> consumer) {
            return false;
        }

        @Override
        public T get() {
            return value;
        }

        @Override
        public T getOrElse(Function<? super E, ? extends T> mapper) {
            return value;
        }

        @Override
        public <E1 extends Exception> T getOrThrow(Function<? super E, ? extends E1> mapper) throws E1 {
            return value;
        }

        @Override
        public E getError() {
            throw new NoSuchElementException("this Result contains a value");
        }

        @Override
        public Optional<E> consume(Consumer<? super T> consumer) {
            consumer.accept(value);
            return Optional.empty();
        }

        @Override
        public Optional<T> consumeError(Consumer<? super E> errorConsumer) {
            return Optional.ofNullable(value);
        }

        @Override
        public <U> Result<U, E> map(Function<? super T, ? extends U> mapper) {
            return new Success<>(mapper.apply(value));
        }

        @Override
        public <U> Result<U, E> flatMap(Function<? super T, Result<U, E>> mapper) {
            return mapper.apply(value);
        }

        @SuppressWarnings("unchecked")
        @Override
        public <F> Result<T, F> mapError(Function<? super E, ? extends F> mapper) {
            return (Result<T, F>) this;
        }

        @SuppressWarnings("unchecked")
        @Override
        public <F> Result<T, F> flatMapError(Function<? super E, Result<T, F>> mapper) {
            return (Result<T, F>) this;
        }

        @SuppressWarnings("unchecked")
        @Override
        public <U, F> Result<U, F> biMap(Function<? super T, ? extends U> mapper,
                                       Function<? super E, ? extends F> errorMapper) {
            return (Result<U, F>) map(mapper);
        }

        @Override
        public <U> U biFlatMap(Function<? super T, ? extends U> mapper,
                                    Function<? super E, ? extends U> errorMapper) {
            return mapper.apply(value);
        }
    }

    private static final class Failure<T, E> extends Result<T, E> {
        E error;

        Failure(E error) {
            this.error = Objects.requireNonNull(error);
        }

        @Override
        public boolean isSuccess() {
            return false;
        }

        @Override
        public boolean isFailure() {
            return true;
        }

        @Override
        public boolean ifSuccess(Consumer<T> consumer) {
            return false;
        }

        @Override
        public boolean ifFailure(Consumer<E> consumer) {
            consumer.accept(error);
            return true;
        }

        @Override
        public T get() {
            throw new NoSuchElementException("this Result contains a error");
        }

        @Override
        public T getOrElse(Function<? super E, ? extends T> mapper) {
            return mapper.apply(error);
        }

        @Override
        public <E1 extends Exception> T getOrThrow(Function<? super E, ? extends E1> mapper) throws E1 {
            throw mapper.apply(error);
        }

        @Override
        public E getError() {
            return error;
        }

        @Override
        public Optional<E> consume(Consumer<? super T> consumer) {
            return Optional.of(error);
        }

        @Override
        public Optional<T> consumeError(Consumer<? super E> errorConsumer) {
            errorConsumer.accept(error);
            return Optional.empty();
        }

        @SuppressWarnings("unchecked")
        @Override
        public <U> Result<U, E> map(Function<? super T, ? extends U> mapper) {
            return (Result<U, E>) this;
        }

        @SuppressWarnings("unchecked")
        @Override
        public <U> Result<U, E> flatMap(Function<? super T, Result<U, E>> mapper) {
            return (Result<U, E>) this;
        }

        @Override
        public <F> Result<T, F> mapError(Function<? super E, ? extends F> mapper) {
            return new Failure<>(mapper.apply(error));
        }

        @Override
        public <F> Result<T, F> flatMapError(Function<? super E, Result<T, F>> mapper) {
            return mapper.apply(error);
        }

        @SuppressWarnings("unchecked")
        @Override
        public <U, F> Result<U, F> biMap(Function<? super T, ? extends U> mapper,
                                       Function<? super E, ? extends F> errorMapper) {
            return (Result<U, F>) mapError(errorMapper);
        }

        @Override
        public <U> U biFlatMap(Function<? super T, ? extends U> mapper,
                                    Function<? super E, ? extends U> errorMapper) {
            return errorMapper.apply(error);
        }
    }
}
