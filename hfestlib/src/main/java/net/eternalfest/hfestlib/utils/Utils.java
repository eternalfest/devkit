package net.eternalfest.hfestlib.utils;

import java.util.Arrays;

public enum Utils {
    ;

    @SafeVarargs
    public static <T> T[] arrayOf(T... objects) {
        return objects;
    }

    @SafeVarargs
    public static <T> T[] arrayConcat(T[] first, T[]... rest) {
        int totalLen = first.length;
        for (T[] a : rest)
            totalLen += a.length;

        T[] res = Arrays.copyOf(first, totalLen);
        int offset = first.length;
        for (T[] a : rest) {
            System.arraycopy(a, 0, res, offset, a.length);
            offset += a.length;
        }

        return res;
    }

    @SuppressWarnings("unchecked")
    public static <T extends Throwable> RuntimeException sneakyThrow(Throwable t) throws T {
        throw (T) t;
    }

    public static int parseInt(String str) {
        return parseInt(str, 0);
    }

    public static int parseInt(String str, int defaultValue) {
        if (str == null)
            return defaultValue;
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static double parseDouble(String str) {
        return parseDouble(str, Double.NaN);
    }

    public static double parseDouble(String str, double defaultValue) {
        if (str == null)
            return defaultValue;
        try {
            double d = Double.parseDouble(str);
            return d;
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }
}
