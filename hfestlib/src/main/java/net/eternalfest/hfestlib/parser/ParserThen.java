package net.eternalfest.hfestlib.parser;

import java.util.function.BiFunction;

import net.eternalfest.hfestlib.utils.Result;
import net.eternalfest.hfestlib.utils.Utils;

final class ParserThen<T, R> implements Parser<T, R> {

    Parser<T, Object>[] parsers;
    BiFunction<Object, Object, Object>[] combiners;

    @SuppressWarnings("unchecked")
    public <R0, RR0> ParserThen(Parser<T, R0> a,
                                Parser<T, RR0> b,
                                BiFunction<? super R0, ? super RR0, R> combiner) {
        if(a instanceof ParserThen) {
            ParserThen<T, Object> pA = (ParserThen<T, Object>) a;
            parsers = pA.parsers;
            combiners = pA.combiners;
        } else {
            parsers = Utils.arrayOf((Parser<T, Object>) a);
            combiners = Utils.arrayOf();
        }

        parsers = Utils.arrayConcat(parsers, Utils.arrayOf((Parser<T, Object>) b));
        combiners = Utils.arrayConcat(combiners, Utils.arrayOf((BiFunction<Object, Object, Object>) combiner));
    }

    //TODO : what to do if a parser can accept 0 tokens ?
    @Override
    @SuppressWarnings("unchecked")
    public Result<R, Parser.Error<T>> parse(ParserCursor<T> cursor) {
        Result<?, Parser.Error<T>> res = parsers[0].parse(cursor);

        if (res.isFailure())
            return (Result<R, Parser.Error<T>>) res;

        Object r = res.get();
        for (int i = 1; i < parsers.length; i++) {
            res = parsers[i].parse(cursor);
            if (res.isSuccess())
                r = combiners[i - 1].apply(r, res.get());

            else return (Result<R, Parser.Error<T>>) res.mapError(e -> e.setHasConsumed(true));
        }

        return Result.ok((R) r);
    }


}
