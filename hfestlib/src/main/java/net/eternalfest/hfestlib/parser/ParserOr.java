package net.eternalfest.hfestlib.parser;


import net.eternalfest.hfestlib.utils.Result;
import net.eternalfest.hfestlib.utils.Utils;

final class ParserOr<T, R> implements Parser<T, R> {

    private Parser<T, ? extends R>[] parsers;

    public ParserOr(Parser<T, ? extends R> a, Parser<T, ? extends R> b) {
        boolean isAOr = a instanceof ParserOr;
        boolean isBOr = b instanceof ParserOr;

        Parser<T, ? extends R>[] parsersA = isAOr ? ((ParserOr<T, ? extends R>) a).parsers : Utils.arrayOf(a);
        Parser<T, ? extends R>[] parsersB = isBOr ? ((ParserOr<T, ? extends R>) b).parsers : Utils.arrayOf(b);

        parsers = Utils.arrayConcat(parsersA, parsersB);
    }

    @Override
    public Result<R, Parser.Error<T>> parse(ParserCursor<T> cursor) {
        for (Parser<T, ? extends R> p : parsers) {
            @SuppressWarnings("unchecked")
            Result<R, Parser.Error<T>> res = (Result<R, Parser.Error<T>>) p.parse(cursor);
            if (res.isSuccess() || res.getError().hasConsumed())
                return res;
        }

        return Result.fail(new Parser.Error<>(cursor.peek(), "Unexpected token", false));
    }
}
