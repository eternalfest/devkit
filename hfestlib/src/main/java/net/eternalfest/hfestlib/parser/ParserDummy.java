package net.eternalfest.hfestlib.parser;

import net.eternalfest.hfestlib.utils.Result;

class ParserDummy<T, R> implements Parser.Dummy<T, R> {

    private static final String NOT_SET_ERROR = "This parser must be set before any use";
    private static final String ALREADY_SET_ERROR = "This parser has already been set";

    Parser<T, ? extends R> real = null;

    public ParserDummy() {
    }

    @SuppressWarnings("unchecked")
    @Override
    public Result<R, Parser.Error<T>> parse(ParserCursor<T> cursor) {
        if (real == null)
            throw new IllegalStateException(NOT_SET_ERROR);
        return (Result<R, Parser.Error<T>>) real.parse(cursor);
    }

    @Override
    public void set(Parser<T, ? extends R> parser) {
        if (real != null)
            throw new IllegalStateException(ALREADY_SET_ERROR);
        real = parser;
    }

}
