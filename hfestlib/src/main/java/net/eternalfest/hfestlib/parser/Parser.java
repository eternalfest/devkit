package net.eternalfest.hfestlib.parser;

import java.util.List;
import java.util.Optional;
import java.util.Stack;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import net.eternalfest.hfestlib.utils.Result;
import net.eternalfest.hfestlib.utils.TriFunction;

public interface Parser<T, R> {
    //T : token type
    //R : result type

    /*------------*
     *-INTERFACES-*
     *------------*/

    public static <T> Parser<T, T> of(Predicate<T> matcher) {
        return of(matcher, Function.identity());
    }

    public static <T, R> Parser<T, R> of(Predicate<T> matcher, Function<T, ? extends R> mapper) {
        return new Parser<T, R>() {
            @Override
            public Result<R, Error<T>> parse(ParserCursor<T> cursor) {
                if (!matcher.test(cursor.peek()))
                    return Result.fail(new Error<>(cursor.peek(), "Unexpected token", false));
                return Result.ok(mapper.apply(cursor.next()));
            }
        };
    }

    @SafeVarargs
    public static <T> Parser<T, Object> lookAhead(Predicate<T>... matchers) {
        Object dummy = new Object();
        return new Parser<T, Object>() {
            @Override
            public Result<Object, Error<T>> parse(ParserCursor<T> cursor) {
                Result<Object, Error<T>> result = Result.ok(dummy);
                Stack<T> seen = new Stack<>();
                for(int i = 0; i < matchers.length; i++) {
                    T tok = cursor.next();
                    seen.push(tok);
                    if(!matchers[i].test(tok)) {
                        result = Result.fail(new Error<>(tok, "Unexpected token", false));
                        break;
                    }
                }
                while(!seen.empty())
                    cursor.undo(seen.pop());
                return result;
            }
        };
    }

    public static <T, R> Dummy<T, R> dummy() {
        return new ParserDummy<>();
    }

    /*-----------*
     *--METHODS--*
     *-----------*/

    @SuppressWarnings("unchecked")
    public static <T, R> Parser<T, R> cast(Parser<T, ? extends R> parser, Class<R> resultClass) {
        return (Parser<T, R>) parser;
    }

    public abstract Result<R, Error<T>> parse(ParserCursor<T> cursor);

    public default <E extends Exception> R parse(ParserCursor<T> tokens, ExceptionFactory<T, E> e) throws E {
        return this.parse(tokens).getOrThrow(error -> e.get(error));
    }

    /*-----------*
     *-FACTORIES-*
     *-----------*/

    public default <E extends Exception> R parse(Supplier<T> tokens, ExceptionFactory<T, E> e) throws E {
        return this.parse(new ParserCursor<>(tokens), e);
    }

    public default Parser<T, Void> ignore() {
        return new Parser<T, Void>() {
            @Override
            public Result<Void, Error<T>> parse(ParserCursor<T> cursor) {
                return Parser.this.parse(cursor).map(r -> null);
            }
        };
    }

    public default <U> Parser<T, U> map(Function<? super R, ? extends U> mapper) {
        return new Parser<T, U>() {
            @Override
            public Result<U, Error<T>> parse(ParserCursor<T> cursor) {
                return Parser.this.parse(cursor).map(mapper);
            }
        };
    }

    public default Parser<T, R> or(Parser<T, ? extends R> other) {
        return new ParserOr<>(this, other);
    }

    public default <RR> Parser<T, R> thenIgnore(Parser<T, RR> other) {
        return then(other, (a, b) -> a);
    }

    public default <RR> Parser<T, RR> ignoreThen(Parser<T, RR> other) {
        return then(other, (a, b) -> b);
    }

    public default <R2, R3> Parser<T, R3> then(Parser<T, ? extends R2> other,
                                               BiFunction<? super R, ? super R2, R3> combiner) {
        return new ParserThen<>(this, other, combiner);
    }

    @SuppressWarnings("unchecked")
    public default <R2, R3, R4> Parser<T, R4> then(
            Parser<T, ? extends R2> other1, Parser<T, ? extends R3> other2,
            TriFunction<? super R, ? super R2, ? super R3, R4> combiner) {
        return then(other1, (r1, r2) -> new Object[] {r1, r2})
            .then(other2, (rs, r3) -> combiner.apply((R) rs[0], (R2) rs[1], r3));
    }

    public default Parser<T, Optional<R>> maybe() {
        return new Parser<T, Optional<R>>() {
            @Override
            public Result<Optional<R>, Error<T>> parse(ParserCursor<T> cursor) {
                return Parser.this.parse(cursor)
                    .map(Optional::of)
                    .flatMapError(e -> e.hasConsumed() ? Result.fail(e) : Result.ok(Optional.empty()));
            }
        };
    }

    public default Parser<T, List<R>> zeroOrMore() {
        return list(true, -1, null);
    }

    public default Parser<T, List<R>> oneOrMore() {
        return list(false, -1, null);
    }

    public default Parser<T, List<R>> list(Parser<T, ?> separator) {
        return list(true, -1, separator);
    }

    public default Parser<T, List<R>> nonEmptyList(Parser<T, ?> separator) {
        return list(false, -1, separator);
    }

    public default Parser<T, List<R>> list(boolean canBeEmpty, int max, Parser<T, ?> separator) {
        return new ParserList<>(this, canBeEmpty, max, separator);
    }

    //This interface is used to generate an exception when a parsing error occurs
    @FunctionalInterface
    public static interface ExceptionFactory<T, E extends Exception> {
        public E get(Error<T> error);
    }

    //The factory Parser::dummy returns a parser implementing this interface
    public static interface Dummy<T, R> extends Parser<T, R> {
        //Set the dummy to a real parser ; can be called only once
        public void set(Parser<T, ? extends R> parser);
    }

    public static final class Error<T> {
        private String m;
        private T t;
        private boolean b;

        public Error(T token, String msg, boolean hasConsumed) {
            t = token;
            m = msg;
            b = hasConsumed;
        }

        public T getToken() {
            return t;
        }

        public String getMessage() {
            return m;
        }

        public boolean hasConsumed() {
            return b;
        }

        public Error<T> setHasConsumed(boolean v) {
            b = v;
            return this;
        }
    }
}
