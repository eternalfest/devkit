package net.eternalfest.hfestlib.parser;

import java.util.Stack;
import java.util.function.Supplier;

public class ParserCursor<T> {

    Supplier<T> tokens;

    private Stack<T> next = new Stack<>();

    public ParserCursor(Supplier<T> tokens) {
        this.tokens = tokens;
    }

    public T next() {
        return next.empty() ? tokens.get() : next.pop();
    }

    public T peek() {
        if(!next.empty())
           return next.peek();

        T t = tokens.get();
        next.push(t);
        return t;
    }

    public void undo(T tok) {
        next.push(tok);
    }
}
