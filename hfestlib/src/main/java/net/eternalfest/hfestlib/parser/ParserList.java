package net.eternalfest.hfestlib.parser;

import net.eternalfest.hfestlib.utils.Result;

import java.util.ArrayList;
import java.util.List;

class ParserList<T, R> implements Parser<T, List<R>> {

    Parser<T, R> parser;
    int max;
    boolean canBeEmpty;
    Parser<T, ?> separator;

    public ParserList(Parser<T, R> parser, boolean canBeEmpty, int max, Parser<T, ?> separator) {
        this.parser = parser;
        this.max = max < 0 ? Integer.MAX_VALUE : max;
        this.canBeEmpty = canBeEmpty;
        this.separator = separator;
    }

    @Override
    public Result<List<R>, Parser.Error<T>> parse(ParserCursor<T> cursor) {
        Result<R, Parser.Error<T>> res = parser.parse(cursor);

        if (res.isFailure())
            return canBeEmpty ? Result.ok(new ArrayList<>()) : res.map(null);

        List<R> list = new ArrayList<>();
        res.ifSuccess(r -> list.add(r));


        if (separator == null) {
            while (list.size() < max) {
                res = parser.parse(cursor);
                if (!res.ifSuccess(r -> list.add(r))) {
                    if (res.getError().hasConsumed())
                        return res.map(null);
                    break;
                }
            }

        } else {
            while (list.size() < max) {
                Result<?, Parser.Error<T>> sepRes = separator.parse(cursor);
                if (sepRes.isFailure()) {
                    if (sepRes.getError().hasConsumed())
                        return sepRes.map(null);
                    break;
                }

                res = parser.parse(cursor);
                if (!res.ifSuccess(r -> list.add(r)))
                    return Result.fail(res.getError().setHasConsumed(true));

            }
        }


        return Result.ok(list);
    }
}
