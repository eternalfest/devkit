package net.eternalfest.hfestlib.data;

public class JSONParseException extends Exception {

    private static final long serialVersionUID = 188348522369347281L;

    public JSONParseException(String msg) {
        super(msg);
    }
}
