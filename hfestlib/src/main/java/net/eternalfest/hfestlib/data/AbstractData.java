package net.eternalfest.hfestlib.data;

import java.util.Optional;

public interface AbstractData {

    public default Optional<Integer> asInt() {
        return this.as(DataInt.class).map(e -> e.get());
    }

    public default Optional<Double> asDouble() {
        return this.as(DataDouble.class).map(e -> e.get());
    }

    public default Optional<Boolean> asBool() {
        return this.as(DataBool.class).map(e -> e.get());
    }

    public default Optional<String> asString() {
        return this.as(DataString.class).map(e -> e.get());
    }

    @SuppressWarnings("unchecked")
    public default <T extends AbstractData> Optional<T> as(Class<T> type) {
        return type.isInstance(this) ? Optional.of((T) this) : Optional.empty();
    }

    public default <T extends AbstractData> boolean is(Class<T> type) {
        return type.isInstance(this);
    }

    public AbstractData deepCopy();
}
