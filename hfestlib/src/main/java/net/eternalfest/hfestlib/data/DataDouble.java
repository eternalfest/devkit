package net.eternalfest.hfestlib.data;

public class DataDouble implements AbstractData {

    private double v;

    private DataDouble(double value) {
        v = value;
    }

    public static DataDouble of(double v) {
        return new DataDouble(v);
    }

    public double get() {
        return v;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof DataDouble) && ((DataDouble) o).v == v;
    }

    @Override
    public int hashCode() {
        return Double.hashCode(v);
    }


    @Override
    public String toString() {
        return "" + v;
    }

    @Override
    public DataDouble deepCopy() {
        return this;
    }
}
