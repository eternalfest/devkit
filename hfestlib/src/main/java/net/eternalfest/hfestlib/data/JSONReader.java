package net.eternalfest.hfestlib.data;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public class JSONReader implements AutoCloseable {
    private static int STRING_LEN = 15;
    private Reader reader;
    private boolean isFirstRead = true;
    private int nextChar;
    private boolean isNewLine = false;
    private int lineNumber = 1;
    private StringBuilder strBuilder = new StringBuilder();
    private boolean allowComments = false;
    private boolean allowNaNAndInfinities = false;

    public JSONReader(Reader reader, Params params) {
        if (params == null)
            params = Params.DEFAULT;

        this.reader = reader;
        allowComments = params.allowComments;
        allowNaNAndInfinities = params.allowNaNAndInfinities;
    }

    public JSONReader(Reader reader) {
        this(reader, null);
    }

    public static Params params() {
        return new Params();
    }

    public static AbstractData fromString(String str, Params params) throws JSONParseException {
        try (JSONReader r = new JSONReader(new StringReader(str), params)) {
            return r.read();
        } catch (IOException e) {
            return null;
        }
    }

    public static AbstractData fromString(String str) throws JSONParseException {
        return fromString(str, null);
    }

    private int next() throws IOException, JSONParseException {
        if (nextChar < 0)
            throw new JSONParseException("Unexpected end of stream");

        int c = nextChar;
        nextChar = reader.read();

        if (isNewLine) {
            lineNumber++;
            isNewLine = false;
        }
        if (c == '\n')
            isNewLine = true;

        if (Character.isHighSurrogate((char) nextChar)) {
            int low = reader.read();
            if (low < 0)
                throw new JSONParseException("Unexpected end of stream");
            nextChar = nextChar << 16 + low;
        }

        return c;
    }

    private String getNextChars() throws IOException {
        int c = nextChar;
        if (c < 0) return "";


        StringBuilder str = new StringBuilder(STRING_LEN);

        if (Character.isSupplementaryCodePoint(c)) {
            str.append(c >> 16);
            c = c & (1 << 16);
        }


        do {
            if (Character.isISOControl(c))
                c = ' ';

            str.append((char) c);
            c = reader.read();

        } while (c >= 0 && str.length() < STRING_LEN);

        if (Character.isHighSurrogate((char) c))
            str.deleteCharAt(str.length() - 1);

        return str.toString();
    }

    private String getCharAsString(int c) {
        return new String(Character.toChars(nextChar));
    }

    private void skipSpaces() throws IOException, JSONParseException {
        while (true) {
            if (allowComments & nextChar == '/') {
                next();
                if (nextChar == '/')
                    skipComment();
                else if (nextChar == '*')
                    skipMultiComment();
                else verify(null);

            } else if (!Character.isWhitespace(nextChar))
                break;

            next();
        }
    }

    private void skipComment() throws IOException, JSONParseException {
        expect('/');
        while (nextChar != '\n' && nextChar != '\r' && nextChar > 0)
            next();
    }

    private void skipMultiComment() throws IOException, JSONParseException {
        boolean foundStar = false;

        expect('*');
        while (!foundStar || nextChar != '/') {
            foundStar = nextChar == '*';
            next();
        }

        next();
    }

    private void expect(int c) throws JSONParseException, IOException {
        if (nextChar != c)
            throw getError("Expected '" + (char) c + "', found " +
                (nextChar < 0 ?
                    "end of file"
                    : ("'" + getCharAsString(nextChar) + "'")));
        next();
    }

    private void expect(String str) throws JSONParseException, IOException {
        for (int i = 0; i < str.length(); i++) {
            if (nextChar != str.charAt(i)) {
                String bad = (i > 1 ? str.substring(0, i - 1) : "") + getCharAsString(nextChar);
                throw getError("Expected '" + str + "', found '" + bad + "'");
            }

            next();
        }
    }

    private JSONParseException getError(String msg) throws IOException {
        msg = "Line " + lineNumber + ": " + msg;
        if (nextChar >= 0)
            msg += " at '" + getNextChars() + "...'";

        return new JSONParseException(msg);
    }

    private <T> T verify(T data) throws IOException, JSONParseException {
        if (data == null)
            throw getError("Unexpected character '" + getCharAsString(nextChar) + "'");
        return data;
    }

    private void accumulate(char c) {
        strBuilder.append(c);
    }

    private String getString() {
        String str = strBuilder.toString();
        strBuilder.delete(0, strBuilder.length());
        return str;
    }

    public AbstractData read() throws IOException, JSONParseException {
        if (isFirstRead) {
            nextChar = reader.read();
            isFirstRead = false;
        }

        return verify(readData());
    }

    private AbstractData readData() throws IOException, JSONParseException {
        skipSpaces();

        if (nextChar == '{')
            return readObject();

        if (nextChar == '[')
            return readArray();

        if (nextChar == '"')
            return DataString.of(readString());

        if ((nextChar >= '0' && nextChar <= '9') || nextChar == '-' || nextChar == '+' ||
            (allowNaNAndInfinities && (nextChar == 'N' || nextChar == 'I')))
            return readNumber();

        if (nextChar == 't')
            return readTrue();

        if (nextChar == 'f')
            return readFalse();

        if (nextChar == 'n')
            return readNull();

        return null;
    }

    private AbstractData readArray() throws JSONParseException, IOException {
        DataArray array = new DataArray();

        expect('[');
        skipSpaces();
        if (nextChar == ']') {
            next();
            return array;
        }


        array.add(verify(readData()));
        skipSpaces();

        while (nextChar != ']') {
            expect(',');
            skipSpaces();
            array.add(verify(readData()));
            skipSpaces();
        }

        expect(']');

        return array;
    }

    private void readEntry(DataObject obj) throws JSONParseException, IOException {
        skipSpaces();
        String key = verify(readString());

        if (obj.containsKey(key))
            throw getError("Duplicate key '" + key + "'");

        skipSpaces();
        expect(':');
        skipSpaces();
        AbstractData value = verify(readData());

        //System.out.println("Added entry " + key + ": " + value);

        obj.put(key, value);
    }

    private AbstractData readObject() throws JSONParseException, IOException {
        DataObject obj = new DataObject();

        expect('{');
        skipSpaces();
        if (nextChar == '}') {
            next();
            return obj;
        }

        readEntry(obj);
        skipSpaces();

        while (nextChar != '}') {
            expect(',');
            skipSpaces();
            readEntry(obj);
            skipSpaces();
        }

        expect('}');

        return obj;
    }

    private AbstractData readNumber() throws JSONParseException, IOException {
        boolean isNegative = false;

        if (nextChar == 'N') {
            expect("NaN");
            return DataDouble.of(Double.NaN);
        }

        if (nextChar == '-') {
            isNegative = true;
            next();
        }

        if (nextChar == 'I') {
            expect("Infinity");
            return DataDouble.of(isNegative ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY);
        }

        if (isNegative)
            accumulate('-');

        boolean isDouble = false;
        while (true) {
            if (nextChar == '.' || nextChar == 'e' || nextChar == 'E' || nextChar == '-' || nextChar == '+')
                isDouble = true;
            else if (nextChar < '0' || nextChar > '9')
                break;

            accumulate((char) next());
        }

        String res = getString();
        if (!isDouble) {
            try {
                return DataInt.of(Integer.parseInt(res));
            } catch (NumberFormatException e) {
                //On ne fait rien, on essayera de le parser comme double
            }
        }

        try {
            return DataDouble.of(Double.parseDouble(res));
        } catch (NumberFormatException e) {
            throw getError(e.getMessage());
        }
    }

    private String readString() throws JSONParseException, IOException {
        expect('"');

        while (nextChar != '"') {
            if (nextChar == '\\') {
                char c = 0;
                next();
                switch (nextChar) {
                    case '\\':
                    case '"':
                        c = (char) nextChar;
                        break;

                    case 'b':
                        c = '\b';
                        break;

                    case 't':
                        c = '\t';
                        break;

                    case 'f':
                        c = '\f';
                        break;

                    case 'n':
                        c = '\n';
                        break;

                    case 'r':
                        c = '\r';
                        break;

                    case 'u':
                        next();
                        for (int i = 0; i < 4; i++) {
                            if (nextChar >= '0' && nextChar <= '9')
                                c = (char) (16 * c + nextChar - '0');

                            else if (nextChar < 'A' || nextChar > 'F')
                                c = (char) (16 * c + nextChar - 'A');

                            else if (nextChar < 'a' || nextChar > 'f')
                                c = (char) (16 * c + nextChar - 'a');

                            else throw getError("Expected hex digit, found '" + getCharAsString(nextChar) + "'");
                        }
                        break;

                    default:
                        throw getError("Illegal escape sequence '\\" + getCharAsString(nextChar) + "'");
                }


                accumulate(c);
                next();


            } else {
                if (Character.isSupplementaryCodePoint(nextChar)) {
                    accumulate((char) (nextChar >> 16));
                    accumulate((char) nextChar);
                } else accumulate((char) nextChar);
                next();
            }
        }

        expect('"');
        return getString();
    }

    private AbstractData readNull() throws JSONParseException, IOException {
        expect("null");
        return DataNull.NULL;
    }

    private AbstractData readFalse() throws JSONParseException, IOException {
        expect("false");
        return DataBool.FALSE;
    }

    private AbstractData readTrue() throws JSONParseException, IOException {
        expect("true");
        return DataBool.TRUE;
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }

    public static class Params {
        private static final Params DEFAULT = new Params();
        private boolean allowComments = false;
        private boolean allowNaNAndInfinities = false;

        private Params() {
        }

        public Params allowComments(boolean v) {
            allowComments = v;
            return this;
        }

        public Params allowNaNAndInfinities(boolean v) {
            allowNaNAndInfinities = v;
            return this;
        }
    }
}
