package net.eternalfest.hfestlib.data;

import java.util.*;
import java.util.stream.Collectors;

public class DataArray extends AbstractContainer implements List<AbstractData> {

    private List<AbstractData> list;

    public DataArray() {
        list = new ArrayList<>();
    }

    public DataArray(List<AbstractData> l) {
        list = new ArrayList<>(l);
    }

    public DataArray(AbstractData... tab) {
        list = new ArrayList<>(Arrays.asList(tab));
    }

    public DataArray(int... tab) {
        list = Arrays.stream(tab)
            .mapToObj(DataInt::of)
            .collect(Collectors.toCollection(ArrayList::new));
    }

    public DataArray(byte... tab) {
        list = new ArrayList<>(tab.length);
        for (int i = 0; i < tab.length; i++)
            list.add(DataInt.of(tab[i]));
    }

    public DataArray(boolean... tab) {
        list = new ArrayList<>(tab.length);
        for (int i = 0; i < tab.length; i++)
            list.add(DataBool.of(tab[i]));
    }

    public DataArray(double... tab) {
        list = Arrays.stream(tab)
            .mapToObj(DataDouble::of)
            .collect(Collectors.toCollection(ArrayList::new));
    }

    public DataArray(String... tab) {
        list = Arrays.stream(tab)
            .map(DataString::of)
            .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public AbstractData set(int index, AbstractData value) {
        Objects.requireNonNull(value);
        return list.set(index, value);
    }

    public AbstractData set(int index, int value) {
        return list.set(index, DataInt.of(value));
    }

    public AbstractData set(int index, double value) {
        return list.set(index, DataDouble.of(value));
    }

    public AbstractData set(int index, boolean value) {
        return list.set(index, DataBool.of(value));
    }

    public AbstractData set(int index, String value) {
        return list.set(index, DataString.of(value));
    }

    @Override
    public boolean add(AbstractData value) {
        Objects.requireNonNull(value);
        return list.add(value);
    }

    public boolean add(int value) {
        return list.add(DataInt.of(value));
    }

    public boolean add(double value) {
        return list.add(DataDouble.of(value));
    }

    public boolean add(boolean value) {
        return list.add(DataBool.of(value));
    }

    public boolean add(String value) {
        return list.add(DataString.of(value));
    }


    @Override
    public AbstractData get(int index) {
        return list.get(index);
    }

    public Optional<AbstractData> getOpt(int index) {
        return (index < 0 && index >= list.size()) ?
            Optional.empty() :
            Optional.of(list.get(index));
    }

    public <T extends AbstractData> Optional<T> get(int index, Class<T> type) {
        return (index < 0 && index >= list.size()) ?
            Optional.empty() :
            list.get(index).as(type);
    }

    @Override
    public AbstractData remove(int index) {
        return list.remove(index);
    }

    @Override
    public boolean remove(Object o) {
        return list.remove(o);
    }

    @Override
    public Iterator<AbstractData> iterator() {
        return list.iterator();
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return list.contains(o);
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return list.toArray(a);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return list.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends AbstractData> c) {
        return list.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends AbstractData> c) {
        return list.addAll(index, c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return list.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return list.retainAll(c);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public void add(int index, AbstractData element) {
        list.add(index, element);
    }

    @Override
    public int indexOf(Object o) {
        return list.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return list.lastIndexOf(o);
    }

    @Override
    public ListIterator<AbstractData> listIterator() {
        return list.listIterator();
    }

    @Override
    public ListIterator<AbstractData> listIterator(int index) {
        return list.listIterator(index);
    }

    @Override
    public List<AbstractData> subList(int fromIndex, int toIndex) {
        return list.subList(fromIndex, toIndex);
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();

        str.append('[');

        Iterator<AbstractData> iterator = iterator();
        while (iterator.hasNext()) {
            str.append(iterator.next());
            str.append(", ");
        }

        if (!list.isEmpty()) {
            str.deleteCharAt(str.length() - 1);
            str.deleteCharAt(str.length() - 1);
        }

        str.append(']');

        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof DataArray && list.equals(((DataArray) o).list);
    }

    @Override
    public int hashCode() {
        return list.hashCode();
    }


    @Override
    public DataArray deepCopy() {
        DataArray array = new DataArray();
        array.list = list.stream()
            .map(AbstractData::deepCopy)
            .collect(Collectors.toCollection(ArrayList::new));
        return array;
    }
}
