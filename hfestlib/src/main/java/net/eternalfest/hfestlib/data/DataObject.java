package net.eternalfest.hfestlib.data;

import java.util.*;

public class DataObject extends AbstractContainer implements Map<String, AbstractData> {

    private Map<String, AbstractData> list = new HashMap<String, AbstractData>();

    public DataObject() {
        super();
    }

    public DataObject(Map<String, AbstractData> mappings) {
        this();
        list.putAll(mappings);
    }

    @Override
    public AbstractData put(String key, AbstractData value) {
        Objects.requireNonNull(value);
        return list.put(key, value);
    }

    public AbstractData put(String key, int value) {
        return put(key, DataInt.of(value));
    }

    public AbstractData put(String key, double value) {
        return put(key, DataDouble.of(value));
    }

    public AbstractData put(String key, boolean value) {
        return put(key, DataBool.of(value));
    }

    public AbstractData put(String key, String value) {
        return put(key, DataString.of(value));
    }

    @Override
    public AbstractData get(Object key) {
        return list.get(key);
    }

    public Optional<AbstractData> getOpt(Object key) {
        return Optional.ofNullable(list.get(key));
    }

    public <T extends AbstractData> Optional<T> get(Object key, Class<T> type) {
        AbstractData res = list.get(key);
        return res == null ? Optional.empty() : res.as(type);
    }

    @Override
    public AbstractData remove(Object key) {
        return list.remove(key);
    }

    @Override
    public boolean containsKey(Object key) {
        return list.containsKey(key);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean containsValue(Object value) {
        return list.containsValue(value);
    }

    @Override
    public void putAll(Map<? extends String, ? extends AbstractData> m) {
        list.putAll(m);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public Set<String> keySet() {
        return list.keySet();
    }

    @Override
    public Collection<AbstractData> values() {
        return list.values();
    }

    @Override
    public Set<Entry<String, AbstractData>> entrySet() {
        return list.entrySet();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof DataObject && list.equals(((DataObject) o).list);
    }

    @Override
    public int hashCode() {
        return list.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();

        str.append('{');
        for (Entry<String, AbstractData> e : entrySet()) {
            str.append('"');
            str.append(e.getKey());
            str.append("\":");
            str.append(e.getValue().toString());
            str.append(", ");
        }

        if (size() > 0) {
            str.deleteCharAt(str.length() - 1);
            str.deleteCharAt(str.length() - 1);
        }

        str.append("}");

        return str.toString();
    }

    @Override
    public DataObject deepCopy() {
        DataObject obj = new DataObject();

        for (Entry<String, AbstractData> e : list.entrySet())
            obj.put(e.getKey(), e.getValue().deepCopy());

        return obj;
    }
}


