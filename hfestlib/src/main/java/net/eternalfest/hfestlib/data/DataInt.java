package net.eternalfest.hfestlib.data;

import java.util.stream.IntStream;


public class DataInt implements AbstractData {

    private static final int CACHE_LOW = -15;
    private static final int CACHE_HIGH = 15;
    private static final DataInt[] cache = IntStream.rangeClosed(CACHE_LOW, CACHE_HIGH)
        .mapToObj(DataInt::new)
        .toArray(DataInt[]::new);

    private int v;

    private DataInt(int value) {
        v = value;
    }

    public static DataInt of(int value) {
        if (value >= CACHE_LOW && value <= CACHE_HIGH)
            return cache[value - CACHE_LOW];

        return new DataInt(value);
    }

    public int get() {
        return v;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof DataInt) && ((DataInt) o).v == v;
    }

    @Override
    public int hashCode() {
        return v;
    }

    @Override
    public String toString() {
        return "" + v;
    }

    @Override
    public DataInt deepCopy() {
        return this;
    }

}
