package net.eternalfest.hfestlib.data;

public enum DataNull implements AbstractData {
    NULL;

    @Override
    public DataNull deepCopy() {
        return this;
    }

    @Override
    public String toString() {
        return "null";
    }
}
