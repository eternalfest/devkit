package net.eternalfest.hfestlib.data;

public class DataString implements AbstractData {

    private String v;

    private DataString(String s) {
        v = s == null ? "" : s;
    }

    public static DataString of(String s) {
        return new DataString(s);
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof DataString) && ((DataString) o).v.equals(v);
    }

    @Override
    public int hashCode() {
        return v.hashCode();
    }

    public String get() {
        return v;
    }

    @Override
    public String toString() {
        return '"' + v + '"';
    }

    @Override
    public DataString deepCopy() {
        return this;
    }
}
