package net.eternalfest.hfestlib.data;

public enum DataBool implements AbstractData {
    TRUE, FALSE;

    public static DataBool of(boolean b) {
        return b ? TRUE : FALSE;
    }

    public boolean get() {
        return this == TRUE;
    }

    @Override
    public String toString() {
        return Boolean.toString(get());
    }

    @Override
    public DataBool deepCopy() {
        return this;
    }

}
