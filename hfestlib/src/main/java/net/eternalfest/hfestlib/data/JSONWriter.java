package net.eternalfest.hfestlib.data;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map.Entry;

public class JSONWriter implements AutoCloseable {

    private Writer writer;
    private boolean printGlobalSeparator = false;
    private boolean prettyPrinting = false;
    private boolean printNaNAndInfinities = false;
    private int indentSize;
    private boolean inlineObject;
    private boolean inlineArray;
    private int curIndent = 0;

    public JSONWriter(Writer writer, Params params) {
        if (params == null)
            params = Params.DEFAULT;

        this.writer = writer;
        prettyPrinting = params.prettyPrinting;
        printNaNAndInfinities = params.printNaNAndInfinities;
        indentSize = params.indentSize;
        inlineObject = params.inlineObject;
        inlineArray = params.inlineArray;
    }

    public JSONWriter(Writer writer) {
        this(writer, null);
    }

    public static Params params() {
        return new Params();
    }

    public static String toString(AbstractData data, Params params) {
        StringWriter str = new StringWriter();
        try (JSONWriter writer = new JSONWriter(str, params)) {
            writer.write(data);
            writer.close();
            return str.toString();
        } catch (IOException e) {
            throw new RuntimeException("This cannot happen", e);
        }
    }

    public static String toString(AbstractData data) {
        return toString(data, null);
    }

    public static String toPrettyString(AbstractData data) {
        return toString(data, JSONWriter.params().setPrettyPrinting(true));
    }

    public void write(AbstractData data) throws IOException {
        init();
        writeData(data);

        if (printGlobalSeparator)
            writer.write('\n');

        printGlobalSeparator = true;
    }

    private void init() {
        curIndent = 0;
    }

    private void indent() {
        if (indentSize < 0)
            curIndent++;
        else curIndent += indentSize;
    }

    private void dedent() {
        if (indentSize < 0)
            curIndent--;
        else curIndent -= indentSize;
    }

    private void writeIndent() throws IOException {
        writer.write('\n');
        if (indentSize >= 0) {
            for (int i = curIndent; i > 0; i--)
                writer.write(' ');

        } else {
            for (int i = curIndent; i > 0; i--)
                writer.write('\t');
        }
    }

    private void writeData(AbstractData data) throws IOException {
        if (data.is(DataObject.class))
            writeObject((DataObject) data);

        else if (data.is(DataArray.class))
            writeArray((DataArray) data);

        else if (data.is(DataBool.class))
            writeBool(((DataBool) data).get());

        else if (data.is(DataDouble.class))
            writeDouble(((DataDouble) data).get());

        else if (data.is(DataInt.class))
            writeInt(((DataInt) data).get());

        else if (data.is(DataString.class))
            writeString(((DataString) data).get());

        else if (data.is(DataNull.class))
            writeNull();

        else throw new IllegalArgumentException("Unknown object type " + data.getClass().getCanonicalName());
    }

    private void writeObject(DataObject data) throws IOException {
        boolean separator = false;

        if (prettyPrinting && data.isEmpty()) {
            writer.write("{}");
            return;
        }

        boolean inline = inlineObject && !data.values().stream().anyMatch(e -> e.is(AbstractContainer.class));

        writer.write('{');

        if (prettyPrinting && !inline) {
            indent();
            writeIndent();
        }

        for (Entry<String, AbstractData> e : data.entrySet()) {
            if (separator)
                writeSeparator(inline);

            writeString(e.getKey());
            writer.write(':');

            if (prettyPrinting)
                writer.write(' ');

            writeData(e.getValue());

            separator = true;
        }

        if (prettyPrinting && !inline) {
            dedent();
            writeIndent();
        }

        writer.write('}');

    }

    private void writeArray(DataArray data) throws IOException {
        boolean separator = false;

        if (prettyPrinting && data.isEmpty()) {
            writer.write("[]");
            return;
        }

        boolean inline = inlineArray && !data.stream().anyMatch(e -> e.is(AbstractContainer.class));

        writer.write('[');

        if (prettyPrinting && !inline) {
            indent();
            writeIndent();
        }

        for (AbstractData e : data) {
            if (separator)
                writeSeparator(inline);

            writeData(e);

            separator = true;
        }

        if (prettyPrinting && !inline) {
            dedent();
            writeIndent();
        }

        writer.write(']');
    }

    private void writeSeparator(boolean inline) throws IOException {
        writer.write(',');

        if (!prettyPrinting)
            return;

        if (inline)
            writer.write(' ');
        else writeIndent();
    }

    private void writeBool(boolean data) throws IOException {
        writer.write(data ? "true" : "false");
    }

    private void writeDouble(double data) throws IOException {
        if (!this.printNaNAndInfinities) {
            writer.write(Double.toString(data));
            return;
        }

        if (Double.isNaN(data))
            writer.write("null");

        else if (data == Double.POSITIVE_INFINITY)
            writer.write("1e9999");

        else if (data == Double.NEGATIVE_INFINITY)
            writer.write("-1e9999");

        else writer.write(Double.toString(data));
    }

    private void writeInt(int data) throws IOException {
        writer.write(Integer.toString(data));
    }

    private void writeString(String data) throws IOException {
        writer.write('"');

        for (int i = 0, l = data.length(); i < l; i++) {
            char c = data.charAt(i);
            switch (c) {
                case '"':
                    writer.write("\\\"");
                    break;

                case '\\':
                    writer.write("\\\\");
                    break;

                case '\b':
                    writer.write("\\b");
                    break;

                case '\t':
                    writer.write("\\t");
                    break;

                case '\f':
                    writer.write("\\f");
                    break;

                case '\n':
                    writer.write("\\n");
                    break;

                case '\r':
                    writer.write("\\r");
                    break;

                default:
                    if (Character.isHighSurrogate(c) && i < l - 1) {
                        char low = data.charAt(++i);

                        if (Character.isISOControl(c << 16 + low)) {
                            writeEscapedChar(c);
                            writeEscapedChar(low);
                        } else {
                            writer.write(c);
                            writer.write(low);
                        }

                    } else if (Character.isISOControl(c))
                        writeEscapedChar(c);

                    else writer.write(c);
            }
        }

        writer.write('"');
    }

    private void writeEscapedChar(char c) throws IOException {
        writer.write("\\u");
        String res = Integer.toHexString(c);

        for (int i = res.length(); i < 4; i++)
            writer.write('0');

        writer.write(res);

    }

    private void writeNull() throws IOException {
        writer.write("null");
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }

    public void flush() throws IOException {
        writer.flush();
    }

    public static class Params {
        private static final Params DEFAULT = new Params();
        private boolean prettyPrinting = false;
        private boolean printNaNAndInfinities = false;

        //Toutes ces variables n'ont d'effet que si prettyPrinting = true
        private int indentSize = 4; //-1 = tabulation
        private boolean inlineObject = false;
        private boolean inlineArray = false;

        private Params() {
        }

        public Params setPrettyPrinting(boolean v) {
            if (prettyPrinting == v)
                return this;

            //defaults
            indentSize = 4;
            inlineObject = false;
            inlineArray = false;

            prettyPrinting = v;
            return this;
        }

        public Params setIndentSize(int size) {
            if (!prettyPrinting)
                badConfig();

            indentSize = size < 0 ? -1 : size;
            return this;
        }

        public Params setInlineObject(boolean v) {
            if (!prettyPrinting)
                badConfig();

            inlineObject = v;
            return this;
        }

        public Params setInlineArray(boolean v) {
            if (!prettyPrinting)
                badConfig();

            inlineArray = v;
            return this;
        }

        public Params setPrintNaNAndInfinities(boolean v) {
            this.printNaNAndInfinities = v;
            return this;
        }

        private void badConfig() {
            throw new IllegalArgumentException("Pretty printing isn't enabled !");
        }
    }
}
