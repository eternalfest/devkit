package net.eternalfest.hfestlib.data;

public abstract class AbstractContainer implements AbstractData {
    private boolean added = false;

    protected void addedTo(AbstractData root) {
        if (added)
            throw new IllegalArgumentException("Cannot share containers between objects !");
        added = true;
    }
}
