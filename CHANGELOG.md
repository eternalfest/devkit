# 1.0.0-rc.0 (2017-06-13)

- **[Internal]** Create **CHANGELOG.md**
- **[Internal]** Make the project compatible with _npm_
