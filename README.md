# Eternalfest Devkit

The Eternalfest Devkit (Development Kit) is a set of tools to create game content for Eternalfest.

The different tools are:

- **LevelEditor** (`editor`): A graphical level editor. It stores levels as `.lvl` files.
- **Mapper** (`mapper`): Allows to generate maps from `.lvl` or PNG files. Handy to view labyrinths.

The following tools are obsolete and no longer available; their source code can still be viewed on the [`legacy-tools`][legacy-tools] tag:

- **Deobfuscator** (`deobfuscator`): replaced by the [`obf` library][obf-library].
- **GameBundler** (`bundler`): replaced by the [`efc` tool][efc-tool].
- **LevelExtractor** (`extractor`): no replacement.

## Run requirements

- JDK 1.8 or higher

## Download

- [Latest version][download-master-build]

## Build from source

### Requirements

- JDK 1.8 or higher

### Build

Build a zip file containing all the tools:

```shell
# Run this command from the project root
./gradlew distZip
```

The output is in **devkit/build/distribution**.

You can also build, run or export the tools individually. Replace `$TOOL_DIRECTORY` by the tool you want:

```shell
# Run this command from the project root
./gradlew -p $TOOL_DIRECTORY jar
./gradlew -p $TOOL_DIRECTORY run
./gradlew -p $TOOL_DIRECTORY distZip
```

[download-master-build]: https://gitlab.com/eternalfest/devkit/builds/artifacts/master/download?job=build
[legacy-tools]: https://gitlab.com/eternalfest/devkit/-/tree/legacy-tools
[obf-library]: https://gitlab.com/eternalfest/obf
[efc-tool]: https://gitlab.com/eternalfest/efc

